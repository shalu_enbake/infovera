get "/demo/fill" do 

companies = Array.new 
companies = [47628,47631,47637,47629,47641,47629,47639,47632,47638,47641,47633,47633,47640]
 
 companies.each do |c|
 	
 	 	relationship = AffiliateRelationship.create(
 			affiliate_id: 1,
 			company_id: c,
 			company_approved: true
 		)
 		
 	score = Score.create(
		tevo: rand(500..840),
		company_id: c, 
		level: 4
 	)





 end


end




get "/seed/?" do 

	# Seed Companies
	# demo_seed_companies

	# See Tevo L1 Scores
	# demo_seed_scores

	# Seed Relationships
	# demo_seed_affiliate_relationships
	
	# Seed days
	# demo_seed_affiliate_views

	# Add Credits
	# demo_seed_credits

	# Seed Network Requests
	# demo_seed_network_requests

	# Seed Algolia
	demo_seed_algolia_revenue

	# Generate L4's & L5's
	# demo_seed_l4_l5

	# Generate L2 Scores
	# demo_seed_l2_scores
	

	# Randomize Dates
	# demo_seed_randomize_dates

end

configure :development do
get "/seed-dev/?" do 

	# Seed Companies
	# demo_seed_companies

	# See Tevo L1 Scores
	# demo_seed_scores

	# Seed Relationships
	# demo_seed_affiliate_relationships
	
	# Seed days
	# demo_seed_affiliate_views

	# Add Credits
	demo_seed_credits

	# Generate L4's & L5's
	demo_seed_l4_l5

	# Generate L2 Scores
	# demo_seed_l2_scores
	

	# Randomize Dates
	demo_seed_randomize_dates

end
end