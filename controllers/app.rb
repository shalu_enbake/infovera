########################################################
#
# Admin Config
#
########################################################

before do
  x_authorize! unless (request.url.include?("/seed"))
  cache_control :public, :must_revalidate, :max_age => 0
  expires 0, :public, :must_revalidate
end

not_found do
 erb :'404', :layout => false
end

error do
  @e = env['sinatra.error'].message 
  erb :'500', :layout => false
end