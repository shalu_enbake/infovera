get "/util/upload-reports/?" do 

	17.times do |r|
		report = Report.create(
				company_id: 1,
				score_id: 1,
				affiliate_id: 1
			)
	end
end


 configure :development do
# get "/util/add-usr" do 
# 	@user = AffiliateUser.create(
# 			first_name: "Tim",
# 			last_name: "Mushen",
# 			email: "tim.mushen@gmail.com",
# 			password: "Testing",
# 			affiliate_id: 1
# 		)
# 	puts "#{@user.errors.inspect}"
# end

get "/util/seed-proaffiliates/?" do 

	10.times do |i,index|
		AffiliateProaffiliateRelationship.create(
				affiliate_id: rand(58.90),
				proaffiliate_id: 1
			)
	end

end

get "/util/set-demo/" do 
	@user = AffiliateUser.get(4)
	@user.update(
			email: "joe@infovera.com",
			password: "joemama"
		)

end




get "/util/populate-relationships/?" do 
	affiliate_id = 1
	affiliate_user_id = 1 


	200.times do 
		@relationship = AffiliateRelationship.create(
				company_id: rand(5710),
				affiliate_id: session[:affiliate_id] 
			)
	end

	40.times do 
		@group = AffiliateGroupCompany.create(
		company_id: rand(5710), 
		affiliate_group_id: rand(2),
		affiliate_id: session[:affiliate_id],
		affiliate_user_id: session[:affiliate_user_id]
		)
	end


end

get "/util/companies/x_id" do 
	@companies = Company.all
	@companies.update(x_id: SecureRandom.uuid)

	@companies.each do |company|
		@co = Company.get(company.id)
		@co.update(x_id: SecureRandom.uuid)
		puts "#{@co.errors.inspect}"
	end
end


########################################################
#
#  Sync company files
#
########################################################
get '/util/sync/karla/?' do 
	@karla = CompaniesKarla.all(order: :id.desc)
	puts "#{@karla.count}"
	@karla.each do |k|
		if k.new_company_id == nil
			@company = Company.create(
				name: k.name, 
				sales_force_id: k.sales_force_id,
				address_1: k.address_1,
				address_2: k.address_2,
				address_3: k.address_3,
				city: k.city,
				state: k.state,
				zip: k.zip,
				# county: k.county,
				country: k.country,
				phone: k.phone, 
				toll_free: k.toll_free,
				website: k.website,
				fax: k.fax,
				description: k.description,
				contact_first_name: k.first_name, 
				contact_last_name: k.last_name,
				contact_title: k.title,
				ein: k.ein,
				ubi: k.ubi,
				naics: k.naics,
				naics_2: k.naics_2,
				duns: k.duns,
				sic: k.sic
			)
			puts "#{@company.errors.inspect}"
			k.update(new_company_id: @company.id)
			puts "#{k.errors.inspect}"
		else
			@company = Company.get(k.new_company_id)
			@company.update(
				name: k.name, 
				sales_force_id: k.sales_force_id,
				address_1: k.address_1,
				address_2: k.address_2,
				address_3: k.address_3,
				city: k.city,
				state: k.state,
				zip: k.zip,
				# county: k.county,
				country: k.country,
				phone: k.phone, 
				toll_free: k.toll_free,
				website: k.website,
				fax: k.fax,
				description: k.description,
				contact_first_name: k.first_name, 
				contact_last_name: k.last_name,
				contact_title: k.title,
				ein: k.ein,
				ubi: k.ubi,
				naics: k.naics,
				naics_2: k.naics_2,
				duns: k.duns,
				sic: k.sic
			)
			puts "#{@company.errors.inspect}"
		end

	end
end

get '/util/sync/kris/?' do 
	@kris = CompaniesKris.all(:id.gt => 35974)
	puts "#{@kris.count}"
	@kris.each do |k|
		if k.new_company_id == nil
			@company = Company.create(
				name: k.name, 
				# sales_force_id: k.sales_force_id,
				address_1: k.address_1,
				address_2: k.address_2,
				address_3: k.address_3,
				city: k.city,
				state: k.state,
				zip: k.zip,
				# county: k.county,
				country: k.country,
				phone: k.phone, 
				toll_free: k.toll_free,
				website: k.website,
				fax: k.fax,
				# description: k.description,
				contact_first_name: k.first_name, 
				contact_last_name: k.last_name,
				# contact_title: k.title,
				# ein: k.ein,
				# ubi: k.ubi,
				naics: k.naics,
				naics_2: k.naics_2,
				duns: k.duns,
				sic: k.sic
			)
			puts "#{@company.errors.inspect}"
			k.update(new_company_id: @company.id)
			puts "#{k.errors.inspect}"
		else
			@company = Company.get(k.new_company_id)
			@company.update(
				name: k.name, 
				# sales_force_id: k.sales_force_id,
				address_1: k.address_1,
				address_2: k.address_2,
				address_3: k.address_3,
				city: k.city,
				state: k.state,
				zip: k.zip,
				# county: k.county,
				country: k.country,
				phone: k.phone, 
				toll_free: k.toll_free,
				website: k.website,
				fax: k.fax,
				# description: k.description,
				contact_first_name: k.first_name, 
				contact_last_name: k.last_name,
				# contact_title: k.title,
				# ein: k.ein,
				# ubi: k.ubi,
				naics: k.naics,
				naics_2: k.naics_2,
				duns: k.duns,
				sic: k.sic
			)
			puts "#{@company.errors.inspect}"
		end

	end
end

########################################################
#
#  Migrate Karla Scores
#
########################################################
get '/util/sync/scores/kris-update/?' do 

	@kris = CompaniesKris.all(order: :id.desc)

	@kris.each do |k|

		score = Score.create(
			tevo: k.tevo_score,
			score_date: Time.now,
			company_id: k.new_company_id,
			revenue: k.revenue,
			assets: k.assets, 
			employees: k.employees
		)

		puts "#{score.errors.inspect}"

	end
		# @score.save

	# @report = Score.first(company_id: k.new_company_id)
	# 	if @report.nil?
	# 		# Create Score
			

	# 	else
	# 		# Update Row
	# 		@score = Score.first(company_id: k.new_company_id)
	# 		@score.update(
	# 				tevo: k.tevo_score,
	# 				score_date: Time.now,
	# 				revenue: k.revenue,
	# 				assets: k.assets, 
	# 				employees: k.employees
	# 			)

	# 	end

end

########################################################
#
#  Scores Karla
#
########################################################
get '/util/sync/scores/karla/?' do 
	@karla = CompaniesKarla.all(order: :id)
	puts "#{@karla.count}"
	@karla.each do |k|
	@report = Score.first(company_id: k.new_company_id)
		if @report.nil?
			# Create Score
			@score  = Score.create(
					tevo: k.tevo_score,
					score_date: k.tevo_score_date,
					company_id: k.new_company_id,
					# revenue: k.revenue,
					# assets: k.assets, 
					employees: k.employees
				)

		else
			# Update Row
			@score = Score.first(company_id: k.new_company_id)
			@score.update(
					tevo: k.tevo_score,
					score_date: k.tevo_score_date,
					# revenue: k.revenue,
					# assets: k.assets, 
					employees: k.employees
				)

		end
end
end
########################################################
#
# Sync all companies to algolia
#
########################################################
get "/util/companies/sync_company_algelo" do
	begin
		@companies = Company.last(100)  #all(x_id: nil)
		my_index = Algolia::Index.new("#{settings.company_index}")
		object_json = []
		@companies.each do |company|
			score = company.scores.last
			if score.nil?
				score = Score.new
			end
			object_json << {
				:objectID => company.id,
				:name => company.name,
	      :id => company.id,
	      :x_id => company.x_id,
	      :ubi => company.ubi,
	      :ein => company.ein,
	      :address => company.address_1,
	      :address_2 => company.address_2,
	      :city => company.city,
	      :state => company.state,
	      :zip => company.zip,
	      :phone => company.phone,
	      :website => company.website,
	      :naics => company.naics,
	      :tevo =>score.tevo,
	      :revenue => score.revenue,
				:assets => score.assets,
				:cash => score.cash,
				:ebitda => score.ebitda,
				:ar => score.ar,
				:inventory => score.inventory,
				:total_liabilities => score.total_liabilities,
				:number_of_employee => score.number_of_employee,
				:ebitda_percent_rank => score.percentile_ebitda,
				:cash_percent_rank => score.percentile_cash,
				:ar_percent_rank => score.percentile_ar,
				:inventory_percent_rank => score.percentile_inventory,
				:rev_emp_percent_rank => score.percentile_rev_emp,
				:total_liabilitie_percent_rank => score.percentile_total_liabilities,
				:revenue_percent_rank => score.percentile_revenue,
				:total_assets_percent_rank => score.percentile_assets
	    }
		end
		my_index.add_objects(object_json)
		flash[:success] = "Company Sync to algolia."
		redirect "/"
	rescue Exception => e
		Bugsnag.notify(RuntimeError.new(e))
		flash[:error] = "There are some errors, please try again! #{e}"
		redirect "/"
	end
end

########################################################
#
# Delete all companies to algolia
#
########################################################
get "/util/companies/delete_company_from_algelo" do 
	begin
		company_objects = []
		Company.last(100).each do |company|  #all(x_id: nil)
			company_objects << company.id
		end
		my_index = Algolia::Index.new("#{settings.company_index}")
		my_index.delete_objects(company_objects)
		flash[:success] = "Company removed from algolia."
		redirect "/"
	rescue Exception => e
		Bugsnag.notify(RuntimeError.new(e))
		flash[:error] = "There are some errors, please try again! #{e}"
		redirect "/"
	end
end


########################################################
#
# Sync all Affiliate to algolia
#
########################################################
get "/util/companies/sync_affiliate_algelo" do
	begin
		@affiliates = Affiliate.all
		my_index = Algolia::Index.new("#{settings.affiliate_index}")
		object_json = []
		@affiliates.each do |affiliate|
			object_json << {
				:objectID => affiliate.id,
				:name => affiliate.name,
				:id => affiliate.id,
				:x_id => affiliate.x_id.to_s,
				:affiliate_type => affiliate.affiliate_type,
				:phone => affiliate.phone,
				:email => affiliate.email,
				:web => affiliate.web,
		 		:city => affiliate.city,
		 		:state => affiliate.state,
		 		:zip => affiliate.zip,
		 		:summary => affiliate.summary,
			}
		end
		my_index.add_objects(object_json)
		flash[:success] = "Affiliate Sync to algolia."
		redirect "/"
	rescue Exception => e
		Bugsnag.notify(RuntimeError.new(e))
		flash[:error] = "There are some errors, please try again! #{e}"
		redirect "/"
	end
end

########################################################
#
# Delete all affiliate to algolia
#
########################################################
get "/util/companies/delete_affiliate_from_algelo" do 
	begin
		affiliate_objects = []
		Affiliate.all.each do |affiliate|  #all(x_id: nil)
			affiliate_objects << affiliate.id
		end
		my_index = Algolia::Index.new("#{settings.affiliate_index}")
		my_index.delete_objects(affiliate_objects)
		flash[:success] = "Affiliate removed from algolia."
		redirect "/"
	rescue Exception => e
		Bugsnag.notify(RuntimeError.new(e))
		flash[:error] = "There are some errors, please try again! #{e}"
		redirect "/"
	end
end


########################################################
#
# Sync all Score to algolia
#
########################################################
get "/util/companies/sync_score_algelo" do
	begin
		@scores = Score.last(200)
		my_index = Algolia::Index.new("demo_score")
		object_json = []
		@scores.each do |score|
			object_json << {
				:objectID => score.id,
				:level => score.level,
				:id => score.id,
				:x_id => score.x_id.to_s,
				:tevo => score.tevo,
				:revenue => score.revenue,
				:total_asset => score.assets,
				:number_of_employee => score.employees,
		 		:company_id => score.company_id,
			}
		end
		my_index.add_objects(object_json)
		flash[:success] = "Score Sync to algolia."
		redirect "/"
	rescue Exception => e
		Bugsnag.notify(RuntimeError.new(e))
		flash[:error] = "There are some errors, please try again! #{e}"
		redirect "/"
	end
end

########################################################
#
# Delete all score to algolia
#
########################################################
get "/util/companies/delete_score_from_algelo" do 
	begin
		score_objects = []
		Score.last(200).each do |score|  #all(x_id: nil)
			score_objects << score.id
		end
		my_index = Algolia::Index.new("demo_score")
		my_index.delete_objects(score_objects)
		flash[:success] = "Score removed from algolia."
		redirect "/"
	rescue Exception => e
		Bugsnag.notify(RuntimeError.new(e))
		flash[:error] = "There are some errors, please try again! #{e}"
		redirect "/"
	end
end

########################################################
#
#  Create Affiliate
#
########################################################
get "/util/affiliate-add/?" do 

	@affiliate = Affiliate.create(name: "Testing Affiliate")
	@affiliate_user = AffiliateUser.create(
			email: "tim.mushen@gmail.com",
			user_name: "tim.mushen@gmail.com",
			first_name: "Tim",
			last_name: "Mushen",
			password: "Testing",
			affiliate_id: @affiliate.id

		)
end

########################################################
#
#  Update Affiliate
#
########################################################
get "/util/affiliate-update/?" do 
	@affiliate = Affiliate.all 
	@affiliate.each do |affiliate|
		affiliate.update(updated_at: Time.now, created_at: Time.now)
	end
end

########################################################
#
#  Update Companies
#
########################################################
get "/util/companies-update/?" do 
	@companies = Company.all(limit: 10000) 
	@companies.each do |company|
		company.update(updated_at: Time.now)
	end
end

########################################################
#
#  Naics Short
#
########################################################
get "/util/company-split-naics/?" do 
	@companies = Company.all(:naics_sector_id => 0)
	

	@companies.each do |c|
		ns = "#{(c.naics.to_s[0..1])}"
		sector = NaicsSector.first(code: "#{c.naics.to_s[0..1]}")
		naic = Naic.first(code: c.naics)
		naics_id = naic.id rescue nil
		naics_id ||= 0
		unless sector.nil?
		new_company = Company.first(id: c.id)
		new_company.update(
			naics_sector_id: sector.id,
			naic_id: naics_id,
			naics_sector_code: "#{ns}"
			)
		puts "#{c.naics.to_s[0..1]}"
		puts "#{new_company.errors.inspect}"
		end
	end

end

########################################################
#
#  Set up Affiliate Group Companies
#
########################################################
get "/util/affiliate-groups" do 
	@companies = Company.all(:id.gt => 42534)
	@companies.each do |company|
		af = AffiliateGroupCompany.create(
				company_id: company.id, 
				affiliate_group_id: 1,
				affiliate_id: 1,
				affiliate_user_id: 1
			)
	end
	
end

########################################################
#
#  Set up Categories
#
########################################################
get "/util/affiliate-categories/?" do 

	category_list = ["Finacial Analysis", "Base Business", "Systems & Processes", "Management Team", "Growth & Potenial"]
	category_list.each do |item|
		@cat = AffiliateCategory.create(
				name: item
			)
	end

end

########################################################
#
#  Set up Sub Categories
#
########################################################
get "/util/affiliate-subcategories/?" do 

	category_list = ["Life Insurance","Business Strategy","Marketing","Sales","Merger & Acquisition","Business Broker","HR Services","Benefits","P&C Insurance","401(k)","Peer to Peer Advisory","Payroll","Merchant Services","Business Coaching","Capital Equipment/Leasing","Transportation","Logistics Consulting","Executive Recruiting"]
	category_list.each do |item|
		@cat = AffiliateSubcategory.create(
				affiliate_category_id: 1,
				name: item
			)
	end

end

########################################################
#
#  See Affiliate Relationships By Group
#
########################################################
get "/util/affiliate-relationship/?" do 
	@companies = AffiliateGroupCompany.all(affiliate_id: 1)

	@companies.each do |c|
		relationship = AffiliateRelationship.create(
				company_id: c.company_id,
				affiliate_id: 1,
				company_approved: true
			)

	end
end

########################################################
#
#  Add Testing EU
#
########################################################
get "/util/add-testing-eu/?" do 
	@eu = User.create(
		user_name: "example",
		password: "example",
		first_name: "Test",
		last_name: "Test",
		email: "t@getx.io",
		company_id: 42554

		)
	halt "#{@eu.errors.inspect}"
end



########################################################
#
#  Update Score
#
########################################################
get "/util/update-score-l4/?" do 
	@score = Score.first(company_id: 42554)

	@score.update(
			:financial_percentage => 74,
			:base_percentage => 61,
			:systems_percentage => 80,
			:management_percentage => 46,
			:growth_percentage => 74,
			:overall_percentage => 69,
		)

halt "#{@score.errors.inspect}"

end

########################################################
#
#  Add New Term
#
########################################################
get "/util/add_term/?" do 
	begin
		@term = Term.new
		@term.terms = '<p>By your signature below,” You agree to the following terms and conditions for obtaining the TEVO Service from InfoVera. (“InfoVera”) as selected by You the Client. In this Agreement, “We”, “Our” and “InfoVera” refers to Sterling Creek Holdings, Inc. DBA InfoVera<br>
		</p>
		<p>InfoVera provides enterprise evaluation services including a Total Enterprise Valuation Opportunity (TEVO) score which is an objective measure that third parties, such as lenders, investment banks and acquirers, may use to measure the business risk and market value of an enterprise. You are obtaining the Services relating to a business enterprise, you selected .
		</p>
		<p>The TEVO score and services are provided in various levels of detail and precision.
		</p>
		<p>You desire to purchase from InfoVera the Services you selected on this website
		on the terms and conditions set forth herein.
		</p>
		<p>In consideration of the mutual covenants contained herein, InfoVera and You (each a “Party,” and collectively the “Parties”) agree as follows:</p>
		<p>Definitions. All definitions below or elsewhere in these Terms of Service apply to both their singular and plural forms, as the context may require. "H/herein," "hereunder," and "hereof" and similar expressions refer to these Terms of Service. "Section" refers to sections in these Terms of Service. "I/including" means "including without limitation." "Days" means "calendar days."
		</p>
		<p>“Your Data” means the business information pertaining to the business enterprise furnished by You as part of Your use of the Services.
		</p>
		<p>“InfoVera Technology” collectively means the Services, the TEVO score, the Documentation, the Services Output and all Intellectual Property Rights therein or associated therewith.
		</p>
		<p>“Documentation” means the published documentation provided to You by InfoVera related to the Services.
		</p>
		<p>"Intellectual Property" or "Intellectual Property Rights" collectively means any and all patents, patent registrations, patent applications, data rights, utility models, business processes, trademarks or names, service marks or names, trade secrets, know how, mask works, copyrights, moral rights and any other form of proprietary protection arising or enforceable under the laws of the United States, any other jurisdiction or any bi-lateral or multi-lateral treaty.
		</p>
		<p>“Services” means any services to be provided by InfoVera, that You have selected.
		</p>
		<p>“Services Output” means any analytic output generated by the Services including the TEVO score.
		</p>
		<p>“Specifications” means the parameters, rules and data input specified by InfoVera in the Forms that You must complete in order to obtain the Services Output.
		</p>
		<p><span style="font-weight: bold;">1.<span class="Apple-tab-span" style="white-space:pre">	</span>SERVICES</span>
		</p>
		<p>1.1InfoVera provides the Level 4: Marketability Assessment.
		</p>
		<p>1.2In order to obtain the Services selected, You shall complete the form related to the specific Services and provide the information requested and provide the information about the business enterprise.
		</p>
		<p><span style="font-weight: bold;">2.<span class="Apple-tab-span" style="white-space:pre">	</span>DUTIES OF INFOVERA</span></p>
		<p>2.1InfoVera shall perform the Services and produce such data, information, documents or other materials (“Output Material”) as described and in the manner set forth.
		</p>
		<p>2.2InfoVera may also request from You any data, information, documents or other materials related to the business enterprise that it believes necessary or appropriate to the provision of the Services (“Input Material”). 
		</p>
		<p>2.3InfoVera’s Services shall be limited to providing the reports You selected. 
		</p>
		<p>2.4InfoVera shall have no obligation to evaluate any type of investments or any investment criteria or objectives. 
		</p>
		<p>2.5InfoVera shall not have discretionary authority or control (as agent, attorney-in-fact or otherwise) to implement, or to direct the implementation of any investment selection on behalf of Your business enterprise, or otherwise to negotiate the terms of any investment, or to enter into any contracts relating to any investment in which You may be interested or involving Your business enterprise. 
		</p>
		<p>2.6InfoVera shall not advise on any tax, compliance, legal or regulatory matters. There is no mutual agreement, arrangement or understanding, written or otherwise, between InfoVera and You that the Services will serve as a primary basis for investment decisions to be made regarding Your business enterprise.
		</p>
		<p><span style="font-weight: bold;">3.<span class="Apple-tab-span" style="white-space:pre">	</span>YOUR DUTIES</span>
		</p>
		<p>3.1You shall supply InfoVera with all Input Material requested to enable InfoVera to provide the Services in accordance with these Terms of Service and make reasonable efforts to verify the accuracy and completeness of Input Material.
		</p>
		<p>3.2The Parties agree that InfoVera will rely upon the Input Material in the performance of the Services, that InfoVera shall have no obligation to verify the accuracy or completeness of the Input Material, and that InfoVera shall have no liability for consequences of such inaccuracy or incompleteness. 
		</p>
		<p><span style="font-weight: bold;">4.<span class="Apple-tab-span" style="white-space:pre">	</span>TIME OF PERFORMANCE</span>
		</p>
		<p>InfoVera shall use reasonable efforts to provide the Services to You as expeditiously as possible. However, the Parties agree that time is not of the essence in InfoVera’s provision of Services and failure to meet any time frame shall not be deemed a material term. Furthermore, InfoVera assumes no responsibility or liability for any losses, direct or consequential, in the event that it fails to provide the Services by any estimated timeframes agreed upon by the Parties.
		</p>
		<p><span style="font-weight: bold;">5.<span class="Apple-tab-span" style="white-space:pre">	</span>PAYMENT</span>
		</p>
		<p>The fees for the Services You select are specified by either your InfoVera ProAffiliate or by InfoVera. Upon payment of the fees, InfoVera shall provide the TEVO score and Services You selected.
		</p>
		<p><span style="font-weight: bold;">6.<span class="Apple-tab-span" style="white-space:pre">	</span>STANDARD OF CARE</span>
		</p>
		<p>Any use of or reliance on the Services and/or the Output Material shall be at Your sole risk. InfoVera shall use reasonable care in the performance of the Services and the provision of the Output Material, and may rely on any instruction from You that it reasonably believes to be genuine and authorized.
		</p>
		<p><span style="font-weight: bold;">7.<span class="Apple-tab-span" style="white-space:pre">	</span>LIMITATION OF INFOVERA’S LIABILITY</span>
		</p>
		<p>7.1 The Parties agree that InfoVera shall not be liable for any of the following:
		</p>
		<p>(i) Any direct, indirect, incidental, special or consequential losses, damages, costs, expenses or other claims:
		</p>
		<p>(a) that You may incur by reason of any business decision made, or other action taken or omitted, in good faith, by You in reliance on the Services, Output Material, or any other communications with InfoVera regarding Your business enterprise;
		</p>
		<p>(b) arising from InfoVera’s adherence to Your instructions; 
		</p>
		<p>(c) arising from, or in any way connected to incomplete, inaccurate, illegible, out of sequence, or wrongly formed Input Material or instructions supplied by You, or arising from their late arrival or non-arrival, or otherwise caused by You; or
		</p>
		<p>(d) arising from a delay in performing, or failure to perform any of InfoVera’s obligations under the Terms of Service, if the delay or failure was due to a Force Majeure event (as defined below). 
		</p>
		<p>(ii) Any losses, damages, costs, expenses or other claims, including any loss of profits, goodwill, reputation, revenue, opportunity, anticipated savings, business receipts, or contracts, or any direct, indirect, incidental, special or consequential losses, damages, costs, expenses or other claims howsoever caused whether arising directly or indirectly out of the provision by InfoVera of the Services or the Output Material; or of any error, inaccuracy, or other defect therein, or the performance, non-performance or delayed performance by InfoVera of its obligations under the Terms of Service; or resulting from Third Party claims which arise out of, or in connection with InfoVera’s provision of Services or Output Material to the You;
		</p>
		<p>7.1Notwithstanding any other provision of these Terms of Service, in no case shall InfoVera’s total liability to You under or in connection with these Terms of Service, whether in contract, tort or otherwise, be greater than the amount of the annual fees paid by You, provided however, that the federal and state securities laws impose liabilities under certain circumstances on persons who act in good faith, and therefore nothing in these Terms of Service will waive or limit any rights that You may have under those laws.
		</p>
		<p><span style="font-weight: bold;">8.<span class="Apple-tab-span" style="white-space:pre">	</span>REPRESENTATIONS AND WARRANTIES / COVENANTS</span>
		</p>
		<p>8.1WARRANTY DISCLAIMER. The TEVO score is an estimate for the value and multiple of Your business enterprise. InfoVera does not warrant that any InfoVera Product will (i) meet Your requirements, (ii) meet any performance level, resource utilization, response time, or system overhead requirements, or (iii) operate uninterrupted, free of errors, or without delay. EXCEPT FOR THE EXPRESS WARRANTIES SET FORTH IN THIS AGREEMENT, INFOVERA MAKES NO OTHER WARRANTIES, EITHER EXPRESS OR IMPLIED, AND HEREBY DISCLAIMS ALL IMPLIED WARRANTIES, INCLUDING ANY WARRANTIES REGARDING MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, AND ANY WARRANTY ARISING FROM A COURSE OF DEALING, USAGE, OR TRADE PRACTICE.
		</p>
		<p>In performing the Services and preparing the Output Material, InfoVera may rely upon information from a variety of sources, including that provided by You or parties other than the You; some of the information obtained may be inaccurate and InfoVera makes no representations or warranties as to the quality, accuracy, completeness, or fitness for purpose of the Services or Output Material or of any data, information or other materials supplied by the You or any party other than the You.
		</p>
		<p>8.2Each Party represents and warrants to the other Party that:
		</p>
		<p>(i) it has the requisite legal capacity and authority to agree to these Terms of Service;
		</p>
		<p>(ii) these Terms of Service have been duly authorized and are a legal, valid and binding agreement enforceable against it in accordance with their terms;
		</p>
		<p>(iii) <span class="Apple-tab-span" style="white-space:pre">	</span>the execution of these Terms of Service and the performance of its obligations hereunder do not conflict with or violate any provisions of any obligations by which the Party is bound, whether by contract, operation of law or otherwise;
		</p>
		<p>(v)<span class="Apple-tab-span" style="white-space:pre">	</span>it owns or otherwise has the right to use and permit the other to use the Output Material or Input Material (as appropriate) for the purposes contemplated by these Terms of Service, and that any such use by the other Party will not violate or infringe any copyright and intellectual property rights of any other person to the Output Material or Input Material (as appropriate).
		</p>
		<p>8.3You Covenant:
		</p>
		<p>You shall not re-sell the Output Material or Services for any consideration and whether in whole or in part and in whatever form or medium and whether under InfoVera’s label or under any other label to any Third Party, except that the Output Material and Services may be shared with financial advisors, accountants, attorneys and others assisting You in analysing the business enterprise; 
		</p>
		<p><br>
		</p>
		<p><span style="font-weight: bold;">9.<span class="Apple-tab-span" style="white-space:pre">	</span>INTELLECTUAL PROPERTY</span>
		</p>
		<p>9.1Input Material. You grant to InfoVera a limited, non-exclusive, non-transferable right to use the Input Material in connection with its performance of its obligations under these Terms of Service and to incorporate the Input Material (or portions thereof) into the Output Material. Nothing in these Terms of Service shall limit in any way the InfoVera’s right to develop,use, license, create derivative works of, or otherwise exploit the InfoVera’s Intellectual Property Rights arising from or related to any of the foregoing, or to permit third parties to do so.InfoVera shall be free to use the ideas, concepts, techniques and know-how used and developed in connection with delivering the Services hereunder. In any event, InfoVera shall befree to perform similar services for other clients and customers, including Your competitors, using general knowledge, skills and experience obtained in connection with providing theServices.
		</p>
		<p>9.2Output Material. InfoVera grants to You a limited, non-exclusive, non-transferable right to use the Output Material solely for the purposes contemplated by, and subject to the terms of these Terms of Service. You acknowledge, that, except with respect to any Input Material incorporated into the Output Material, or as expressly granted by these Terms of Service,You hold no property, copyright, intellectual property or other rights in the Output Material other than the right to use the Output Material for the purposes permitted by the Terms of Service.
		</p>
		<p><span style="font-weight: bold;">10.<span class="Apple-tab-span" style="white-space:pre">	</span>CONFIDENTIALITY</span>
		</p>
		<p>10.1Meaning of Confidential Information. For the purposes of these Terms of Service, “Confidential Information” means: (i) with respect to InfoVera, all Intellectual Property of InfoVera; the Output Material and any information or material, in tangible or intangible form, pertaining to its business or affairs or any of its affiliates, subsidiaries, directors, officers, employees, consultants, independent contractors, customers, suppliers, managers, service providers, any and all trade secrets relating to business practices and procedures and any and all information relating to assets, valuation and pricing methods, processes, financial data, lists, statistics, systems or equipment, programs, research and development, strategic plans or related information (whether in relation to past, present, or future, business activities), research, services, financial products, investment policies and procedures, markets, methods, processes, formulas, records, marketing materials or financial information whether disclosed with a proprietary or confidential stamp or legend, or otherwise and whether disclosed orally, or in writing or electronically without a proprietary or confidential stamp or legend if it would be apparent to a reasonable person, familiar with InfoVera’s business or the alternative investment industry that such information or material is of a confidential or proprietary nature, the maintenance of which would be important, and which information and material is regarded as being proprietary under the Terms of Service; (ii) with respect to You, the Input Material to the extent they identify Your business enterprise. 
		</p>
		<p>10.2Maintenance of Confidentiality. Each Party acknowledges that the Confidential Information contains proprietary information and should be kept confidential. Each Party agrees not to use, disclose, reproduce, summarize and/or distribute Confidential Information disclosed to it by the other Party for any purpose, except as provided by these Terms of Service, or with the consent of the other party. 
		</p>
		<p>The obligations under this Section shall not apply to the extent any Party’s Confidential Information (the “Non-Disclosing Party”):
		</p>
		<p>(i) is in the possession of the other Party, at the time of disclosure to such other Party, as shown by the other Party’s files and records immediately prior to the time of that disclosure;
		</p>
		<p>(ii) lawfully comes into the other Party’s possession from a party (other than You) who has a right to disclose the same; 
		</p>
		<p>(iii) becomes, or has become, at any time part of the public knowledge or literature, not as a result of any improper action or inaction of the other Party; 
		</p>
		<p>(iv) is or has been expressly approved in writing by the Non-Disclosing Party for release or use by the other Party; 
		</p>
		<p>(v) is or has been independently developed by the other Party in its name, without the use of, or reference to, any Confidential Information received from the Non-Disclosing Party; or
		</p>
		<p>(vi) is required to be disclosed under the compulsory process or procedure of any governmental, judicial, or administrative authority, provided that the disclosing Party shall use reasonable efforts to notify the other Party of any such disclosure in advance of making such disclosure so as to afford the other Party time to take any action which it reasonably believes to be appropriate (whether as to the advisability of taking any legally-available steps to resist or narrow such disclosure or otherwise).
		</p>
		<p><span style="font-weight: bold;">11.</span><span style="font-weight: bold;">INDEMNIFICATION</span>
		</p>
		<p>You shall defend, indemnify, and hold harmless InfoVera and its employees, directors, officers and agents (collectively the “InfoVera Indemnitees”), from and against any and all third party claims, actions, suits, investigations, governmental actions, liabilities, judgments, demands, losses, damages, costs or expenses, including reasonable attorneys’ fees, arising out of any unauthorized use by You of the Services or any violation of InfoVera’s Intellectual Property Rights to InfoVera Technology by You.
		</p>
		<p><span style="font-weight: bold;">12.</span><span style="font-weight: bold;">LIMITATION OF LIABILITY</span>
		</p>
		<p>12.1<span class="Apple-tab-span" style="white-space:pre">	</span><span style="text-decoration: underline;">Limitation On Types of Recoverable Damages</span>. EXCEPT WITH RESPECT TO THE INDEMNIATION PROVISIONS SET FORTH HEREIN ANY VIOLATION BY YOU OF THE INFOVERA’S INTELLECTUAL PROPERTY RIGHTS NEITHER PARTY SHALL BE LIABLE FOR ANY CONSEQUENTIAL, INCIDENTAL, INDIRECT, EXEMPLARY, SPECIAL OR PUNITIVE DAMAGES (INCLUDING LOST PROFITS), REGARDLESS OF WHETHER THE CLAIM GIVING RISE TO SUCH DAMAGES IS BASED UPON BREACH OF WARRANTY, BREACH OF CONTRACT, STRICT LIABILITY, NEGLIGENCE OR OTHER THEORY, EVEN IF A PARTY HAS BEEN ADVISED OF THE POSSIBILITY THEREOF.
		</p>
		<p>12.2<span class="Apple-tab-span" style="white-space:pre">	</span><span style="text-decoration: underline;">Limitation on Maximum Damages</span>. IN NO EVENT SHALL INFOVERA BE LIABLE TO YOU FOR DAMAGES UNDER THESE TERMS OF SERVICE FOR THE APPLICABLE SERVICE IN EXCESS OF THE TOTAL FEES PAID BY YOU TO INFOVERA FOR THE SERVICES PROVIDED.
		</p>
		<p><span style="font-weight: bold;">13.  MISCELLANEOUS</span>
		</p>
		<p>13.1<span class="Apple-tab-span" style="white-space:pre">	</span><span style="text-decoration: underline;">Governing Law.</span> The validity, interpretation, construction and performance of these Terms of Service, and all acts and transactions pursuant hereto and the rights and obligations of the parties hereto will be governed, construed and interpreted in accordance with the laws of the state of Utah, without giving effect to principles of conflicts of law. For purposes of litigating any dispute that may arise directly or indirectly from these Terms of Service, the parties hereby submit and consent to the exclusive jurisdiction and venue of the state of Utah and agree that any such litigation will be conducted only in the courts of Utah or the federal courts of the United States located in Utah and no other courts. Each Party hereby waves the right to assert the doctrine of Forum Non Conveniens or to otherwise object to venue.
		</p>
		<p>13.2<span class="Apple-tab-span" style="white-space:pre">	</span><span style="text-decoration: underline;">Entire Terms of Service.</span> This Terms of Service sets forth the entire agreement and understanding of the parties relating to the subject matter herein and supersedes all prior or contemporaneous discussions, understandings and agreements whether oral or written, between them relating to the subject matter hereof. 
		</p>
		<p>13.3<span class="Apple-tab-span" style="white-space:pre">	</span><span style="text-decoration: underline;">Amendments and Waivers.</span> No modification of or amendment to these Terms of Service, nor any waiver of any rights under these Terms of Service, will be effective unless in writing signed by the parties to these Terms of Service. No delay or failure to require performance of any provision of these Terms of Service will constitute a waiver of that provision as to that or any other instance.
		</p>
		<p>13.4<span class="Apple-tab-span" style="white-space:pre">	</span><span style="text-decoration: underline;">Successors and Assigns.</span> Except as otherwise provided in these Terms of Service, these Terms of Service, and the rights and obligations of the parties hereunder, will be binding upon and inure to the benefit of their respective successors, assigns, heirs, executors, administrators and legal representatives. InfoVera may assign any of its rights and obligations under these Terms of Service. No other party to these Terms of Service may assign, whether voluntarily or by operation of law, any of its rights and obligations under these Terms of Service, except with the prior written consent of InfoVera. 
		</p>
		<p>13.5<span class="Apple-tab-span" style="white-space:pre">	</span><span style="text-decoration: underline;">Severability.</span> If any one or more provisions (or any part thereof) of these Terms of Service shall be held invalid, illegal or unenforceable in any respect, that provision shall be construed so as to most closely reflect the original intent of the parties, but still be enforceable, and the validity, legality and enforceability of the remaining provisions (or any part thereof) shall not in any way be affected or impaired thereby.
		</p>'
		@term.save
		flash[:success] = "New terms added."
		redirect "/terms/"	
	rescue Exception => e
		Bugsnag.notify(RuntimeError.new(e))
		flash[:error] = "There are some errors, please try again! #{e}"
		redirect "/"
	end
end

########################################################
#
#  Add Data to naics_sub_sector and naics_industry_group
#
########################################################
get "/util/add_naics_sector_and_naics_industry_group/?" do
	begin
		@naics = Naic.all
		@naics.each do |naic|
			if naic.level == "Sector"
				naic_sector = NaicsSector.first(code: naic.code)
				unless naic_sector.present?
					NaicsSector.create(code: naic.code, name: naic.title)
				end
			elsif naic.level == "Subsector"
				naic_sub_sector = NaicsSubSector.first(code: naic.code)
				unless naic_sub_sector.present?
					NaicsSubSector.create(code: naic.code, name: naic.title)
				end
			elsif naic.level == "NAICS Industry"
				naics_industry = NaicsIndustry.first(code: naic.code)
				unless naics_industry.present?
					NaicsIndustry.create(code: naic.code, name: naic.title)
				end
			else
				naics_industry_group = NaicsIndustryGroup.first(code: naic.code)
				unless naics_industry_group.present?
					NaicsIndustryGroup.create(code: naic.code, name: naic.title)
				end
			end
		end
		flash[:success] = "Naics data saved to seperate tables."
		redirect "/"
	rescue Exception => e
		Bugsnag.notify(RuntimeError.new(e))
		flash[:error] = "There are some errors, please try again! #{e}"
		redirect "/"
	end
end

end
# Faker::Address.city #=> "Imogeneborough"
# Faker::Address.street_name #=> "Larkin Fork"
# Faker::Address.street_address #=> "282 Kevin Brook"
# Faker::Address.secondary_address #=> "Apt. 672"
# Faker::Address.building_number #=> "7304"
# Faker::Address.zip_code #=> "58517"
# Faker::Address.zip #=> "58517"
# Faker::Address.postcode #=> "58517"
# Faker::Address.time_zone #=> "Asia/Yakutsk"
# Faker::Address.street_suffix #=> "Street"
# Faker::Address.city_suffix #=> "fort"
# Faker::Address.city_prefix #=> "Lake"
# Faker::Address.state #=> "California"
# Faker::Address.state_abbr #=> "AP"
# Faker::Address.country #=> "French Guiana"
# Faker::Address.country_code #=> "IT"
# Faker::Address.latitude #=> "-58.17256227443719"
# Faker::Address.longitude #=> "-156.65548382095133"
# Faker::Name.name #=> "Tyshawn Johns Sr."
# Faker::Name.first_name #=> "Kaci"
# Faker::Name.last_name #=> "Ernser"
# Faker::Name.prefix #=> "Mr."
# Faker::Name.suffix #=> "IV"
# Faker::Name.title #=> "Legacy Creative Director"