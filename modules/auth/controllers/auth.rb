########################################################
#
# Set Before for AffiliateUser Area
#
########################################################
before '/login/*' do
  x_authorize!
end

########################################################
#
# Login Form
#
########################################################
get '/login/?' do
  erb :"../modules/auth/views/login", :layout => :login_layout
end

########################################################
#
# Login Process
#
########################################################
post '/login/?' do
  begin
    user = AffiliateUser.first(email: params[:post]['email'].downcase, deleted: false)
    affiliate = user.affiliate unless user.nil?
    redirect_to = params[:post]['redirect_to']
    # redirect_to ||= "/academy/"
    redirect_to ||= "/"

    # encrypt password
    pwd =params[:post]['password']
    if !user.nil? && !user.password.nil? && user.password == pwd && user.email.downcase == params[:post]['email'].downcase 

      if affiliate.nil? 
        session[:error] = "Your account is not active."
        redirect "/login/"
      end

       if affiliate.deleted? 
        session[:error] = "Your account is not active."
        redirect "/login/"
      end


      if user.active == true
        #Set Session Vars

        session[:authorized] = true
        session[:first_name] = user.first_name
        session[:last_name] = user.last_name
        session[:email] = user.email
        session[:affiliate_user_id] = user.id
        session[:affiliate_id] = user.affiliate_id
        session[:logo_secure_url] = user.affiliate.logo_secure_url rescue nil

        if user.user_badge.present?
          if user.affiliate.logo_secure_url.present?
            user.user_badge.update(first_logo_upload: true, sign_in_count: user.user_badge.sign_in_count + 1)
          else
            user.user_badge.update(sign_in_count: user.user_badge.sign_in_count + 1)
          end
        else
          UserBadge.create(sign_in_count: 1, affiliate_user_id: user.id)
        end
        if user.user_badge.sign_in_count == 1
          session[:sign_in_count] = user.user_badge.sign_in_count
        end
        if user.user_badge.first_company_searched == false
          session[:first_company_searched] = true
        end
        if user.user_badge.first_group_created == false
          session[:first_group_created] = true
        end
        if user.user_badge.first_score_created == false
          session[:first_score_created] = true
        end
        if user.user_badge.first_company_added_to_group == false
          session[:first_company_added_to_group] = true
        end
        if user.user_badge.first_network_connection_accept == false
          session[:first_network_connection_accept] = true
        end
        if user.user_badge.first_company_user_created == false
          session[:first_company_user_created] = true
        end
        if user.user_badge.first_video_completed == false
          session[:first_video_completed] = true
        end
        if user.user_badge.first_logo_upload == false
          session[:first_logo_upload] = true
        end

        if user.password_reset == true
          redirect '/login/reset/'
        else
          has_accepted_terms?
          redirect redirect_to
        end


      else
        session[:authorized] = false
        flash[:error] = "Your account is deactivated by Admin."
        redirect '/login/'
      end
    else
      session[:authorized] = false
      flash[:error] = "You entered incorrect email or password."
      redirect '/login/'
    end
  rescue Exception => e
    flash[:error] = "There are some errors, please try again! #{e}"
    Bugsnag.notify(RuntimeError.new(e))
    redirect "/"
  end
end

########################################################
#
# Forgot Password Form
#
########################################################
get '/login/forgot/?' do
  begin
    erb :"../modules/auth/views/forgot", :layout => :login_layout
  rescue Exception => e
    flash[:error] = "There are some errors, please try again! #{e}"
    Bugsnag.notify(RuntimeError.new(e))
    redirect "/"
  end
end

########################################################
#
# Forgot Password Process
#
########################################################
post '/login/forgot/?' do
  begin
    user = AffiliateUser.first(email: params[:post]['email'],deleted: false)

    if !user.nil?
      @random_password = Array.new(10).map { (65 + rand(58)).chr }.join

      # @random_password_hash = Digest::MD5.hexdigest(@random_password)
      user.update(password_reset:true, password: @random_password)

      # Insert Mailjet
      variable = Mailjet::Send.create(messages: [{
        'From'=> { 
          'Email'=> "tim.mushen@infovera.com", 'Name'=> "InfoVera" 
        },
        'To'=> [
          { 'Email'=> "#{user.email}", 'Name'=> "#{user.first_name}" + " " + "#{user.last_name}" }
        ],
        'TemplateID'=> 252880,
        'TemplateLanguage'=> true,
        'Subject'=> "Welcome to InfoVera",
        'Variables'=> { 
          "password" => @random_password
        }
      }])
      # End Mailjet

      flash[:success] = "We've sent a new password to your email!"
      redirect '/login/'
    else
      flash[:error] = "We could not find your log in information."
      redirect '/login/forgot/'
    end
  rescue Exception => e
    flash[:error] = "There are some errors, please try again! #{e}"
    Bugsnag.notify(RuntimeError.new(e))
    redirect "/"
  end
end  

########################################################
#
# Reset Password
#
########################################################
get '/login/reset/' do
  begin
    erb :"../modules/auth/views/password_reset", :layout => :login_layout
  rescue Exception => e
    flash[:error] = "There are some errors, please try again! #{e}"
    Bugsnag.notify(RuntimeError.new(e))
    redirect "/"
  end
end

########################################################
#
# Reset Password Process
#
########################################################
post '/login/reset/' do 
  begin
    user = AffiliateUser.first(email: session[:email])
    user.update(password: params[:post]['password'], password_reset: false)
    flash[:success] = "Your password has been updated."
    redirect '/'
  rescue Exception => e
    Bugsnag.notify(RuntimeError.new(e))
    flash[:error] = "There are some errors, please try again! #{e}"
    redirect '/'
  end
end

########################################################
#
# Logout
#
########################################################
get '/logout/?' do
  x_logout!
  flash[:success] = "You've been logged out."
  redirect '/login/'
end