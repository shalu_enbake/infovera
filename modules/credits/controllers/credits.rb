########################################################
#
#  Credits
#
########################################################
get "/credits/?" do
	begin
		erb :"../modules/credits/views/index"
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/"
	end
end