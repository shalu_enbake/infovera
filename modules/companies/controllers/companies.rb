########################################################
#
#  Companies
#
########################################################
get "/companies/?" do
	begin
		@affiliate = Affiliate.get(session[:affiliate_id])
		@companies = @affiliate.affiliate_relationships(company_approved: true, deleted:false).companies(deleted: false)
		@page_title = "Companies"
		erb :"../modules/companies/views/index"
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/"
	end
end

########################################################
#
#  All Companies
#
########################################################
get "/companies/all/?" do
	begin
		@page_title = "Companies"
		if session[:first_company_searched] == true
			user_badge = AffiliateUser.first(id: session[:affiliate_user_id]).user_badge
			user_badge.update(first_company_searched: true)
			unless user_badge.errors.count == 0
				flash[:error] = user_badge.errors.full_messages.flatten.join(', ')
			else
				Achievement.create(achievement: "First Company searched complete successfully.", date: DateTime.now, affiliate_user_id: session[:affiliate_user_id])
			end
		end

		if session[:relation_count].present?
			arr = [1, 3, 5, 10, 20, 30, 50]
			if arr.include?(session[:relation_count].to_i)
				@relation_count = session[:relation_count].to_i
				Achievement.create(achievement: "#{@relation_count} companies added into Affiliate.", date: DateTime.now, affiliate_user_id: session[:affiliate_user_id])
				session[:relation_count] = nil
			end
		end
		erb :"../modules/companies/views/all"
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/"
	end
end

########################################################
#
#  Add Company
#
########################################################
get "/companies/add/?" do
	begin
		@company = Company.new
		@naics_sectors = NaicsSector.all(deleted: false, active: true, order: [ :code.asc ])
		naics_sector_code = @naics_sectors.first.code rescue nil
		@naics_sub_sectors = NaicsSubSector.all(:code.like => "#{naics_sector_code.to_i}%", deleted: false, active: true, order: [ :code.asc ])
		naics_sub_sector_code = @naics_sub_sectors.first.code rescue nil 
		@naics_industries = NaicsIndustry.all(:code.like => "#{naics_sub_sector_code.to_i}%", deleted: false, active: true, order: [ :code.asc ])
		@page_title = "Add Company"
		erb :"../modules/companies/views/add"
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/"
	end
end

########################################################
#
#  Add Company Post
#
########################################################
post "/companies/add/?" do 
	begin
		params[:post][:affiliate_id] = session[:affiliate_id]
		@pending_company = PendingCompany.create(params[:post])
		naics_sector = params[:post]['naics'].to_s[0..1] rescue nil
		# Send email to Karla
		variable = Mailjet::Send.create(messages: [{
			'From'=> { 
				'Email'=> "tim.mushen@infovera.com", 'Name'=> "InfoVera" 
			},
			'To'=> [
				{ 'Email'=> "karla.boles@infovera.com" }
			],
			'TemplateID'=> 256962,
			'TemplateLanguage'=> true,
			'Subject'=> "New Company created.",
			'Variables'=> { 
				"companyname" => params[:post][:name],
				"naic" => params[:post][:naics],
				"naics_sector" => naics_sector,
				"address" => params[:post][:address_1] + "," + params[:post][:address_2] + "," + params[:post][:city] + "," + params[:post][:state],
				"zip_code" => params[:post][:zip],
				"phone" => params[:post][:phone],
				"website" => params[:post][:website],
				"contact_phone" => params[:post][:contact_phone],
				"contact_email" => params[:post][:contact_email],
				"contact_name" => params[:post][:contact_first_name] + " " + params[:post][:contact_last_name],
				"revenue" => params[:post][:revenue],
				"employees" => params[:post][:employees],
				"ebitda" => params[:post][:ebitda],
			}
		}])

		flash[:success] = "We've recieved the message and we will be reviewing the information."
		redirect "/companies/"

	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/"
	end
end

########################################################
#
#  Edit Company
#
########################################################
get "/companies/edit/:x_id/?" do 
	begin
		@company = Company.first(x_id: params[:x_id])
		@page_title = "Edit Company"
		erb :"../modules/companies/views/add"
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/"
	end
end

########################################################
#
#  Edit Company Post
#
########################################################
post "/companies/edit/:x_id/?" do 
	begin
		@company = Company.first(x_id: params[:x_id])
		@company = @company.update(params[:post])
		flash[:notice] = "Company Updated."
		redirect "/companies"
	rescue Exception => e
		Bugsnag.notify(RuntimeError.new(e))
		flash[:error] = "There are some errors, please try again! #{e}"
		redirect "/companies/"
	end
end

########################################################
#
#  Company Delete
#
########################################################
get "/companies/delete/:x_id/?" do 
	begin
		@company = Company.first(x_id: params[:x_id])
		@company.update(deleted: true, deleted_at: Time.now)
		flash[:error] = "company Deleted"
		redirect "/companies/"
	rescue Exception => e
		Bugsnag.notify(RuntimeError.new(e))
		flash[:error] = "There are some errors, please try again! #{e}"
		redirect "/companies/"
	end
end

########################################################
#
#  Find Naic
#
########################################################
post "/companies/find_naics/?" do 
	content_type :json
	begin
		@naics = Naic.all(:code.like => "%#{params[:term]}%")
		{naics: @naics, status: 200}.to_json
	rescue Exception => e
		Bugsnag.notify(RuntimeError.new(e))
		{message: "There are some error, Please try again!", status: 500}.to_json
	end
end

########################################################
#
#  Find Naic sectors
#
########################################################
post "/companies/find_naic_sectors/?" do 
	content_type :json
	begin
		if params[:term].present?
			@naic = Naic.last(code: "#{params[:term]}")
			sector_code = @naic.code[0, 2]
			@naics_sectors = NaicsSector.all(:code.like => "%#{sector_code.to_i}%", deleted: false, active: true, order: [ :code.asc ])
			naics_sector_code = @naics_sectors.first.code
			@naic_sub_sectors = NaicsSubSector.all(:code.like => "#{naics_sector_code.to_i}%", deleted: false, active: true, order: [ :code.asc ])
			naics_sub_sector_code = @naic_sub_sectors.first.code
			@naic_industries = NaicsIndustry.all(:code.like => "#{naics_sub_sector_code.to_i}%", deleted: false, active: true, order: [ :code.asc ])
			# {naics_sectors: @naics_sectors, naic_sub_sectors: @naic_sub_sectors, naic_industries: @naic_industries, status: 200}.to_json
			{naics_sectors: @naics_sectors.last, naic_sub_sectors: @naic_sub_sectors, naic_industries: @naic_industries, status: 200}.to_json
		elsif params[:naics_sector_id].present?
			naics_sector_code = NaicsSector.get(params[:naics_sector_id]).code
			naic_sub_sectors = NaicsSubSector.all(:code.like => "#{naics_sector_code.to_i}%", deleted: false, active: true, order: [ :code.asc ])
			naics_sub_sector_code = naic_sub_sectors.first.code
			naic_industries = NaicsIndustry.all(:code.like => "#{naics_sub_sector_code.to_i}%", deleted: false, active: true, order: [ :code.asc ])
			{naic_sub_sectors: naic_sub_sectors, naic_industries: naic_industries, status: 200}.to_json
		else
			naics_sub_sector_code = NaicsSubSector.get(params[:naics_sub_sector_id]).code
			naic_industries = NaicsIndustry.all(:code.like => "#{naics_sub_sector_code.to_i}%", deleted: false, active: true, order: [ :code.asc ])
			{naic_industries: naic_industries, status: 200}.to_json
		end
	rescue Exception => e
		Bugsnag.notify(RuntimeError.new(e))
		{message: "There are some error, Please try again!", status: 500}.to_json
	end
end

########################################################
#
# Send Company relation requests to EUP user
#
########################################################
get '/companies/send_request/:x_id/?' do
	begin
		company = Company.first(x_id: params[:x_id])
		company ||= Company.new
		user = AffiliateUser.first(id: session[:affiliate_user_id])
		affiliate = Affiliate.get(session[:affiliate_id])
		@affiliate_relationship = AffiliateRelationship.all(affiliate_id:affiliate.id, company_id: company.id).last
		unless @affiliate_relationship.present?
			AffiliateRelationship.create(affiliate_id:affiliate.id, company_id: company.id, is_form_affiliate: true, affiliate_requested: true, affiliate_requested_date: Time.now)
			relation_count = AffiliateRelationship.all(affiliate_id: session[:affiliate_id], is_form_affiliate: true).count
			session[:relation_count] = relation_count
			flash[:success] = "We've sent a new relation request to #{company.name}."
		else 
			if @affiliate_relationship.company_approved?
				flash[:success] = "#{company.name} is already connected."
			else
				flash[:success] = "You've already sent a relation request to #{company.name}."
			end
		end
		redirect '/companies/all/?'
	rescue Exception => e
		Bugsnag.notify(RuntimeError.new(e))
		flash[:error] = "There are some errors, please try again! #{e}"
		redirect "/companies/all/?"
	end
end

########################################################
#
#  Check company name exist
#
########################################################
post "/companies/check_company/?" do 
	content_type :json
	begin
		if params[:name].present?
			@company = Company.last(name: params[:name])
		else
			@company = Company.last(address_1: params[:address])
		end
		if @company.present?
			{company: @company, status: 200}.to_json
		else
			{company: @company, status: 204}.to_json
		end
	rescue Exception => e
		Bugsnag.notify(RuntimeError.new(e))
		{message: "There are some error, Please try again!", status: 500}.to_json
	end
end


# show tab senario page
get "/companies/:x_id/tevo_details" do
	begin
		@company = Company.first(x_id: params[:x_id])
		@score_details = @company.scores.aggregate(:assets.sum,:revenue.sum,:total_liabilities.sum)
		url = "#{Sinatra::Application.settings.api_url}/v1/scores/tevo/"
		@result = HTTParty.post(url, 
			:body => { 
				:naics => @company.naics,
				:revenue => @score_details[1] ||= 0,
				:assets => @score_details[0] ||= 0
		})

		if @result['status'] == "error"
			flash[:error] =  "#{@result['message']}"
			redirect "/companies"
		else
			flash[:success] = "Tevo Score calculations is done"
			@tevo = @result['score']['tevo']
			erb :"../modules/companies/views/tevo/tevo_details"
		end
	rescue Exception => e
		flash[:error] = e
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/"
	end
end

#send search mail
post "/companies/all/request_list/" do
	begin
		user = AffiliateUser.get(session[:affiliate_user_id])
		index = Algolia::Index.new(Sinatra::Application.settings.company_index)
		
		if params[:query][:facetFilters].present?
			facetFiltersArray = []
			params[:query][:facetFilters].each do |key, value|
				facetFiltersArray << value
			end
			params[:query][:facetFilters] = facetFiltersArray
		end
		res = index.browse(params[:query])
		if res["hits"].present?
			mail_encoded = Base64.encode64(res["hits"].to_json)
		end
		variable = Mailjet::Send.create(messages: [{
			'From'=> {
				'Email'=> "tim.mushen@infovera.com",
				'Name'=> "InfoVera"
			},
			'To'=> [
			{
				'Email'=> user.email,
				'Name'=> user.first_name + " " + user.last_name
			}
			],
			'Subject'=> 'Filter Queries and filter data file.',
			'TextPart'=> 'Hello ' + user.first_name + ' ' + user.last_name + ', We send you filter data and filter query. Please find attached files.',
			'HTMLPart'=> '<h3>Hello ' + user.first_name + ' ' + user.last_name + ',</h3><br />We send you filter data and filter query. Please find attached files.',
			'Attachments'=> [
				{
					'ContentType'=> 'text/plain',
					'Filename'=> 'companies.json',
					'Base64Content'=> mail_encoded
				},
				{
					'ContentType'=> 'text/plain',
					'Filename'=> 'query.json',
					'Base64Content'=> Base64.encode64(params[:query].to_json)
				}
			]
		}])
		flash[:success] = "We have send filtered data over mail, please check your inbox."
	rescue Exception => e
		Bugsnag.notify(RuntimeError.new(e))
		flash[:success] = "There are some errors, Please try again! #{e}"
	end
end
