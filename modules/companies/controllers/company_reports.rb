########################################################
#
#  Company Reports
#
########################################################
get "/companies/:x_id/reports/?" do
	begin
		@company = Company.first(x_id: params[:x_id])
		@reports = @company.reports.all(deleted: false)
		@page_title = "#{@company.name rescue nil} Reports"
		erb :"../modules/companies/views/company_reports/reports"
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/"
	end
end


########################################################
#
#  Add Company Reports
#
########################################################
get "/companies/:x_id/reports/add/?" do
	begin
		@company = Company.first(x_id: params[:x_id])
		@report = @company.tevo_reports.new
		@action = "/companies/#{params[:x_id]}/reports/add/?"
		erb :"../modules/companies/views/company_reports/form"
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/"
	end
end

########################################################
#
#  Add Company Data Post
#
########################################################
post "/companies/:x_id/reports/add/?" do
	begin
		@company = Company.first(x_id: params[:x_id])
		@report = Report.create(params[:post])
		unless @report.errors.count == 0
			flash[:error] = @report.errors.full_messages.flatten.join(', ')
			redirect "/companies/#{params[:x_id]}/reports/add/"
		else
			flash[:success] = "Report Added"
		  redirect "/companies/#{params[:x_id]}/reports/"
		end
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/#{params[:x_id]}/reports/"
	end
end

########################################################
#
#  Company Data upload Modal
#
########################################################
get "/companies/:x_id/reports/upload/" do
	begin
		@company = Company.first(x_id: params[:x_id])
		@report = @company.reports.new
		@score_id = @company.scores.last.id
		@action = "/companies/#{params[:x_id]}/reports/upload/"
		erb :"../modules/companies/views/company_reports/upload", :layout => :modal_layout
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/#{params[:x_id]}/reports/"
	end
	
end

########################################################
#
#  Add Company Data Post
#
########################################################
post "/companies/:x_id/reports/upload/?" do
	begin
		@company = Company.first(x_id: params[:x_id])
		x_s3_init
		x_s3_upload(new_filename: "#{SecureRandom.urlsafe_base64}.pdf", file: "#{params[:filename][:tempfile]}", bucket: "#{Platformx.configuration.aws_bucket}", path: "reports/#{company_id}")
		@report = Report.create(params[:post])
		unless @report.errors.count == 0
			flash[:error] = @report.errors.full_messages.flatten.join(', ')
		else
			flash[:success] = "Report Uploaded"
		end
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
	end
end


########################################################
#
#  Edit Company Data
#
########################################################
get "/companies/:x_id/reports/edit/:id/?" do
	begin
		@company = Company.first(x_id: params[:x_id])
		@report = @company.tevo_reports.first(id: params[:id])
		@page_title = "Edit Report"
		@action = "/companies/#{params[:x_id]}/reports/edit/#{params[:id]}"
		erb :"../modules/companies/views/company_reports/form"
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/#{params[:x_id]}/reports/"
	end
end

########################################################
#
#  Edit Company Data Post
#
########################################################
post "/companies/:x_id/reports/edit/:id/?" do
	begin
		@report = Report.first(id: params[:id])
		@report.update(params[:post])
		unless @report.errors.count == 0
			flash[:error] = @report.errors.full_messages.flatten.join(', ')
			redirect "/companies/#{params[:x_id]}/reports/edit/#{params[:id]}"
		else
			flash[:success] = "Report Updated."
			redirect "/companies/#{params[:x_id]}/reports/"
		end
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/#{params[:x_id]}/reports/"
	end
end

########################################################
#
#  Company Data Delete
#
########################################################
get "/companies/:x_id/reports/delete/:id/?" do
	begin
		@report = Report.first(id: params[:id])
		@report.update(deleted: true, deleted_at: Time.now)
		flash[:error] = "Report Deleted"
		redirect "/companies/#{params[:x_id]}/reports/"
	rescue Exception => e
		flash[:error] = "There are some errors, please try again!"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/#{params[:x_id]}/reports/"
	end
end


########################################################
#
#  Company Data Details
#
########################################################
get "/companies/:x_id/reports/:id/?" do
	begin
		@company = Company.first(x_id: params[:x_id])
		@report = Report.first(id: params[:id])
		erb :"../modules/companies/views/company_reports/report_details"
	rescue Exception => e
		flash[:error] = "There are some errors, please try again!"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/#{params[:x_id]}/reports/"
	end
end

