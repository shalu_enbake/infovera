########################################################
#
#  Company Detail
#
########################################################
get "/companies/:x_id" do
	begin
		@company = Company.first(x_id: params[:x_id])
		@tevo = @company.scores.last.tevo rescue nil
		@tevo ||= 0 
		erb :"../modules/companies/views/detail"
	rescue Exception => e
		flash[:error] = "There are some errors, Please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/"
	end
end

########################################################
#
#  Company Contacts
#
########################################################
get "/companies/:x_id/contacts/?" do 
	@company = Company.first(x_id: params[:x_id])
	begin
		@contacts = @company.company_users.all(deleted: false)
		if session[:first_company_user_created] == true && @company.company_users.count == 1
			@first_company_user_created_alert = true
			user_badge = AffiliateUser.first(id: session[:affiliate_user_id]).user_badge
			user_badge.update(first_company_user_created: true)
			unless user_badge.errors.count == 0
				flash[:error] = user_badge.errors.full_messages.flatten.join(', ')
			else
				session[:first_company_user_created] = nil
				Achievement.create(achievement: "First company user created under #{@company.name}.", date: DateTime.now, affiliate_user_id: session[:affiliate_user_id])
			end
		end

		@page_title = "#{@company.name rescue nil} Contacts"
		erb :"../modules/companies/views/contacts"
	rescue Exception => e
		flash[:error] = "There are some errors, Please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/"
	end
end

get "/companies/:x_id/contacts/add" do
	begin
		@company = Company.first(x_id: params[:x_id])
		@contact = CompanyUser.new
		@action = "/companies/#{@company.x_id}/contacts/add"
		erb :"../modules/companies/views/add_contact"
	rescue Exception => e
		flash[:error] = "There are some errors, Please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/#{@company.x_id}/contacts/"
	end
end

post "/companies/:x_id/contacts/add" do
	begin
		@company = Company.first(x_id: params[:x_id])
		params[:post][:password] = SecureRandom.hex(4)
		@contact = @company.company_users.create(params[:post])
		unless @contact.errors.count == 0
			flash[:error] = @contact.errors.full_messages.flatten.join(', ')
			redirect "/companies/#{@company.x_id}/contacts/add"
		else
			flash[:success] = "Company Contact Added"
			redirect "/companies/#{@company.x_id}/contacts"
		end
	rescue Exception => e
		Bugsnag.notify(RuntimeError.new(e))
		flash[:error] = "There are some errors, Please try again! #{e}"
		redirect "/companies/#{@company.x_id}/contacts/"
	end
end

########################################################
#
#  Company Profile
#
########################################################
get "/companies/:x_id/profile/?" do 
	@company = Company.first(x_id: params[:x_id])
	begin
		@profile = @company.company_users.all(deleted: false)
		@naics_sectors = NaicsSector.all(order: :name)
		erb :"../modules/companies/views/profile"
	rescue Exception => e
		flash[:error] = "There are some errors, Please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/"
	end
end

########################################################
#
#  Company Affiliates
#
########################################################
get "/companies/:x_id/affiliates/?" do 
	@company = Company.first(x_id: params[:x_id])
	begin
		@affiliate_relationships = @company.affiliate_relationships
		@page_title = "#{@company.name rescue nil} Affiliates"
		erb :"../modules/companies/views/affiliates"
	rescue Exception => e
		flash[:error] = "There are some errors, Please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/"
	end
end

########################################################
#
#  Company Alerts
#
########################################################
get "/companies/:x_id/alerts/?" do 
	begin
		@company = Company.first(x_id: params[:x_id])
		@alerts = @company.alerts.all(deleted: false, limit: 10, order: [ :created_at.desc ])
		# @alerts = Alert.all(deleted: false, limit: 10, order: [ :created_at.desc ])
		erb :"../modules/companies/views/alerts"
	rescue Exception => e
		flash[:error] = "There are some errors, Please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/"
	end
end

########################################################
#
#  Company Finance
#
########################################################
get "/companies/:x_id/finance/?" do 
	begin
		@company = Company.first(x_id: params[:x_id])
		@finances = @company.scores.company_finances
		@page_title = "#{@company.name rescue nil} Finance"
		erb :"../modules/companies/views/finance"
	rescue Exception => e
		flash[:error] = "There are some errors, Please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/"
	end
end

########################################################
#
#  Company Base
#
########################################################
get "/companies/:x_id/base/?" do
	begin
		@company = Company.first(x_id: params[:x_id])
		@base_scores = @company.scores.company_base_scores
		@sum_of_score = @base_scores.sum(:company_score)
		@sum_of_possible = @base_scores.sum(:possible)
		@sum_of_impact_zone = @base_scores.sum(:impact_zone)
		erb :"../modules/companies/views/base"
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/#{params[:x_id]}"
	end
end

########################################################
#
#  Company Base add
#
########################################################
get "/companies/:x_id/base/add/?" do
	begin
		@company = Company.first(x_id: params[:x_id])
		@base_score = @company.scores.company_base_scores.new
		@action = "/companies/#{params[:x_id]}/base/add/"
		erb :"../modules/companies/views/add_base_score"
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/#{params[:x_id]}"
	end
end

########################################################
#
#  Company Base add
#
########################################################
post "/companies/:x_id/base/add/?" do
	begin
		@company = Company.first(x_id: params[:x_id])
		@base_score = @company.scores.last.company_base_scores.create(params[:post])
		unless @base_score.errors.count == 0
			flash[:error] = @base_score.errors.full_messages.flatten.join(', ')
			@action = "/companies/#{params[:x_id]}/base/add/"
			redirect "/companies/#{params[:x_id]}/base/add"
		else
			flash[:success] = "Base Score Added"
	    redirect "/companies/#{params[:x_id]}/base/"
		end
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/#{params[:x_id]}"
	end
end

########################################################
#
#  Company Systems
#
########################################################
get "/companies/:x_id/systems/?" do
	begin
		@company = Company.first(x_id: params[:x_id])
		@page_title = "#{@company.name rescue nil} Systems"
		erb :"../modules/companies/views/systems"
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/#{params[:x_id]}"
	end
end

########################################################
#
#  Company Management
#
########################################################
get "/companies/:x_id/management/?" do
	begin
		@company = Company.first(x_id: params[:x_id])
		@page_title = "#{@company.name rescue nil} Management"
		erb :"../modules/companies/views/management"
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/#{params[:x_id]}"
	end
end

########################################################
#
#  Company Growth
#
########################################################
get "/companies/:x_id/growth/?" do
	begin
		@company = Company.first(x_id: params[:x_id])
		@page_title = "#{@company.name rescue nil} Growth"
		erb :"../modules/companies/views/growth"
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/#{params[:x_id]}"
	end
end

########################################################
#
#  Company Data
#
########################################################
get "/companies/:x_id/data/?" do
	begin
		@company = Company.first(x_id: params[:x_id])
		@page_title = "#{@company.name rescue nil} Data"
		erb :"../modules/companies/views/data"
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/#{params[:x_id]}"
	end
end
