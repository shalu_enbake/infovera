########################################################
#
#  Company Data index
#
########################################################
get "/companies/:x_id/company_data/?" do 
	begin
		@company = Company.first(x_id: params[:x_id])
		@company_data = @company.company_datas.all(deleted: false)
		@page_title = "#{@company.name rescue nil} Data"
		erb :"../modules/companies/views/data/index"
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/"
	end
end


########################################################
#
#  Add Company Data Level 1
#
########################################################
get "/companies/:x_id/company_data/add_level1/?" do 
	begin
		@company = Company.first(x_id: params[:x_id])
		@company_data = @company.company_datas.new
		@action = "/companies/#{params[:x_id]}/company_data/add_level1/?"
		erb :"../modules/companies/views/data/add_level_form", layout: :modal_layout
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/"
	end
end

########################################################
#
#  Add Company Data Level 1 Post
#
########################################################
post "/companies/:x_id/company_data/add_level1/?" do 
	begin
		@company = Company.first(x_id: params[:x_id])
		@company_data = @company.company_datas.create(params[:post])
		unless @company_data.errors.count == 0
			flash[:error] = @company_data.errors.full_messages.flatten.join(', ')
		else
			flash[:success] = "Data Added"
		end
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
	end
end


########################################################
#
#  Add Company Data Level 2
#
########################################################
get "/companies/:x_id/company_data/add_level2/?" do 
	begin
		@company = Company.first(x_id: params[:x_id])
		@company_data = @company.company_datas.new
		@action = "/companies/#{params[:x_id]}/company_data/add_level2/?"
		erb :"../modules/companies/views/data/add_level2_form", layout: :modal_layout
	rescue Exception => e
		flash[:error] = "There are some errors, please try again!"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies"
	end
end

########################################################
#
#  Add Company Data Level 2 Post
#
########################################################
post "/companies/:x_id/company_data/add_level2/?" do
	begin
		@company = Company.first(x_id: params[:x_id])
		@company_data = @company.company_datas.create(params[:post])
		unless @company_data.errors.count == 0
			flash[:error] = @company_data.errors.full_messages.flatten.join(', ')
		else
			flash[:success] = "Data Added"
		end
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
	end
end


########################################################
#
#  Add Company Data
#
########################################################
get "/companies/:x_id/company_data/add/?" do 
	begin
		@company = Company.first(x_id: params[:x_id])
		@company_data = @company.company_datas.new
		@action = "/companies/#{params[:x_id]}/company_data/add/?"
		erb :"../modules/companies/views/data/form"
	rescue Exception => e
		flash[:error] = "There are some errors, please try again!"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/"
	end
end

########################################################
#
#  Add Company Data Post
#
########################################################
post "/companies/:x_id/company_data/add/?" do
	begin
		@company = Company.first(x_id: params[:x_id])
		@company_data = @company.company_datas.create(params[:post])
		unless @company_data.errors.count == 0
			flash[:error] = @company_data.errors.full_messages.flatten.join(', ')
		else
			flash[:success] = "Data Added"
		end
		redirect "/companies/#{params[:x_id]}/company_data"
	rescue Exception => e
		flash[:error] = "There are some errors, please try again!"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/"
	end
end

########################################################
#
#  Edit Company Data
#
########################################################
get "/companies/:x_id/company_data/edit/:id/?" do 
	begin
		@company = Company.first(x_id: params[:x_id])
		@company_data = CompanyData.first(x_id: params[:id])
		@page_title = "Edit Data"
		@action = "/companies/#{params[:x_id]}/company_data/edit/#{params[:id]}"
		erb :"../modules/companies/views/data/form"
	rescue Exception => e
		flash[:error] = "There are some errors, please try again!"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/#{params[:x_id]}/company_data"
	end
end

########################################################
#
#  Edit Company Data Post
#
########################################################
post "/companies/:x_id/company_data/edit/:id/?" do
	begin
		@company_data = CompanyData.first(x_id: params[:id])
		@company_data.update(params[:post])
		unless @company_data.errors.count == 0
			flash[:error] = @company_data.errors.full_messages.flatten.join(', ')
			redirect "/companies/#{params[:x_id]}/company_data/edit/#{params[:id]}"
		else
			flash[:notice] = "Data Updated."
			redirect "/companies/#{params[:x_id]}/company_data/"
		end
	rescue Exception => e
		flash[:error] = "There are some errors, please try again!"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/#{params[:x_id]}/company_data/edit/#{params[:id]}"
	end
end

########################################################
#
#  Company Data Delete
#
########################################################
get "/companies/:x_id/company_data/delete/:id/?" do 
	begin
		@company_data = CompanyData.first(x_id: params[:id])
		@company_data.update(deleted: true, deleted_at: Time.now)
		flash[:error] = "Company Data Deleted"
		redirect "/companies/#{params[:x_id]}/company_data"
	rescue Exception => e
		flash[:error] = "There are some errors, please try again!"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/#{params[:x_id]}/company_data"
	end
end


########################################################
#
#  Company Data Details
#
########################################################
get "/companies/:x_id/company_data/:id/?" do 
	begin
		@company_data = CompanyData.first(x_id: params[:id])
		erb :"../modules/companies/views/data/details"
	rescue Exception => e
		flash[:error] = "There are some errors, please try again!"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/#{params[:x_id]}/company_data"
	end
end