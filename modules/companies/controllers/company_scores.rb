########################################################
#
#  Company Score
#
########################################################
get "/companies/:x_id/score/?" do
	begin
		@company = Company.first(x_id: params[:x_id])
		@page_title = "#{@company.name rescue nil} Score"
		erb :"../modules/companies/views/score"
	rescue Exception => e
		flash[:error] = "Company not found with x_id=#{params[:x_id]}."
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/"
	end
end

########################################################
#
#  Company Score all
#
########################################################
get "/companies/:x_id/score/all/?" do
	begin
		@affiliate = Affiliate.get(session[:affiliate_id])
		@user = AffiliateUser.first(email: session[:email])
		@company = Company.first(x_id: params[:x_id])
		if session[:first_score_created] == true
			score_counts = (Score.all(deleted: false, level: (1..2), affiliate_user_id: @user.id) + Score.all(deleted: false, level: (4..5), affiliate_user_id: @user.id)).count
			if score_counts == 1
				@score_counts = true
				user_badge = @user.user_badge
				user_badge.update(first_score_created: true)
				unless user_badge.errors.count == 0
					flash[:error] = user_badge.errors.full_messages.flatten.join(', ')
				else
					session[:first_score_created] = nil
					Achievement.create(achievement: "First Score is created.", date: DateTime.now, affiliate_user_id: session[:affiliate_user_id])
				end
			end
		end
		@page_title = "#{@company.name rescue nil} Score"
		erb :"../modules/companies/views/score/all"
	rescue Exception => e
		flash[:error] = "There are some errors, please try again!"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/"
	end
end

########################################################
#
#  Add L1
#
########################################################
get "/companies/:x_id/score/base" do
	begin
		@company = Company.first(x_id: params[:x_id])
		@page_title = "#{@company.name rescue nil} Score"
		@action = "/companies/#{params[:x_id]}/score/base"
		erb :"../modules/companies/views/score/base"
	rescue Exception => e
		flash[:error] = "There are some errors, please try again!"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/"
	end
end

########################################################
#
#  Calc L1
#
########################################################
post "/companies/:x_id/score/base" do
	@company = Company.first(x_id: params[:x_id])

	params[:post]['revenue'] ||= 0
	params[:post]['assets'] ||= 0
	begin
	url = "#{settings.api_url}/v1/scores/tevo/"
	@result = HTTParty.post(url, 
    :body => { 
    	:naics => @company.naics,
    	:revenue => params[:post]['revenue'],
    	:assets => params[:post]['assets']
    })

	if @result['status'] == "error"
		flash[:error] =  "#{@result['message']}"
		redirect "/companies/#{params[:x_id]}/score/base"
	else
	# Insert into Score
	@score = Score.create(
		company_id: @company.id,
		level: 1,
		revenue: params[:post]['revenue'],
		assets: params[:post]['assets'],
		naics: @company.naics, 
		score_date: Time.now, 
		naics_sector: @result["score"]["naics_sector"],
		sector_asset_class_key: @result["score"]["sector_asset_class_key"],
		sac_intercept_estimate: @result["score"]["sac_intercept_estimate"],
		sac_intercept: @result["score"]["sac_intercept"],
		log_of_revenue: @result["score"]["log_of_revenue"],
		asset_lookup_value: @result["score"]["asset_lookup_value"],
		asset_class: @result["score"]["asset_class"],
		sector_asset_class_combo: @result["score"]["sector_asset_combo"],
		tevo: @result["score"]["tevo"]
	)

	# if @company.contact_email.present?
	# 	send_email = Mailjet::Send.create(
	# 			from_email: "tim.mushen@infovera.com",
	# 			from_name: "tim.mushen@infovera.com",
	# 			subject: "Score Updated",
	# 			"Mj-TemplateID": "217972",
	# 			"Mj-TemplateLanguage": "true",
	# 			# recipients: [{ 'Email'=> "#{@company.contact_email}"}],
	# 			recipients: [{ 'Email'=> "#{@company.contact_email}"}],
	# 			vars: {
	#     			"company" => "#{@company.name rescue nil}",
	    
	# 			}
	# 		)
	# end
	flash[:success] = "Tevo Score: #{@result["score"]["tevo"]}"
	redirect "/companies/#{params[:x_id]}/score/all"

	end
	rescue Exception => e
		flash[:error] = e
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/#{params[:x_id]}/score/base"
	end

end


########################################################
#
#  Add L2
#
########################################################
get "/companies/:x_id/score/l2" do
	begin
		@company = Company.first(x_id: params[:x_id])
		@page_title = "#{@company.name rescue nil} Score"
		@action = "/companies/#{params[:x_id]}/score/l2"
		erb :"../modules/companies/views/score/l2"
	rescue Exception => e
		flash[:error] = "There are some errors, please try again!"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/"
	end
end

########################################################
#
#  Calc L2
#
########################################################
post "/companies/:x_id/score/l2" do
	begin
		@company = Company.first(x_id: params[:x_id])
		@affiliate = Affiliate.get(session[:affiliate_id])

		params[:post]['revenue'] ||= 0
		params[:post]['assets'] ||= 0
		
		params[:post]['rev_emp'] = (params[:post]['revenue'].to_f/params[:post]['employees'].to_f).round(2) rescue nil 
		params[:post]['rev_emp'] ||= 0

		# begin
		url = "#{settings.api_url}/v1/scores/extended/"
		@result = HTTParty.post(url, 
	    :body => { 
	        :naics => @company.naics,
	        :revenue => params[:post]['revenue'],
	        :assets => params[:post]['assets'],
	        :ebitda => params[:post]['ebitda'],
	        :cash => params[:post]['cash'],
	        :ar => params[:post]['ar'],
	        :inventory => params[:post]['inventory'],
	        :total_liabilities => params[:post]['total_liabilities'],
	        :employees => params[:post]['employees'],
	        :rev_emp => params[:post]["rev_emp"],
	    })

		if @result['status'] == "error"
			flash[:error] =  "#{@result['message']}"
			redirect "/companies/#{params[:x_id]}/score/l2"
		else
			# Insert into Score
			@score = Score.create(
				company_id: @company.id,
				level: 2,
				naics: @company.naics,
				tevo: @result["score"]["extended"]["extended_tevo_score"],
				base_tevo: @result["score"]["tevos"]["tevo"],
				tevo_upper:  @result["score"]["multiples"]["industry_metrics"]["tevo_stats"]["max"].to_i,
				tevo_lower: @result["score"]["multiples"]["industry_metrics"]["tevo_stats"]["min"].to_i,
				tevo_min: @result["score"]["extended"]["industry_metrics"]["industry_tevo_max"].to_i,
				tevo_median: @result["score"]["extended"]["industry_metrics"]["industry_tevo_median"].to_i,
				tevo_max: @result["score"]["extended"]["industry_metrics"]["industry_tevo_min"],
				score_date: Time.now, 
				naics_sector: @result["score"]["tevos"]["naics_sector"],
				sector_asset_class_key: @result["score"]["tevos"]["sector_asset_class_key"],
				sac_intercept_estimate: @result["score"]["tevos"]["sac_intercept_estimate"],
				sac_intercept: @result["score"]["tevos"]["sac_intercept"],
				log_of_revenue: @result["score"]["tevos"]["log_of_revenue"],
				asset_lookup_value: @result["score"]["tevos"]["asset_lookup_value"],
				asset_class: @result["score"]["tevos"]["asset_class"],
				sector_asset_class_combo: @result["score"]["tevos"]["sector_asset_combo"],
				revenue: params[:post]['revenue'],
				assets: params[:post]['assets'],
				ar: params[:post]['ar'],
				inventory: params[:post]['inventory'],
				total_liabilities: params[:post]['total_liabilities'],
				number_of_employee: params[:post]['employees'],
				
				# upper_estimate: ,
				# lower_estimate: ,

				multiple_revenue_lower: @result["score"]["multiples"]["multiple_revenue_lower"],
				multiple_revenue_estimate: @result["score"]["multiples"]["multiple_revenue_median"],
				multiple_revenue_upper: @result["score"]["multiples"]["multiple_revenue_upper"],

				multiple_ebitda_lower: @result["score"]["multiples"]["multiple_ebitda_lower"],
				multiple_ebitda_estimate: @result["score"]["multiples"]["multiple_ebitda_median"],
				multiple_ebitda_upper: @result["score"]["multiples"]["multiple_ebitda_upper"],

				multiple_net_income_lower: @result["score"]["multiples"]["multiple_net_income_lower"],
				multiple_net_income_estimate: @result["score"]["multiples"]["multiple_net_income_median"],
				multiple_net_income_upper: @result["score"]["multiples"]["multiple_net_income_upper"],

				multiple_assets_lower: @result["score"]["multiples"]["multiple_assets_lower"],
				multiple_assets_estimate: @result["score"]["multiples"]["multiple_assets_median"],
				multiple_assets_upper: @result["score"]["multiples"]["multiple_assets_upper"],

				percentile_revenue: @result["score"]["extended"]["percent_ranks"]["revenue"],
				percentile_ebitda: @result["score"]["extended"]["percent_ranks"]["ebitda"],
				percentile_cash: @result["score"]["extended"]["percent_ranks"]["cash"],
				percentile_ar: @result["score"]["extended"]["percent_ranks"]["ar"],
				percentile_inventory: @result["score"]["extended"]["percent_ranks"]["inventory"],
				percentile_assets: @result["score"]["extended"]["percent_ranks"]["assets"] ,
				percentile_total_liabilities: @result["score"]["extended"]["percent_ranks"]["total_liabilities"],
				percentile_rev_emp: @result["score"]["extended"]["percent_ranks"]["rev_emp"],

				waterfall_revenue: @result["score"]["extended"]["waterfall"]["revenue"],
				waterfall_ebitda: @result["score"]["extended"]["waterfall"]["ebitda"],
				waterfall_cash: @result["score"]["extended"]["waterfall"]["cash"],
				waterfall_ar: @result["score"]["extended"]["waterfall"]["ar"],
				waterfall_inventory: @result["score"]["extended"]["waterfall"]["inventory"],
				waterfall_total_liabilities: @result["score"]["extended"]["waterfall"]["total_liabilities"],
				waterfall_rev_emp: @result["score"]["extended"]["waterfall"]["rev_emp"],
				waterfall_assets: @result["score"]["extended"]["waterfall"]["assets"],
				affiliate_user_id: session[:affiliate_user_id]
			)
			@url2 = "#{settings.api_url}/v1/reports/l2/generate/"
			@result2 = HTTParty.post(@url2, 
			:body => { 
				:company_id => @company.id,
				:score_id => @score.id,
				:affiliate => @affiliate.id,
			})



			flash[:success] = "Tevo Score: #{@result["score"]["extended"]["extended_tevo_score"]}"

			# Check for credit
			@credit = @affiliate.report_credits.first(deleted: false, used: false) rescue nil 
			unless @credit.nil? 
				@credit.update(
					used: true, 
					score_id: @score.id, 
					date_used: Time.now
				)
			end
			redirect "/companies/#{params[:x_id]}/score/all"
		end
	rescue Exception => e
		flash[:error] = "There are some errors, please try again!"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/"
	end
end


########################################################
#
#  Company Score L1 add
#
########################################################
get "/companies/:x_id/score/manually-add-l1/?" do
	begin
		@company = Company.first(x_id: params[:x_id])
		@score = @company.scores.new
		@action = "/companies/#{params[:x_id]}/score/manually-add-l1/"
		erb :"../modules/companies/views/score/L1_form", :layout => :modal_layout
	rescue Exception => e 
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/"
	end
end


########################################################
#
#  Company Score add post
#
########################################################
post "/companies/:x_id/score/manually-add-l1/?" do
	begin
		@score = Score.create(params[:post]) 

		unless @score.errors.count == 0
			flash[:error] = @score.errors.full_messages.flatten.join(', ')
			redirect "/companies/#{params[:x_id]}/score/all"
		else
			flash[:success] = "Score Added"
	    redirect "/companies/#{params[:x_id]}/score/all"
	  end
	rescue Exception => e 
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/"
	end
end


########################################################
#
#  Company Score L2 add
#
########################################################
get "/companies/:x_id/score/manually-add-l2/?" do
	begin
		@company = Company.first(x_id: params[:x_id])
		@score = @company.scores.new
		@action = "/companies/#{params[:x_id]}/score/manually-add-l2/"
		erb :"../modules/companies/views/score/L2_form", :layout => :modal_layout
	rescue Exception => e 
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/"
	end
end


########################################################
#
#  Company Score add post
#
########################################################
post "/companies/:x_id/score/manually-add-l2/?" do
	begin
		@score = Score.create(params[:post]) 

		unless @score.errors.count == 0
			flash[:error] = @score.errors.full_messages.flatten.join(', ')
			redirect "/companies/#{params[:x_id]}/score/all"
		else
			flash[:success] = "Score Added"
	    redirect "/companies/#{params[:x_id]}/score/all"
	  end
	rescue Exception => e 
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/"
	end
end


########################################################
#
#  Company Score edit
#
########################################################
get "/companies/:x_id/score/edit/:id" do
	begin
		@score = Score.first(x_id: params[:id])
		@company = Company.first(x_id: params[:x_id])
		@action = "/companies/#{params[:x_id]}/score/edit/#{params[:id]}"
		erb :"../modules/companies/views/score/form", :layout => :modal_layout
	rescue Exception => e 
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/"
	end
end
########################################################
#
#  Company Score edit post
#
########################################################
post "/companies/:x_id/score/edit/:id" do
	begin
		@score = Score.first(x_id: params[:id])
		@score.score(params[:post])
		unless @company.errors.count == 0
			flash[:error] = @score.errors.full_messages.flatten.join(', ')
			redirect "/companies/#{params[:x_id]}/score/edit/#{params[:id]}"
		else
			flash[:success] = "Score Updated"
	    	redirect "/companies/#{params[:x_id]}/score/all"
	  end
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/#{params[:x_id]}/score/all"
	end
end
########################################################
#
#  Company Score delete
#
########################################################
get "/companies/:x_id/score/delete/:id" do
	begin
		@score = Score.first(x_id: params[:id]).update(deleted: true, deleted_at: Time.now)

		unless @score == true
			flash[:error] = @score.errors.full_messages.flatten.join(', ')
			redirect "/companies/#{params[:x_id]}/score/all/?"
		else
			flash[:success] = "Score Deleted"
	    redirect "/companies/#{params[:x_id]}/score/all/?"
	  end
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/#{params[:x_id]}/score/all"
	end	
end		
########################################################
#
#  Company Finance
#
########################################################
get "/companies/:x_id/finance/?" do
	begin
		@company = Company.first(x_id: params[:x_id])
		@page_title = "#{@company.name rescue nil} Finance"
		erb :"../modules/companies/views/finance"
	rescue Exception => e
		flash[:error] = "Company not found with x_id=#{params[:x_id]}."
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/"
	end
end

########################################################
#
#  Company Base
#
########################################################
get "/companies/:x_id/base/?" do
	begin
		@company = Company.first(x_id: params[:x_id])
		@page_title = "#{@company.name rescue nil} Base"
		erb :"../modules/companies/views/base"
	rescue Exception => e
		flash[:error] = "Company not found with x_id=#{params[:x_id]}."
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/"
	end
end

########################################################
#
#  Company Systems
#
########################################################
get "/companies/:x_id/systems/?" do
	begin
		@company = Company.first(x_id: params[:x_id])
		@page_title = "#{@company.name rescue nil} Systems"
		erb :"../modules/companies/views/systems"
	rescue Exception => e
		flash[:error] = "Company not found with x_id=#{params[:x_id]}."
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/"
	end
end

########################################################
#
#  Company Management
#
########################################################
get "/companies/:x_id/management/?" do
	begin
		@company = Company.first(x_id: params[:x_id])
		@page_title = "#{@company.name rescue nil} Management"
		erb :"../modules/companies/views/management"
	rescue Exception => e
		flash[:error] = "Company not found with x_id=#{params[:x_id]}."
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/"
	end
end

########################################################
#
#  Company Growth
#
########################################################
get "/companies/:x_id/growth/?" do
	begin
		@company = Company.first(x_id: params[:x_id])
		@page_title = "#{@company.name rescue nil} Growth"
		erb :"../modules/companies/views/growth"
	rescue Exception => e
		flash[:error] = "Company not found with x_id=#{params[:x_id]}."
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/"
	end
end