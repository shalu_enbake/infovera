########################################################
#
#  Company Report Create
#
########################################################
get "/companies/:x_id/generate-report/:score_id/?" do
	begin
		@score = Score.first(x_id: params[:score_id])
		@company = Company.first(x_id: params[:x_id])
		@affiliate = Affiliate.get(session[:affiliate_id])

		# Check if PDF exists for score
		@existing_report = Report.first(score_id: @score.id, company_id: @company.id, affiliate_id: @affiliate.id)
		
		if @existing_report.nil? == true

			# Send call to API to create PDF
			url = "#{settings.api_url}/v1/reports/#{@company.x_id}/#{@score.x_id }/#{@affiliate.x_id}/link"
			@result = HTTParty.get(url) 
			# Forward user to returned S3 URL
			s3_path_link = get_loan_pdf_link_https(s3_path: @result['s3_path'])
		
			# Save to Database
			@report = Report.create(
					date_created: Time.now, 
					tevo: @score.tevo,
					level: @score.level,
					s3_path: @result['s3_path'],
					company_id: @company.id, 
					score_id: @score.id, 
					affiliate_id: @affiliate.id, 
					affiliate_user_id: session["affiliate_user_id"]
				)
			
		else			
			s3_path_link = get_loan_pdf_link_https(s3_path: @existing_report.s3_path)
		end
		redirect s3_path_link
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/"
	end
end