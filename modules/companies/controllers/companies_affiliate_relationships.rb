########################################################
#
#  Add Affiliate Relationship 
#
########################################################
get "/companies/:x_id/affiliate_relationship/add/?" do 
	@company = Company.first(x_id: params[:x_id])
	begin
		@affiliate_relationship = @company.affiliate_relationships.new
		@action = "/companies/#{params[:x_id]}/affiliate_relationship/add"
		@page_title = "Affiliate Relationship"
		erb :"../modules/companies/views/affiliate_relationships"#, layout: :modal_layout
	rescue Exception => e
		Bugsnag.notify(RuntimeError.new(e))
		flash[:error] = "There are some errors, please try again! #{e}"
		redirect "/companies/"
	end
end

########################################################
#
#  Add Affiliate Relationship  Post
#
########################################################
post "/companies/:x_id/affiliate_relationship/add/?" do 
	@company = Company.first(x_id: params[:x_id])
	begin
		@affiliate_relationships = @company.affiliate_relationships.create(params[:post])
		# To Do: Send approval email to company
		redirect "/companies/#{@company.x_id}"
	rescue Exception => e
		Bugsnag.notify(RuntimeError.new(e))
		flash[:error] = "There are some errors, please try again! #{e}"
		redirect "/companies/"
	end
end