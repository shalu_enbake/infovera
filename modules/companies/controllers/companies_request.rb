########################################################
#
#  Company Connect
#
########################################################
get "/companies/connect/:x_id" do
	begin
		@company = Company.first(x__id: params[:x_id])
		@affiliate = Affiliate.get(session[:affiliate_id])
		@page_title = "#{@company.name rescue nil} Connection Request"
		@action = "/companies/connect/#{params[:x_id]}"
		erb :"../modules/companies/views/connect/index" 	
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/"
	end
end

########################################################
#
#  Company Connect
#
########################################################
post "/companies/connect/:x_id" do
	begin
		# Get Company and Affiliate 
		@company = Company.first(x__id: params[:x_id])
		@affiliate = Affiliate.get(session[:affiliate_id])

		# TODO Update database

		# TODO Send Email to Company Contact

		# Forward to Confirmation Page
		redirect "/companies/connect/confirmation/"
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/"
	end
end

########################################################
#
#  Company Connect
#
########################################################
get "/companies/connect/confirmation/" do
	begin
		@page_title = "Connection Confirmed"
		erb :"../modules/companies/views/connect/confirmation"
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/"
	end
end