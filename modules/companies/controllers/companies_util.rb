 ########################################################
#
#  Update Companies
#
########################################################
get "/util/companies-update/?" do 
	begin
		@companies = Company.all 
		@companies.each do |company|
			company.update(updated_at: Time.now)
		end
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/"
	end
end