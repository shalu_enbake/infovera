########################################################
#
#  Academy
#
########################################################
get "/academy/?" do
	begin
		@page_title = "Academy"
		user = AffiliateUser.get(session[:affiliate_user_id])
		if user.user_type == "ProAffiliate"
		  @academies_overview = Academy.all(deleted: false , is_overview: true, :order => [ :priority.asc ])
		  @academies_how = Academy.all(deleted: false , is_how_to: true, :order => [ :priority.asc ])
		else
			@academies_overview = Academy.all(deleted: false , is_overview: true,only_pro_affiliate: false, :order => [ :priority.asc ])
		  @academies_how = Academy.all(deleted: false , is_how_to: true,only_pro_affiliate: false, :order => [ :priority.asc ])
		end

		if session[:first_video_completed] == true && user.academy_affiliateusers.all(completed: true).count == 1
			user_badge = user.user_badge
			user_badge.update(first_video_completed: true)
			unless user_badge.errors.count == 0
				flash[:error] = user_badge.errors.full_messages.flatten.join(', ')
			else
				session[:first_video_completed] = nil
				Achievement.create(achievement: "First video completed.", date: DateTime.now, affiliate_user_id: session[:affiliate_user_id])
			end
		end
		erb :"../modules/academy/views/index"
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/"
	end
end

########################################################
#
#  Academy Webinars
#
########################################################
get "/academy/webinars/?" do
	begin
		@page_title = "Webinar Archive"
		user = AffiliateUser.get(session[:affiliate_user_id])
		if user.user_type == "ProAffiliate"
		  @academies_events = Academy.all(deleted: false , is_event_webinar: true, :order => [ :priority.asc ])
		  @academies_training = Academy.all(deleted: false , is_training_webinar: true, :order => [ :priority.asc ])
		else
		  @academies_events = Academy.all(deleted: false , is_event_webinar: true, only_pro_affiliate: false, :order => [ :priority.asc ])
		  @academies_training = Academy.all(deleted: false , is_training_webinar: true, only_pro_affiliate: false, :order => [ :priority.asc ])
		end

		if session[:first_video_completed] == true && user.academy_affiliateusers.all(completed: true).count == 1
			user_badge = user.user_badge
			user_badge.update(first_video_completed: true)
			unless user_badge.errors.count == 0
				flash[:error] = user_badge.errors.full_messages.flatten.join(', ')
			else
				session[:first_video_completed] = nil
				Achievement.create(achievement: "First video completed.", date: DateTime.now, affiliate_user_id: session[:affiliate_user_id])
			end
		end
		erb :"../modules/academy/views/webinars"
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/"
	end
end

########################################################
#
#  post Academy Modal
#
########################################################
post "/academy/:x_id/completed" do
	begin
		@academy = Academy.first(x_id: params[:x_id])
		@relation = @academy.academy_affiliateusers.create_or_first(affiliate_user_id: session[:affiliate_user_id])
		unless @relation.errors.count == 0
			flash[:error] = @relation.errors.full_messages.flatten.join(', ')
			redirect "/academy/"
		else
			flash[:success] = "Video completed"
		    redirect "/academy/"
		end
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/academy/"
	end
end

post "/acadmey/update_video_status/" do
	begin
		@academy = Academy.first(id: params[:id])
		@affiliateuser = AffiliateUser.get(session[:affiliate_user_id])
		@academy_affiliateuser = AcademyAffiliateuser.first(academy_id: @academy.id ,affiliate_user_id: @affiliateuser.id)

		if @academy_affiliateuser.present? 
			if !@academy_affiliateuser.completed && params[:status] == "completed"
				@academy_affiliateuser.update(completed: true)

				if session[:first_video_completed] == true && @affiliateuser.academy_affiliateusers.all(completed: true).count == 1
					user_badge = @affiliateuser.user_badge
					user_badge.update(first_video_completed: true)
					unless user_badge.errors.count == 0
						flash[:error] = user_badge.errors.full_messages.flatten.join(', ')
					else
						session[:first_video_completed] = nil
						Achievement.create(achievement: "First video completed.", date: DateTime.now, affiliate_user_id: session[:affiliate_user_id])
					end
				end
			end
		else
			@affiliateuser.academy_affiliateusers.create(academy_id: @academy.id ,affiliate_user_id: @affiliateuser.id)
		end
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
	end
end

########################################################
#
#  Academy Resources
#
########################################################
get "/academy/resources/" do
	begin
		@page_title = "Academy Resources"
		if AffiliateUser.get(session[:affiliate_user_id]).user_type == "ProAffiliate"
			@docs = AcademyDocument.all(deleted: false , :order => [ :priority.asc ])
		else
			@docs = AcademyDocument.all(deleted: false, only_pro_affiliate: false, :order => [ :priority.asc ])
		end
		erb :"../modules/academy/views/resources"
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/academy/"
	end
end