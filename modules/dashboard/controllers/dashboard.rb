########################################################
#
#  Dashboard Page
#
########################################################
get "/?" do
	begin
		@affiliate = Affiliate.get(session[:affiliate_id])

		@affiliate_view = @affiliate.affiliate_views.all(:created_at=> ((Date.today).beginning_of_day..(Date.today).end_of_day)).count
		@one_day_before_affiliate_view = @affiliate.affiliate_views.all(:created_at => ((Date.today - 1.days).beginning_of_day..(Date.today-1.days).end_of_day)).count
		@two_day_before_affiliate_view = @affiliate.affiliate_views.all(:created_at => ((Date.today - 2.days).beginning_of_day..(Date.today-2.days).end_of_day)).count
		@three_day_before_affiliate_view = @affiliate.affiliate_views.all(:created_at => ((Date.today - 3.days).beginning_of_day..(Date.today-3.days).end_of_day)).count
		@four_day_before_affiliate_view = @affiliate.affiliate_views.all(:created_at => ((Date.today - 4.days).beginning_of_day..(Date.today-4.days).end_of_day)).count
		@five_day_before_affiliate_view = @affiliate.affiliate_views.all(:created_at => ((Date.today - 5.days).beginning_of_day..(Date.today-5.days).end_of_day)).count
		@six_day_before_affiliate_view = @affiliate.affiliate_views.all(:created_at => ((Date.today - 6.days).beginning_of_day..(Date.today-6.days).end_of_day)).count

		@total_week_affiliate_view = @affiliate_view + @one_day_before_affiliate_view + @two_day_before_affiliate_view + @three_day_before_affiliate_view + @four_day_before_affiliate_view + @five_day_before_affiliate_view + @six_day_before_affiliate_view
		@affiliate_companies = @affiliate.affiliate_relationships(company_approved: true).companies(deleted: false).count
		@lvl2_credits = @affiliate.report_credits.all(deleted: false, used: false, report_level: "L2").count
		 "#{@affiliate.inspect} | #{session.inspect}"
		@my_proaffiliate = Affiliate.all(proaffiliate_id: session[:affiliate_id],deleted: false).count if @affiliate.affiliate_type == "Proaffiliate"
	
		@l4_reports = Report.all(level: 4, affiliate_id: @affiliate.id, created_at: (Date.today.beginning_of_month..Date.today.end_of_month)).count
		@l5_reports = Report.all(level: 5, affiliate_id: @affiliate.id, created_at: (Date.today.beginning_of_month..Date.today.end_of_month)).count

		@affiliate_users = @affiliate.affiliate_users.count rescue nil 
		@affiliate_users ||= 0
		@page_title = "Dashboard"
		@alerts = Alert.all(deleted: false, limit: 10,affiliate_id: nil, order: [ :created_at.desc ])
		if session[:sign_in_count].present?
			@sign_in_alert = true
			Achievement.create(achievement: "Affiliate User first login successfully.", date: DateTime.now, affiliate_user_id: session[:affiliate_user_id])
			session[:sign_in_count] = nil
		end

		if Sinatra::Application.environment == :demo
			erb :"../modules/dashboard/views/demo_index"
		else
			erb :"../modules/dashboard/views/index"
		end
	rescue Exception => e
		flash[:error] = "There are some errors. Please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/"
	end
end

########################################################
#
#  Comming Soon Page
#
########################################################
get "/comming_soon/?" do
	begin
		@page_title = "Comming Soon"
		erb :"../modules/dashboard/views/comming_soon"
	rescue Exception => e
		flash[:error] = "There are some errors. Please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/"
	end
end