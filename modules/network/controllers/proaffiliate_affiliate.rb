########################################################
#
#  Pro-Affiliates affiliates Index
#
########################################################
get "/network/pro-affiliates/:x_id/affiliates/?" do 
	begin
		@proaffiliate = Proaffiliate.first(x_id: params[:x_id])
		@affiliates = @proaffiliate.affiliates(deleted: false)
		erb :"../modules/network/views/proaffiliates_affiliates/index"
	rescue Exception => e
		flash[:error] = "There are some errors, Please try again. #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/network/pro-affiliates/"
	end
end

########################################################
#
#  Pro-Affiliates affiliates Add
#
########################################################
get "/network/pro-affiliates/:x_id/affiliates/add/?" do 
	begin
		@proaffiliate = Proaffiliate.first(x_id: params[:x_id])
		@affiliate = @proaffiliate.affiliates.new
		@action = "/network/pro-affiliates/#{params[:x_id]}/affiliates/add/"
		erb :"../modules/network/views/proaffiliates_affiliates/form"
	rescue Exception => e
		flash[:error] = "There are some errors, Please try again. #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/network/pro-affiliates/#{params[:x_id]}/affiliates/"
	end
end

########################################################
#
#  Pro-Affiliates affiliates Add Post
#
########################################################
post "/network/pro-affiliates/:x_id/affiliates/add/?" do 
	begin
		@proaffiliate = Proaffiliate.first(x_id: params[:x_id])

		@affiliate =  @proaffiliate.affiliates.create(params[:post])
		unless @proaffiliate.errors.count == 0
			flash[:error] = @proaffiliate.errors.full_messages.flatten.join(', ')
			redirect "/network/pro-affiliates/#{params[:x_id]}/affiliates/add/"
		else
			flash[:success] = "Affiliate Added"
	    	redirect "/network/pro-affiliates/#{params[:x_id]}/affiliates/"
	  	end
	rescue Exception => e
		flash[:error] = "There are some errors, Please try again. #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/network/pro-affiliates/#{params[:x_id]}/affiliates/"
	end
end

########################################################
#
#  Pro-Affiliates affiliates Edit
#
########################################################
get "/network/pro-affiliates/:id/affiliates/edit/:x_id" do
	begin
		@proaffiliate = Proaffiliate.first(x_id: params[:id])
		@affiliate =  @proaffiliate.affiliates.first(x_id: params[:x_id])
		@action = "/network/pro-affiliates/#{params[:id]}/affiliates/edit/#{params[:x_id]}"
		@page_title = "Edit Affiliate"
		erb :"../modules/network/views/proaffiliates_affiliates/form"
	rescue Exception => e
		flash[:error] = "There are some errors, Please try again. #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/network/pro-affiliates/"
	end
end

########################################################
#
#  Pro-Affiliates affiliates Edit Post
#
########################################################
post "/network/pro-affiliates/:id/affiliates/edit/:x_id" do
	begin
		@proaffiliate = Proaffiliate.first(x_id: params[:id])
		@affiliate =  @proaffiliate.affiliates.first(x_id: params[:x_id])
		@affiliate.update(params[:post])
		unless @affiliate.errors.count == 0
			flash[:error] = @affiliate.errors.full_messages.flatten.join(', ')
			redirect "/network/pro-affiliates/#{params[:id]}/affiliates/edit/#{params[:x_id]}"
		else
			flash[:success] = "Affiliate Updated."
	    redirect "/network/pro-affiliates/#{params[:id]}/affiliates/"
	  end
	rescue Exception => e
		flash[:error] = "There are some errors, Please try again. #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/network/pro-affiliates/#{params[:id]}/affiliates/"
	end
end

########################################################
#
#  Pro-Affiliates affiliates Delete
#
########################################################
get "/network/pro-affiliates/:id/affiliates/delete/:x_id" do
	begin
		@proaffiliate = Proaffiliate.first(x_id: params[:id])
		@affiliate =  @proaffiliate.affiliates.first(x_id: params[:x_id])
		@affiliate.update(deleted: true, deleted_at: Time.now)
		unless @affiliate.errors.count == 0
			flash[:error] = @affiliate.errors.full_messages.flatten.join(', ')
			redirect "/network/pro-affiliates/#{params[:id]}/affiliates/"
		else
			flash[:success] = "Affiliate Deleted"
	    redirect "/network/pro-affiliates/#{params[:id]}/affiliates/"
	  end
	rescue Exception => e
		flash[:error] = "There are some errors, Please try again. #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/network/pro-affiliates/#{params[:id]}/affiliates/"
	end
end


########################################################
#
#  Pro-Affiliates affiliates Details
#
########################################################
get "/network/pro-affiliates/:id/affiliates/:x_id" do 
	begin
		@proaffiliate = Proaffiliate.first(x_id: params[:id])
		@affiliate =  @proaffiliate.affiliates.first(x_id: params[:x_id])
		erb :"../modules/network/views/proaffiliates_affiliates/details"
	rescue Exception => e
		flash[:error] = "There are some errors, Please try again. #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/network/pro-affiliates/#{params[:id]}/affiliates/"
	end
end