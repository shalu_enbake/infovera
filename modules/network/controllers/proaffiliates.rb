########################################################
#
#  Pro-Affiliates Index
#
########################################################
get "/network/pro-affiliates/?" do 
	begin
		@proaffiliates = Proaffiliate.all(deleted: false)
		erb :"../modules/network/views/proaffiliates/index"
	rescue Exception => e
		flash[:error] = "There are some errors, Please try again. #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/network/"
	end
end

########################################################
#
#  Pro-Affiliates Add
#
########################################################
get "/network/pro-affiliates/add/?" do 
	begin
		@proaffiliate = Proaffiliate.new
		@action = "/network/pro-affiliates/add/"
		erb :"../modules/network/views/proaffiliates/form"
	rescue Exception => e
		flash[:error] = "There are some errors, Please try again. #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/network/"
	end
end

########################################################
#
#  Pro-Affiliates Add Post
#
########################################################
post "/network/pro-affiliates/add/?" do 
	begin
		@proaffiliate = Proaffiliate.create(params[:post])
		unless @proaffiliate.errors.count == 0
			flash[:error] = @proaffiliate.errors.full_messages.flatten.join(', ')
			redirect "/network/pro-affiliates/add/"
		else
			flash[:success] = "Affiliate Added"
	    	redirect "/network/pro-affiliates/"
	  	end
	rescue Exception => e
		flash[:error] = "There are some errors, Please try again. #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/network/pro-affiliates/"
	end
end

########################################################
#
#  Pro-Affiliates Edit
#
########################################################
get "/network/pro-affiliates/edit/:x_id" do
	begin
		@proaffiliate = Proaffiliate.first(x_id: params[:x_id])
		@action = "/network/pro-affiliates/edit/#{params[:x_id]}"
		@page_title = "Edit Affiliate"
		erb :"../modules/network/views/proaffiliates/form"
	rescue Exception => e
		flash[:error] = "There are some errors, Please try again. #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/network/pro-affiliates/"
	end
end

########################################################
#
#  Pro-Affiliates Edit Post
#
########################################################
post "/network/pro-affiliates/edit/:x_id" do
	begin
		@proaffiliate = Proaffiliate.first(x_id: params[:x_id])
		@proaffiliate.update(params[:post])
		unless @proaffiliate.errors.count == 0
			flash[:error] = @proaffiliate.errors.full_messages.flatten.join(', ')
			redirect "/network/pro-affiliates/edit/#{params[:x_id]}"
		else
			flash[:success] = "Affiliate Updated."
	    redirect "/network/pro-affiliates/"
	  end
	rescue Exception => e
		flash[:error] = "There are some errors, Please try again. #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/network/pro-affiliates/"
	end
end

########################################################
#
#  Pro-Affiliates Delete
#
########################################################
get "/network/pro-affiliates/delete/:x_id" do
	begin
		@proaffiliate = Proaffiliate.first(x_id: params[:x_id])
		@proaffiliate.update(deleted: true, deleted_at: Time.now)
		unless @proaffiliate.errors.count == 0
			flash[:error] = @proaffiliate.errors.full_messages.flatten.join(', ')
			redirect "/network/pro-affiliates/"
		else
			flash[:success] = "Affiliate Deleted"
	    redirect "/network/pro-affiliates/"
	  end
	rescue Exception => e
		flash[:error] = "There are some errors, Please try again. #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/network/pro-affiliates/"
	end
end

