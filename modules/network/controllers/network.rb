########################################################
#
#  Network
#
########################################################
get "/network/?" do 
	begin
		@profile = Affiliate.get(session[:affiliate_id])
		@user = AffiliateUser.first(email: session[:email])
		@page_title = "My Network"

		if @profile.affiliate_type == "ProAffiliate"
			@affiliate_relationships = Affiliate.all(affiliate_type: "Affiliate", active: true, proaffiliate_id: @profile.id)
		else
			@proaffiliate = Affiliate.first(id: @profile.proaffiliate_id)
			if @proaffiliate.nil? == true 
				@proaffiliate = Affiliate.get(1)
			end
		end
		erb :"../modules/network/views/index"
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/"		
	end

end

########################################################
#
#  Network Connect
#
########################################################
get "/network/connect/:x_id" do
	begin
		erb :"../modules/network/views/connect"
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/network/"
	end
end

########################################################
#
#  Affiliate
#
########################################################
get "/network/affiliates/?" do
	begin
		erb :"../modules/network/views/affiliates"
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/network/"
	end
end

########################################################
#
#  Requests
#
########################################################
get "/network/requests/?" do
	begin
		@affiliate = Affiliate.get(session[:affiliate_id])
		@requests = @affiliate.affiliate_relationships.all(deleted: false, company_approved: false, company_rejected: false, affiliate_approved: false, affiliate_rejected: false)
		
		if session[:first_network_connection_accept] == true
			request_count = @affiliate.affiliate_relationships(company_requested: true, company_approved: true).count
			if request_count == 1
				@first_network_connection_accept_alert = true
				user_badge = AffiliateUser.first(id: session[:affiliate_user_id]).user_badge
				user_badge.update(first_network_connection_accept: true)
				unless user_badge.errors.count == 0
					flash[:error] = user_badge.errors.full_messages.flatten.join(', ')
				else
					session[:first_network_connection_accept] = nil
					Achievement.create(achievement: "First network request approved.", date: DateTime.now, affiliate_user_id: session[:affiliate_user_id])
				end
			end
		end
		erb :"../modules/network/views/requests"
	rescue Exception => e
		flash[:error] = "There are some error, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/network/"
	end
end

########################################################
#
#  Request Approve
#
########################################################
get "/network/requests/:x_id/approve/" do
	begin
		@affiliate_relationship = AffiliateRelationship.first(x_id: params[:x_id])
		@affiliate_relationship.update(company_approved: true, company_approved_date: DateTime.now)
		unless @affiliate_relationship.errors.count == 0
			flash[:error] = @affiliate_relationship.errors.full_messages.flatten.join(', ')
			redirect "/network/requests/"
		else
			if @affiliate_relationship.affiliate.email.present?
				# Insert Mailjet
				variable = Mailjet::Send.create(messages: [{
				'From'=> { 
					'Email'=> "tim.mushen@infovera.com", 'Name'=> "InfoVera" 
				},
				'To'=> [
					{ 'Email'=> @affiliate_relationship.affiliate.email }
				],
				'TemplateID'=> 285543,
				'TemplateLanguage'=> true,
				'Subject'=> "Relationship request approved by Affiliate: #{@affiliate_relationship.affiliate.name} for Company: #{@affiliate_relationship.company.name}",
				'Variables'=> { 
					"affiliate" => @affiliate_relationship.affiliate.name,
					"company" => @affiliate_relationship.company.name,
					"ubi" => (@affiliate_relationship.company.ubi rescue nil)
				}
				}])
				# End Mailjet
			end
			flash[:success] = "Company Approved."
			redirect "/network/requests/"
		end
	rescue Exception => e
		flash[:error] = "There are some error, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/network/"
	end
end

########################################################
#
#  Network Company Remove
#
########################################################
get "/network/remove/:x_id/affiliate_relationship/" do
	begin
		@affiliate_relationship = AffiliateRelationship.first(x_id: params[:x_id])
		@affiliate_relationship.destroy
		unless @affiliate_relationship.errors.count == 0
			flash[:error] = @affiliate_relationship.errors.full_messages.flatten.join(', ')
			redirect "/network/requests/"
		else
			flash[:success] = "Company Relationship Removed."
			redirect "/network/requests/"
		end
	rescue Exception => e
		flash[:error] = "There are some error, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/network/"
	end
end

get "/network/:x_id/company_details/" do
	begin
		affiliate_relationship = AffiliateRelationship.first(x_id: params[:x_id])
		@company = 	affiliate_relationship.company
		erb :"../modules/network/views/company_details", layout: :modal_layout
	rescue Exception => e
		flash[:error] = "There are some error, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/network/"
	end
end



get "/network/requests/delete/:x_id/" do
	begin
		@affiliate_relationship = AffiliateRelationship.first(x_id: params[:x_id])
        @affiliate_relationship.update(deleted:true,deleted_at: Time.now)
        unless @affiliate_relationship.errors.count == 0
        	flash[:error] = @affiliate_relationship.errors.full_messages.flatten.join(', ')
        else
        	flash[:success] = "Affiliate Relationship Removed."
        end
		redirect "/network/requests/"
	rescue Exception => e
		flash[:error] = "There are some error, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/network/requests/"
	end
end