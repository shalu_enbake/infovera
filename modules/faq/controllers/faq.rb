########################################################
#
#  Faq
#
########################################################
get "/faq/?" do
	begin
		@faq_cat = FaqCategory.all(deleted: false, :order => [ :cat_order.asc ])
		@faq = Faq.all(deleted: false, :order => [ :faq_order.asc ])
		erb :"../modules/faq/views/index"
	rescue Exception => e
		flash[:error] = "There are some errors. Please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/"
	end
end