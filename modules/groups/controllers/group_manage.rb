########################################################
#
#  Group Add
#
########################################################
get "/groups/add/?" do 
	begin
		@group = AffiliateGroup.new
		@action = "/groups/add/"
		@page_title = "Add Group"
		erb :"../modules/groups/views/form"
	rescue Exception => e
		flash[:error] = "There are some errors, Please try again."
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/groups/"
	end
end

########################################################
#
#  Groups Add Post
#
########################################################
post "/groups/add/?" do 
	begin
		params[:post]['affiliate_id'] = session[:affiliate_id]
		params[:post]['affiliate_user_id'] = session[:affiliate_user_id]
		@group = AffiliateGroup.create(params[:post])
		unless @group.errors.count == 0
			flash[:error] = @group.errors.full_messages.flatten.join(', ')
			redirect "/groups/add/"
		else
			session[:group_count] = AffiliateGroup.all(affiliate_id: session[:affiliate_id]).count
			flash[:success] = "Group Added"
	    redirect "/groups/"
	  end
	rescue Exception => e
		flash[:error] = "There are some errors, Please try again."
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/groups/"
	end
end

########################################################
#
#  Groups Edit
#
########################################################
get "/groups/edit/:x_id" do
	begin
		@group = AffiliateGroup.first(x_id: params[:x_id])
		@action = "/groups/edit/#{params[:x_id]}"
		@page_title = "Edit Group"
		erb :"../modules/groups/views/form"
	rescue Exception => e
		flash[:error] = "Group not found with x_id=#{params[:x_id]}."
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/groups/"
	end
end

########################################################
#
#  Groups Edit Post
#
########################################################
post "/groups/edit/:x_id" do
	begin
		@group = AffiliateGroup.first(x_id: params[:x_id])
		@group.update(params[:post])
		unless @group.errors.count == 0
			flash[:error] = @group.errors.full_messages.flatten.join(', ')
			redirect "/groups/edit/#{params[:x_id]}"
		else
			flash[:success] = "Group Updated."
	    redirect "/groups/"
	  end
	rescue Exception => e
		flash[:error] = "Group not found with x_id=#{params[:x_id]}."
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/groups/"
	end
end

########################################################
#
#  Group Delete
#
########################################################
get "/groups/delete/:x_id" do
	begin
		@group = AffiliateGroup.first(x_id: params[:x_id])
		@group.update(deleted: true, deleted_at: Time.now)
		unless @group.errors.count == 0
			flash[:error] = @group.errors.full_messages.flatten.join(', ')
			redirect "/groups/"
		else
			flash[:success] = "Group Deleted"
	    redirect "/groups/"
	  end
	rescue Exception => e
		flash[:error] = "Group not found with x_id=#{params[:id]}."
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/groups/"
	end
end