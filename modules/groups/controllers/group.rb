########################################################
#
#  Groups
#
########################################################
get "/groups/?" do
	begin
		@groups = AffiliateGroup.all(affiliate_user_id: session[:affiliate_user_id], deleted: false)
		if session[:first_group_created] == true && @groups.count == 1
			@group_create_alert = true
			user_badge = AffiliateUser.first(id: session[:affiliate_user_id]).user_badge
			user_badge.update(first_group_created: true)
			unless user_badge.errors.count == 0
				flash[:error] = user_badge.errors.full_messages.flatten.join(', ')
			else
				session[:first_group_created] = nil
				Achievement.create(achievement: "First Group created.", date: DateTime.now, affiliate_user_id: session[:affiliate_user_id])
			end
		end

		if session[:group_count].present?
			arr = [3, 5]
			if arr.include?(session[:group_count].to_i)
				@group_count = session[:group_count].to_i
				Achievement.create(achievement: "#{@group_count} groups added into Affiliate.", date: DateTime.now, affiliate_user_id: session[:affiliate_user_id])
				session[:group_count] = nil
			end
		end
		@page_title = "Groups"
		erb :"../modules/groups/views/index"
	rescue Exception => e
		flash[:error] = "There are some errors. Please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/"
	end
end