########################################################
#
#  Company Search
#
########################################################
get "/groups/:group_id/companies/add/?" do
	begin
		@group = AffiliateGroup.first(x_id: params[:group_id])
		@companies = 	Affiliate.get(session[:affiliate_id]).affiliate_relationships(company_approved: true).companies.all(order: :name) rescue nil 
		@companies ||= ""
		@action = "/groups/#{params[:group_id]}/companies/add/"
		erb :"../modules/groups/views/modals/add_company_list", layout: :modal_layout
	rescue Exception => e
		flash[:error] = "There are some errors. Please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/groups/"
	end
end


########################################################
#
#  Add Company
#
########################################################
post "/groups/:group_id/companies/add/?" do
	begin
		@group = AffiliateGroup.first(x_id: params[:group_id])
		@company = Company.first(x_id: params[:post]['x_id'])
		@existing_group = @group.affiliate_group_companies.all(company_id: @company.id)

		unless @existing_group.present?
			@new_company = AffiliateGroupCompany.create(
					affiliate_group_id: @group.id,
					affiliate_id: session[:affiliate_id],
					affiliate_user_id: session[:affiliate_user_id],
					company_id: @company.id
				)
			flash[:success] = "Company added to group."
		else
			flash[:success] = "Company already added."
		end
	rescue Exception => e
		flash[:error] = "There are some errors. Please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/groups/"
	end
end


########################################################
#
#  Remove Company
#
########################################################
get "/groups/:group_id/companies/remove/:x_id" do
	begin
		@group = AffiliateGroup.first(x_id: params[:group_id])
		@company = Company.first(x_id: params[:x_id])
		@affiliate_group_company = AffiliateGroupCompany.first(
				affiliate_group_id: @group.id, 
				company_id: @company.id,
				affiliate_id: session[:affiliate_id],
				affiliate_user_id: session[:affiliate_user_id]
			)
		@affiliate_group_company.destroy
		flash[:error] = "Company Removed"
		redirect "/groups/dashboard/#{params[:group_id]}"
	rescue Exception => e
		flash[:error] = "There are some errors. Please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/groups/"
	end
end

########################################################
#
#  Toggle Hide Company
#
########################################################
get "/groups/:group_id/companies/toggle/:x_id/hide/?" do
	begin
		@group = AffiliateGroup.first(x_id: params[:group_id])
		@company = Company.first(x_id: params[:x_id])
		@affiliate_group_company = AffiliateGroupCompany.first(
				affiliate_group_id: @group.id, 
				company_id: @company.id,
				affiliate_id: session[:affiliate_id],
				affiliate_user_id: session[:affiliate_user_id]
			)
		@affiliate_group_company.update(visible: false)
		flash[:error] = "Company Hidden"
		redirect "/groups/dashboard/#{params[:group_id]}"
	rescue Exception => e
		flash[:error] = "There are some errors. Please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/groups/"
	end
end

########################################################
#
#  Toggle Show Company
#
########################################################
get "/groups/:group_id/companies/toggle/:x_id/show/?" do
	begin
		@group = AffiliateGroup.first(x_id: params[:group_id])
		@company = Company.first(x_id: params[:x_id])
		@affiliate_group_company = AffiliateGroupCompany.first(
				affiliate_group_id: @group.id, 
				company_id: @company.id,
				affiliate_id: session[:affiliate_id],
				affiliate_user_id: session[:affiliate_user_id]
			)
		@affiliate_group_company.update(visible: true)
		flash[:success] = "Company Visible"
		redirect "/groups/dashboard/#{params[:group_id]}"
	rescue Exception => e
		flash[:error] = "There are some errors. Please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/groups/"
	end
end