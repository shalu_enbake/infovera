
########################################################
#
#  Groups Detail
#
########################################################
get "/groups/dashboard/:id" do 
	begin
		@group = AffiliateGroup.first(x_id: params[:id],affiliate_user_id: session[:affiliate_user_id])
		@affiliate_group_companies = @group.affiliate_group_companies
		@page_title = "#{@group.name rescue nil}"
		if session[:first_company_added_to_group] == true && @affiliate_group_companies.count == 1
			@first_company_added_to_group_alert = true
			user_badge = AffiliateUser.first(id: session[:affiliate_user_id]).user_badge
			user_badge.update(first_company_added_to_group: true)
			unless user_badge.errors.count == 0
				flash[:error] = user_badge.errors.full_messages.flatten.join(', ')
			else
				session[:first_company_added_to_group] = nil
				Achievement.create(achievement: "First Company added to Group successfully.", date: DateTime.now, affiliate_user_id: session[:affiliate_user_id])
			end
		end
		erb :"../modules/groups/views/dashboard"
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"	
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/groups/"
	end
end

########################################################
#
#  Groups Finance
#
########################################################
get "/groups/finance/:id" do
	begin
		@group = AffiliateGroup.first(x_id: params[:id],affiliate_user_id: session[:affiliate_user_id])
		# @affiliates = AffiliateGroupAffiliate.all(partner_group_id: @group.id, show_finance: true)
		@page_title = "#{@group.name rescue nil} Finance"
		erb :"../modules/groups/views/financial"
	rescue Exception => e
		flash[:error] = "There are some errors. Please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/groups/"
	end
end

########################################################
#
#  Groups Base
#
########################################################
get "/groups/base/:id" do 
	begin
		@group = AffiliateGroup.first(x_id: params[:id],affiliate_user_id: session[:affiliate_user_id])
		# @affiliates = AffiliateGroupAffiliate.all(partner_group_id: @group.id, show_base: true)
		@page_title = "#{@group.name rescue nil} Base"
		erb :"../modules/groups/views/base"
	rescue Exception => e
		flash[:error] = "There are some errors. Please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/groups/"
	end
end

########################################################
#
#  Groups Systems
#
########################################################
get "/groups/systems/:id" do
	begin
		@group = AffiliateGroup.first(x_id: params[:id],affiliate_user_id: session[:affiliate_user_id])
		# @affiliates = AffiliateGroupAffiliate.all(partner_group_id: @group.id, show_systems: true)
		@page_title = "#{@group.name rescue nil} Finance"
		erb :"../modules/groups/views/systems"
	rescue Exception => e
		flash[:error] = "There are some errors. Please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/groups/"
	end
end

########################################################
#
#  Groups Management
#
########################################################
get "/groups/management/:id" do
	begin
		@group = AffiliateGroup.first(x_id: params[:id],affiliate_user_id: session[:affiliate_user_id])
		# @affiliates = AffiliateGroupAffiliate.all(partner_group_id: @group.id, show_management: true)
		@page_title = "#{@group.name rescue nil} Management"
		erb :"../modules/groups/views/management"
	rescue Exception => e
		flash[:error] = "There are some errors. Please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/groups/"
	end
end

########################################################
#
#  Groups Growth
#
########################################################
get "/groups/growth/:id" do
	begin
		@group = AffiliateGroup.first(x_id: params[:id],affiliate_user_id: session[:affiliate_user_id])
		# @affiliates = AffiliateGroupAffiliate.all(partner_group_id: @group.id, show_growth: true)
		@page_title = "#{@group.name rescue nil} Growth"
		erb :"../modules/groups/views/growth"
	rescue Exception => e
		flash[:error] = "There are some errors. Please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/groups/"
	end
end
