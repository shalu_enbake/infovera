########################################################
#
#  Company User Delete
#
########################################################
get "/companies/:x_id/company_users/delete/:id/?" do
	@company_user = CompanyUser.first(id: params[:id])
	begin
		@company_user.update(deleted: true, deleted_at: Time.now)
		flash[:error] = "Company Deleted"
		redirect "/companies/#{params[:x_id]}/contacts/"
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/"
	end
end

########################################################
#
#  Company Detail
#
########################################################
get "/companies/:x_id/company_users/:id/?" do
	@company = Company.first(x_id: params[:x_id])
	@page_title = "#{@company.name rescue nil}"

	begin
		@company_user = @company.company_users.first(id: params[:id])
		@page_title = "#{@company.name rescue nil} Contacts"
		erb :"../modules/company_users/views/user_detail" #, layout: :modal_layout
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/companies/"
	end
end