########################################################
#
#  Contact
#
########################################################
get "/contact/?" do
	begin
		@message = session[:message]
		session[:message] = nil
		@action = "/contact/"
		erb :"../modules/contact/views/index"
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/"
	end
end

########################################################
#
#  Contact Post
#
########################################################
post "/contact/?" do 
	begin
		user = AffiliateUser.first(email: session[:email])
		send_email = Mailjet::Send.create(
			from_email: "tim.mushen@infovera.com",
			from_name: "support@infovera.com",
			subject: "Contact Message",
			"Mj-TemplateID": "217971",
			"Mj-TemplateLanguage": "true",
			recipients: [{ 'Email'=> 'support@infovera.com'}],
			vars: {
				"name" => user.first_name + " " + user.last_name,
				"email" => user.email,
				"message" => params[:post]['message']
			}
		)
		session[:message] = nil
		flash[:success] = "Your message has been sent. We will contact you soon."
		redirect '/contact/'
	rescue
		session[:message] = params[:post][:message]
		flash[:error] = "There are some errors, please try again!"
		Bugsnag.notify(RuntimeError.new(e))
		redirect '/contact/'
	end
end