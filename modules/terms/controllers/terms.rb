########################################################
#
#  Terms
#
########################################################
get "/login/terms/?" do
	begin
		@profile = Affiliate.get(session['affiliate_id'])
		@terms = Term.last(active: true)
		@action = "/login/terms/"
		@page_title = "Terms of Use"
		erb :"../modules/terms/views/index"
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/"
	end
end		

########################################################
#
#  Terms Accept
#
########################################################
post "/login/terms/?" do
	begin
		@terms = Term.last(active: true)
		@terms_accepted = TermsAccepted.create(
				term_id: @terms.id, 
				affiliate_id: session[:affiliate_id],
				affiliate_user_id: session[:affiliate_user_id],
				accepted_on: Time.now
			)
		session[:authorized] = true
		redirect "/"
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/"
	end
end