########################################################
#
# event CRUD
#
########################################################


###########################  event List ############################		
get "/up_coming_events/?" do
	begin
		@up_coming_events = Event.all(deleted: false)
		@page_title = "Upcoming Events"
		erb :"../modules/events/views/index"
	rescue Exception => e
		Bugsnag.notify(RuntimeError.new(e))
		flash[:error] = "There are some errors, please try again! #{e}"
		redirect "/"
	end
end

###########################  event View Modal ############################		
get "/up_coming_events/:id" do
	begin
		@event = Event.first(x_id: params[:id])
		@page_title = "Event Details"
		erb :"../modules/events/views/detail"
	rescue Exception => e
		Bugsnag.notify(RuntimeError.new(e))
		flash[:error] = "There are some errors, please try again! #{e}"
		redirect "/up_coming_events/"
	end
end

