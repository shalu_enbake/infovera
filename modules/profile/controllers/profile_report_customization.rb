########################################################
#
#  Profile Report Custumization
#
########################################################
get "/profile/report-customization/?" do
	begin
		erb :"../modules/profile/views/report_customization"
	rescue Exception => e
		flash[:error] = "There are some error, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/profile/"
	end
end