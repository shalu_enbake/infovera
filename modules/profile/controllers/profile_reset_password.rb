########################################################
#
#  Reset Password
#
########################################################
get "/profile/reset-password/?" do
	begin
		@action = "/profile/reset-password/"
		erb :"../modules/profile/views/reset_password"
	rescue Exception => e
		flash[:error] = "There are some error, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/profile/"
	end
end

########################################################
#
# Reset Password Process
#
########################################################
post '/profile/reset-password/?' do
	begin
		user = AffiliateUser.first(email: session[:email])
		user.update(password: params[:post]['password'], password_reset: false)
		flash[:success] = "Your password has been updated."
		redirect '/profile/'
	rescue Exception => e
		flash[:error] = "There are some error, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/profile/"
	end
end