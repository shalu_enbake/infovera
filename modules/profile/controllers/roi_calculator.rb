########################################################
#
#  Plan ROI Calculator
#
########################################################
get "/profile/plan/calculator/?" do 
  begin
    @user = AffiliateUser.first(email: session[:email])
    if @user.subscribed_plan_id.present?
      if @user.subscribed_plan_id == "affiliate"
        @subscribed_plan = "Affiliate"
        @l2_reports = "2/month"
        @l4_reports = "1/month"
        @l5_reports = "1/month"
        @cost = Stripe::Plan.retrieve(@user.subscribed_plan_id).amount
      elsif @user.subscribed_plan_id == "silver_pro"
        @subscribed_plan = "Silver Pro"
        @l2_reports = "5/month"
        @l4_reports = "1/month"
        @l5_reports = "1/month"
        @cost = Stripe::Plan.retrieve(@user.subscribed_plan_id).amount
      elsif @user.subscribed_plan_id == "gold_pro"
        @subscribed_plan = "Gold Pro"
        @l2_reports = "15/month"
        @l4_reports = "1/month"
        @l5_reports = "1/month"
        @cost = Stripe::Plan.retrieve(@user.subscribed_plan_id).amount
      else
        @subscribed_plan = "Platinum"
        @l2_reports = "50/month"
        @l4_reports = "1/month"
        @l5_reports = "1/month"
        @cost = Stripe::Plan.retrieve(@user.subscribed_plan_id).amount
      end
    else
      @subscribed_plan = "No Plan available."
      @l2_reports = "0/month"
      @l4_reports = "0/month"
      @l5_reports = "0/month"
      @cost = 0
    end
    @page_title = "ROI calculator"
    @action = "/profile/plan/calculator/?"
    erb :"../modules/profile/views/roi_calculator/form"
  rescue Exception => e
    flash[:error] = "There are some errors, please try again! #{e}"
    Bugsnag.notify(RuntimeError.new(e))
    redirect "/profile/"    
  end
end

post "/profile/plan/calculator/?" do
  begin
    @user = AffiliateUser.first(email: session[:email])
    if @user.subscribed_plan_id.present?
      if @user.subscribed_plan_id == "affiliate"
        @subscribed_plan = "Affiliate"
        @l2_reports = "2/month"
        @l4_reports = "1/month"
        @l5_reports = "1/month"
        @cost = Stripe::Plan.retrieve(@user.subscribed_plan_id).amount
      elsif @user.subscribed_plan_id == "silver_pro"
        @subscribed_plan = "Silver Pro"
        @l2_reports = "5/month"
        @l4_reports = "1/month"
        @l5_reports = "1/month"
        @cost = Stripe::Plan.retrieve(@user.subscribed_plan_id).amount
      elsif @user.subscribed_plan_id == "gold_pro"
        @subscribed_plan = "Gold Pro"
        @l2_reports = "15/month"
        @l4_reports = "1/month"
        @l5_reports = "1/month"
        @cost = Stripe::Plan.retrieve(@user.subscribed_plan_id).amount
      else
        @subscribed_plan = "Platinum"
        @l2_reports = "50/month"
        @l4_reports = "1/month"
        @l5_reports = "1/month"
        @cost = Stripe::Plan.retrieve(@user.subscribed_plan_id).amount
      end
    else
      @subscribed_plan = "No Plan available."
      @l2_reports = "0/month"
      @l4_reports = "0/month"
      @l5_reports = "0/month"
      @cost = 0
    end
    @l2_cost = params[:post][:l2].to_f * 12 * 129
    @l4_cost = params[:post][:l4].to_f * 12 * 1499
    @l5_cost = params[:post][:l5].to_f * 12 * 4899
    @total = @l2_cost + @l4_cost + @l5_cost
    @plan_affiliate_client_cost = 10993 
    @plan_affiliate_silver_client_cost = 18635
    @plan_affiliate_gold_client_cost = 46911 
    if @total <= @plan_affiliate_client_cost
      @plan = "Affiliate"
      @l2_reports_for_suggested_plan = "2/month"
      @l4_reports_for_suggested_plan = "1/month"
      @l5_reports_for_suggested_plan = "1/month"
      @cost_for_suggested_plan = Stripe::Plan.retrieve("affiliate").amount
    elsif @total <= @plan_affiliate_silver_client_cost
      @plan = "Silver Pro"
      @l2_reports_for_suggested_plan = "5/month"
      @l4_reports_for_suggested_plan = "1/month"
      @l5_reports_for_suggested_plan = "1/month"
      @cost_for_suggested_plan = Stripe::Plan.retrieve("silver_pro").amount
    elsif @total <= @plan_affiliate_gold_client_cost
      @plan = "Gold Pro"
      @l2_reports_for_suggested_plan = "15/month"
      @l4_reports_for_suggested_plan = "1/month"
      @l5_reports_for_suggested_plan = "1/month"
      @cost_for_suggested_plan = Stripe::Plan.retrieve("gold_pro").amount
    elsif @total > @plan_affiliate_gold_client_cost
      @plan = "Platinum"
      @l2_reports_for_suggested_plan = "50/month"
      @l4_reports_for_suggested_plan = "1/month"
      @l5_reports_for_suggested_plan = "1/month"
      @cost_for_suggested_plan = Stripe::Plan.retrieve("platinum").amount
    end
    if @cost <= @cost_for_suggested_plan
      @up_down = "Upgrade to"
    else
      @up_down = "Downgrade to"
    end
    @roi_calculator = true
    Achievement.create(achievement: "Roi Calculator used scuccessfully.", date: DateTime.now, affiliate_user_id: session[:affiliate_user_id])
    erb :"../modules/profile/views/roi_calculator/form"
  rescue Exception => e
    flash[:error] = "There are some errors, please try again! #{e}"
    Bugsnag.notify(RuntimeError.new(e))
    redirect "/profile/"  
  end
end