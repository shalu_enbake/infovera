########################################################
#
#  Categories
#
########################################################
get "/profile/categories/?" do
	begin
		@categories = AffiliateCategory.all
		erb :"../modules/profile/views/categories"
	rescue Exception => e
		flash[:error] = "There are some errors, Please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/profile/"
	end
end
