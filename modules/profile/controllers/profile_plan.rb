########################################################
#
#  Plan
#
########################################################
get "/profile/plan/?" do 
	begin
		@plans = Stripe::Plan.all
		@profile = Affiliate.get(session[:affiliate_id])
		@user = AffiliateUser.first(email: session[:email])
		@page_title = "Profile Plan"
		erb :"../modules/profile/views/plan"
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/profile/"    
	end
end

########################################################
#
#  Plan Signup
#
########################################################
get "/profile/plan/signup/:plan" do
	begin
		user = AffiliateUser.first(email: session[:email])
		send_email = Mailjet::Send.create(
			from_email: "tim.mushen@infovera.com",
			from_name: "tim.mushen@infovera.com",
			subject: "Affiliate Plan Upgrade",
			"Mj-TemplateID": "217972",
			"Mj-TemplateLanguage": "true",
			recipients: [{ 'Email'=> 'karla.boles@infovera.com'}],
			vars: {
				"name" => user.first_name + " " + user.last_name,
				"email" => user.email,
				"company" => "#{user.affiliate.name rescue nil}",
				"plan" => params[:plan]
			}
		)
		erb :"../modules/profile/views/plan_signup"
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/profile/plan/"
	end
end

get "/profile/plan/subscription/:plan" do
	begin
		@selected_plan = params[:plan]
		erb :"../modules/profile/views/subscription"
	rescue Exception => e
		flash[:error] = "There are some error, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/profile/plan/?"
	end
end

post "/profile/plan/subscription_checkout/" do  
	begin
		# Stripe Customer & Subscription Creation 
		Stripe.api_key = settings.secret_key
		user = AffiliateUser.first(email: session[:email])
		customer = Stripe::Customer.create(:email => user.email, :source  => params[:stripeToken],:plan => params[:plan],:description => 'Testing customer')
		if customer.present?
			user.subscribed_plan_id = params[:plan]
			user.stripe_customer_id= customer.id
			@plan = Stripe::Plan.retrieve(params[:plan])
			user.subscribed_plan = @plan.name
			user.save
		end
		erb :"../modules/profile/views/subscription_checkout"
	rescue Exception => e
		flash[:error] = "There are some error, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/profile/plan/subscription/#{params[:plan]}"
	end
end

