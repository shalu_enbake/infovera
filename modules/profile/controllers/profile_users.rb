########################################################
#
#  Profile Users
#
########################################################
get "/profile/users/?" do
	begin
		@profile = Affiliate.get(session[:affiliate_id])
		@affiliate_users = AffiliateUser.all(affiliate_id: session[:affiliate_id], deleted: false)
		@page_title = "Profile Users"
		erb :"../modules/profile/views/users"
	rescue Exception => e
		flash[:error] = "There are some errors, Please try again #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect '/profile/users/?'
	end
	
end

########################################################
#
#  Profile Users
#
########################################################
get "/profile/users/add/?" do 
	begin
		@affiliate_user = AffiliateUser.new
		@action = "/profile/users/add/"
		erb :"../modules/profile/views/modals/user_form", layout: :modal_layout
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/profile/users/"
	end
end

########################################################
#
#  Profile Users Post
#
########################################################
post "/profile/users/add/?" do 
	begin	
		params[:post]['affiliate_id'] = session[:affiliate_id]
		params[:post]['password'] = Array.new(10).map { (65 + rand(58)).chr }.join
		@affiliate_user = AffiliateUser.create(params[:post])
		unless @affiliate_user.errors.count == 0
			flash[:error] = @affiliate_user.errors.full_messages.flatten.join(', ')
		else
			flash[:success] = "User Created"
	  end
	rescue Exception => e
		flash[:error] = "There are some errors, Please try again."
		Bugsnag.notify(RuntimeError.new(e))
	end
end

########################################################
#
#  Delete User
#
########################################################
get "/profile/users/delete/:x_id" do
	begin
		@affiliate_user = AffiliateUser.first(x_id: params[:x_id])
		@affiliate_user.update(deleted: true, deleted_at: Time.now)
		unless @affiliate_user.errors.count == 0
			flash[:error] = @affiliate_user.errors.full_messages.flatten.join(', ')
			redirect '/profile/users/?'
		else
			flash[:success] = "User deleted"
			redirect '/profile/users/?'
		end
	rescue Exception => e
		flash[:error] = "There are some errors, Please try again #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect '/profile/users/?'
	end
end


########################################################
#
#  Edit User
#
########################################################
get "/profile/users/edit/:x_id" do
	begin
		@affiliate_user = AffiliateUser.first(x_id: params[:x_id])
		@action = "/profile/users/edit/#{params[:x_id]}"
		erb :"../modules/profile/views/modals/user_form" , layout: :modal_layout	
	rescue Exception => e
		flash[:error] = "User not found with x_id = #{params[:x_id]}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/profile/users/"		
	end
end

########################################################
#
#  Edit User Post
#
########################################################
post "/profile/users/edit/:x_id" do 
	begin
		@affiliate_user = AffiliateUser.first(x_id: params[:x_id])
		@affiliate_user.update(params[:post])
		unless @affiliate_user.errors.count == 0
			flash[:error] = @affiliate_user.errors.full_messages.flatten.join(', ')
		else
			flash[:success] = "User Updated"
	  end
	rescue Exception => e
		flash[:error] = "User not found with x_id = #{params[:x_id]}"
		Bugsnag.notify(RuntimeError.new(e))
	end
end

get "/profile/users/detail/:x_id/" do
	begin
		@affiliate_user = AffiliateUser.first(x_id: params[:x_id])
		erb :"../modules/profile/views/detail" 
	rescue Exception => e
		flash[:error] = "User not found with x_id = #{params[:x_id]}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/profile/users/"		
	end
end
