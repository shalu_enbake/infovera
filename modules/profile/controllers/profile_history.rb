########################################################
#
#  History
#
########################################################
get "/profile/history/?" do
	begin
		erb :"../modules/profile/views/history"
	rescue Exception => e
		flash[:error] = "There are some errors, Please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/profile/"
	end
end