########################################################
#
#  Profile
#
########################################################
get "/profile/?" do
	begin
		@profile = Affiliate.get(session[:affiliate_id])
		@page_title = "Profile"
		@action = "/profile/update/"
		erb :"../modules/profile/views/index"
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/"
	end
end

########################################################
#
#  Profile Update
#
########################################################
post "/profile/update/?" do
	begin
		@affiliate = Affiliate.get(session[:affiliate_id])
		# @geo = Geokit::Geocoders::GoogleGeocoder.geocode "#{params[:post]['address']}#{params[:post]['address_2']}, #{params[:post]['city']}, #{params[:post]['state']} #{params[:post]['zip']}"
		# params[:post]['latitude'] = @geo.latitude 
		# params[:post]['longitude'] = @geo.longitude
		@affiliate.update(params[:post])
		flash[:success] = "Profile Updated"
		redirect "/profile/"
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/profile/"
	end
end

########################################################
#
#  Profile Logo
#
########################################################
get "/profile/logo/?" do
	begin
		@profile = Affiliate.get(session[:affiliate_id])
		@page_title = "Profile Logo"

		if session[:first_logo_upload] == true && @profile.logo_secure_url.present?
			@first_logo_upload_alert = true
			user_badge = AffiliateUser.first(id: session[:affiliate_user_id]).user_badge
			user_badge.update(first_logo_upload: true)
			unless user_badge.errors.count == 0
				flash[:error] = user_badge.errors.full_messages.flatten.join(', ')
			else
				session[:first_logo_upload] = nil
				Achievement.create(achievement: "First logo upload successfully.", date: DateTime.now, affiliate_user_id: session[:affiliate_user_id])
			end
		elsif session[:logo_updated] == true
			@logo_updated = true
			session[:logo_updated] = nil		
			Achievement.create(achievement: "Logo upload successfully.", date: DateTime.now, affiliate_user_id: session[:affiliate_user_id])
		end
		erb :"../modules/profile/views/logo"
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/profile/"
	end
end

########################################################
#
#  Profile Logo
#
########################################################
post "/profile/logo/?" do 
	begin
		@profile = Affiliate.get(session[:affiliate_id])
		public_id = "affiliate_logos/affiliate_#{@profile.x_id}"

		#Upload thumb to cloudinary
		unless params['filename'].nil?
			thumbnail = Cloudinary::Uploader.upload(
				params['filename'][:tempfile],
				use_filename: true,
				public_id: public_id,
				unique_filename: true
			)

			@profile.update(
				logo_image_name: public_id,
				logo_secure_url: thumbnail['secure_url'],
				logo_bytes: thumbnail['bytes'],
				logo_format: thumbnail['format'],
				logo_width: thumbnail['width'],
				logo_height: thumbnail['height']
			)

			flash[:success] = "Logo Uploaded"
			session[:logo_secure_url] = thumbnail['secure_url']
			session[:logo_updated] = true
			redirect "/profile/logo/"
		else
			flash[:error] = "Please select a logo."
			redirect "/profile/logo/"
		end
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/profile/"
	end
end

########################################################
#
#  Delete Logo
#
########################################################
get "/profile/logo/delete/?" do
	begin
		@profile = Affiliate.get(session[:affiliate_id])
		@profile.update(
			logo_image_name: nil,
			logo_secure_url: nil,
			logo_bytes: nil,
			logo_format: nil,
			logo_width: nil,
			logo_height: nil
		)
		session[:logo_secure_url] = nil
		flash[:error] = "Logo Deleted"
		redirect "/profile/logo/"
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/profile/"
	end
end





########################################################
#
#  Profile Description
#
########################################################
get "/profile/description/?" do
	begin
		@profile = Affiliate.get(session[:affiliate_id])
		@action = "/profile/description/"
		@page_title = "Profile Description"
		if session[:description_updated] == true
			@description_updated = true	
			session[:description_updated] = nil	
			Achievement.create(achievement: "Description upload successfully.", date: DateTime.now, affiliate_user_id: session[:affiliate_user_id])
		end
		erb :"../modules/profile/views/description"
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/profile/"
	end
end

########################################################
#
#  Profile Description Profile
#
########################################################
post "/profile/description/?" do
	begin
		@profile = Affiliate.get(session[:affiliate_id])
		@profile.update(params[:post])
		unless @profile.errors.count == 0
			flash[:error] = @profile.errors.full_messages.flatten.join(', ')
		else
			session[:description_updated] = true
			flash[:success] = "Description Updated"
		end
		redirect "/profile/description"
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/profile/"
	end
end