get "/profile/billing/?" do 
  begin
    user = AffiliateUser.first(email: session[:email])
    customer = x_stripe_customer(customer_id: user.stripe_customer_id) rescue nil
    @charges = customer.charges if customer.present?
    @page_title = "Profile Billing"
    erb :"../modules/profile/views/profile_billing/billing"
  rescue Exception => e  
    flash[:error] = "There are some errors, please try again! #{e}"
    Bugsnag.notify(RuntimeError.new(e))
    redirect "/profile/?"	
  end  
end