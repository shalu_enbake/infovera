########################################################
#
#  Profile forecast
#
########################################################
get "/profile/forecast/?" do 
	begin
		@forecasts = Affiliate.get(session[:affiliate_id]).forecasts.all(deleted: false)
		if session[:forecast_added] == true
			@forecast_added = true
			session[:forecast_added] = nil		
			Achievement.create(achievement: "Forecast added successfully.", date: DateTime.now, affiliate_user_id: session[:affiliate_user_id])
		end
		erb :"../modules/profile/views/forecast/index"
	rescue Exception => e
		flash[:error] = "There are some errors, Please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/profile/"
	end
end

########################################################
#
#  Profile forecast Add
#
########################################################
get "/profile/forecast/add/?" do 
	begin
		@forecast = Affiliate.get(session[:affiliate_id]).forecasts.new
		@companies = Affiliate.get(session[:affiliate_id]).affiliate_relationships.company
		@action = "/profile/forecast/add/"
		erb :"../modules/profile/views/forecast/forecast_form", layout: :modal_layout
	rescue Exception => e
		flash[:error] = "There are some errors, Please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/profile/"
	end
end
########################################################
#
#  Profile forecast Add post
#
########################################################
post "/profile/forecast/add/?" do 
	begin	
		@forecast = Affiliate.get(session[:affiliate_id]).forecasts.create(params[:post])
		unless @forecast.errors.count == 0
			flash[:error] = @forecast.errors.full_messages.flatten.join(', ')
		else
			session[:forecast_added] = true
			flash[:success] = "forecast Added."
		end
	rescue Exception => e
		flash[:error] = "There are some errors, Please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
	end
end

########################################################
#
#  Profile forecast Edit
#
########################################################
get "/profile/forecast/:x_id/edit/?" do 
	begin
		@forecast = Forecast.first(x_id: params[:x_id])
		@companies = Affiliate.get(session[:affiliate_id]).affiliate_relationships.company
		@action = "/profile/forecast/#{params[:x_id]}/edit/"
		erb :"../modules/profile/views/forecast/forecast_form", layout: :modal_layout
	rescue Exception => e
		flash[:error] = "There are some errors, Please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/profile/"
	end
end

########################################################
#
#  Profile forecast Edit post
#
########################################################
post "/profile/forecast/:x_id/edit/?" do
	begin	
		@forecast = Forecast.first(x_id: params[:x_id])
		@forecast.update(params[:post])
		unless @forecast.errors.count == 0
			flash[:error] = @forecast.errors.full_messages.flatten.join(', ')
		else
			flash[:success] = "forecast Updated."
		end
	rescue Exception => e
		flash[:error] = "There are some errors, Please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
	end
end

########################################################
#
#  Profile forecast delete
#
########################################################
get "/profile/forecast/:x_id/delete/?" do
	begin	
		@forecast = Forecast.first(x_id: params[:x_id])
		@forecast.update(deleted: true, deleted_at: Time.now)
		unless @forecast.errors.count == 0
			flash[:error] = @forecast.errors.full_messages.flatten.join(', ')
			redirect "/profile/forecast/?"
		else
			flash[:success] = "forecast Deleted."
			redirect "/profile/forecast/?"
		end
	rescue Exception => e
		flash[:error] = "There are some errors, Please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/profile/forecast/?"
	end
end