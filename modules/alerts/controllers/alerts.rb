########################################################
#
#  Alerts
#
########################################################
get "/alerts/?" do
	begin
		@affiliate = Affiliate.get(session[:affiliate_id])
		# @alerts = @affiliate.alerts.all(deleted: false, limit: 10, order: [ :created_at.desc ])
		@alerts = Alert.all(deleted: false, limit: 10,affiliate_id: @affiliate.id, order: [ :created_at.desc ])
		@page_title = "Alerts"
		erb :"../modules/alerts/views/index"
	rescue Exception => e
		flash[:error] = "There are some errors, please try again! #{e}"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/"		
	end
end


get "/alerts/:id/?" do 
	begin
		@alerts = Alert.first(:x_id=> params[:id])
		erb :"../modules/alerts/views/show"
	rescue Exception => e
		flash[:error] = "There are some errors. Please try again!"
		Bugsnag.notify(RuntimeError.new(e))
		redirect "/"
	end
end