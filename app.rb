########################################################
#
# Require Gems
#
########################################################

require "sinatra"
require "data_mapper"
require "rack/ssl-enforcer"
require "platformx"
require "sinatra/config_file"
require "digest/md5"
require "mailjet"
require "pdfkit"
require "./ext/string.rb"
require "cloudinary"
require "geokit"
require 'algoliasearch'
require "dm-core"
require "dm-validations"
require "stripe"
require "httparty"
require 'wistia'
require 'base64'

########################################################
#
# Config
#
########################################################
set :environments, %w{development test production staging demo}
config_file "config/config.yml"


########################################################
#
# Dev Configuration
#
########################################################
configure :development do
    require_relative "config/development"
end

########################################################
#
# Demo Configuration
#
########################################################
configure :demo do
  require_relative "config/demo"
end

########################################################
#
# Staging Configuration
#
########################################################
configure :staging do
  require_relative "config/staging"
end

########################################################
#
# Production Configuration
#
########################################################
configure :production do
  require_relative "config/production"
end


########################################################
#
# Global Config
#
########################################################
configure do

# Enable Sessions
enable :sessions
set :sessions, :expire_after => 2592000
set :session_secret, "<f472ce47dlc9dkwnalci>lds-vflg,mwfwewe9izbnm23r13r[o1k2ce47dlc9dkwnalci>52f59f299117e9c4680f85233f4faace739de1ca8f3b4f896eb872d32bf9023d35988b10b4075141850241751e64ae822c356442b3f"
set :partial_template_engine, :erb

Platformx.configure do |config|
	config.aws_access_key_id = settings.aws_access_key_id
	config.aws_secret_access_key = settings.aws_secret_access_key
	config.aws_bucket = settings.aws_bucket
	config.aws_region = settings.aws_region
end

Mailjet.configure do |config|
  config.api_key = settings.mailjet_api
  config.secret_key = settings.mailjet_secret
  config.api_version = "v3.1"
end

Wistia.password = settings.wistia_api_password
Faker::Config.locale = 'en-US'
Geokit::Geocoders::GoogleGeocoder.api_key = 'AIzaSyAoSlNSpNQts9Qz4_2EcfG4BF17htNsrek'

########################################################
#
# Bootstrap Modules, Models, Controlers & Helpers
#
########################################################
require_relative "config/bootstrap.rb"

#set :static_cache_control, [:public, :max_age => 0] # 1 month

########################################################
#
# Set Globals
#
########################################################

Algolia.init application_id: settings.algolia_application_id,
             api_key:        settings.algolia_api_key

# End Global Config

# set stripe
set :publishable_key, settings.publishable_key
set :secret_key, settings.secret_key
set :company_index, settings.company_index
set :affiliate_index, settings.affiliate_index
set :api_url, settings.api_url 
Stripe.api_key = settings.secret_key
end