########################################################
#
# Include Application Folders
#
########################################################
app_folders = %w(models controllers modules)

app_folders.each do |folder|	
	Dir[File.join(File.dirname(__FILE__), '..',  "#{folder}", '**/*.rb')].sort.each do |file|
	  require file
	end
end

helpers do
	Dir[File.join(File.dirname(__FILE__), '..',  "helpers", '**/*.rb')].sort.each do |file|
	  require file
	end
end

########################################################
#
# Finalize Datamapper
#
########################################################
DataMapper.finalize     

########################################################
#
# Datamapper Autoupgrade - Creates tables on boot
#
########################################################
DataMapper.auto_upgrade!

########################################################
#
# CSRF Protection
#
########################################################
before do
  session[:csrf] ||= SecureRandom.hex(64)

  response.set_cookie 'authenticity_token', {
    value: session[:csrf],
    expires: Time.now + (60 * 60 * 24 * 180), # that's 180 days
    path: '/',
    httponly: true
    # secure: true # if you have HTTPS (and you should) then enable this
  }
  # this is a Rack method, that basically asks
  #   if we're doing anything other than GET
  if !request.safe?
    # check that the session is the same as the form
    #   parameter AND the cookie value
    # if session[:csrf] == params['_csrf'] && session[:csrf] == request.cookies['authenticity_token']
    #   # everything is good.
    # else
    #   halt "#{session.inspect}"
    #   halt 403, 'CSRF failed'
    # end
  end
end