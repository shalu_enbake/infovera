# Set Server
server "app3.getx.io", user: "ubuntu", roles: %w{app web} #, other_property: :other_value

# Set Git Branch
set :branch, "master"
set :ssh_options, { :forward_agent => true }


set :deploy_to, "/var/www/vhosts/affiliates.infovera.com"
