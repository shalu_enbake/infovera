########################################################
#
# Set Environment Vars
#
########################################################
set :environment, :demo
set :show_exceptions, false
set :dump_errors, true
set :raise_errors, false

########################################################
#
# Datamapper
#
########################################################
#DataMapper::Logger.new($stdout, :debug)
DataMapper::Property::String.length(255)
#DataMapper::Model.raise_on_save_failure = true
DataMapper.setup(:default, settings.database_string)



use Rack::Deflater
#use Rack::Cache
# use Rack::SslEnforcer
########################################################
#
# PDFTK
#
########################################################
set pdftk_path: "/usr/local/bin/pdftk"

########################################################
#
# Cloudinary
#
########################################################
Cloudinary::config do |config|
  config.cloud_name = settings.cloudinary_cloud_name
  config.api_key = settings.cloudinary_api_key
  config.api_secret = settings.cloudinary_api_secret
  config.cdn_subdomain = true
  config.secure = true
  config.enhance_image_tag = true
  config.static_image_support = false
end

########################################################
#
# Bugsnag
#
########################################################
Bugsnag.configure do |config|
  config.api_key = "50ae925d9d7697a67d829679e32eb46f"
  config.notify_release_stages = ['production', 'staging']
  config.send_environment = true
end
 use Bugsnag::Rack


########################################################
#
#  Analytics
#
########################################################
@analytics = ""