# set :environment => :development
set :show_exceptions => true
set :dump_errors => true
set :raise_errors => true
set :reload_templates => true


 use BetterErrors::Middleware
  BetterErrors.application_root = __dir__

########################################################
#
# Datamapper
#
########################################################
DataMapper::Logger.new($stdout, :debug)
DataMapper::Property::String.length(255)
#DataMapper::Model.raise_on_save_failure = true
DataMapper.setup(:default, settings.database_string)


########################################################
#
# Cloudinary
#
########################################################
Cloudinary::config do |config|
  config.cloud_name = settings.cloudinary_cloud_name
  config.api_key = settings.cloudinary_api_key
  config.api_secret = settings.cloudinary_api_secret
  config.cdn_subdomain = true
  config.secure = true
  config.enhance_image_tag = true
  config.static_image_support = false
end


########################################################
#
# PDFTK
#
########################################################
set pdftk_path: "/usr/local/bin/pdftk"
########################################################
#
#  Analytics
#
########################################################
@analytics = ""