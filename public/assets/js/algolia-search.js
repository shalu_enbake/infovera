/* global instantsearch */
/* eslint-disable object-shorthand, prefer-template, prefer-arrow-callback */

var search = instantsearch({ // eslint-disable-line
  appId: 'JJLO4NL37O',
  apiKey: '2cda77ff86e052f3c9e75b9abbd2688a',
  indexName: 'Company',
  urlSync: true
});

search.addWidget(
  instantsearch.widgets.searchBox({
    container: '#search-input'
  })
);

search.addWidget(
  instantsearch.widgets.hits({
    container: '#hits',
    hitsPerPage: 10,
    templates: {
      item: document.getElementById('hit-template').innerHTML,
      empty: "We didn't find any results for the search <em>\"{{query}}\"</em>"
    },
  })
);

search.addWidget(
  instantsearch.widgets.pagination({
    container: '#pagination'
  })
);

search.start();