$(function() {
    // Start Ready

    // Parsley Errors
    $('form').parsley({
        successClass: 'success',
        errorClass: 'error',
        errors: {
            classHandler: function(el) {
                return $(el).closest('.control-group');
            },
            errorsWrapper: '<span class=\"help-inline\"></span>',
            errorElem: '<label error="error"></label>'
        }
    });


    // Forms
    $('textarea').autogrow({ animate: false });
    $('.selectpicker').selectpicker();
    $('.summernote').summernote();
    $('.datetimepicker').datetimepicker({
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-chevron-up",
            down: "fa fa-chevron-down",
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-screenshot',
            clear: 'fa fa-trash',
            close: 'fa fa-remove'
        }
    });

    $('.datepicker').datetimepicker({
        format: 'YYYY-MM-DD',
        viewMode: 'days',
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-chevron-up",
            down: "fa fa-chevron-down",
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-screenshot',
            clear: 'fa fa-trash',
            close: 'fa fa-remove'
        }
    });

    $('.timepicker').datetimepicker({
        //          format: 'H:mm',    // use this format if you want the 24hours timepicker
        format: 'h:mm A', //use this format if you want the 12hours timpiecker with AM/PM toggle
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-chevron-up",
            down: "fa fa-chevron-down",
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-screenshot',
            clear: 'fa fa-trash',
            close: 'fa fa-remove'
        }
    });
    $('.dropify').dropify({
        messages: {
            'default': 'Drag and drop a file here or click',
            'replace': 'Drag and drop or click to replace',
            'remove': 'Remove',
            'error': 'Sorry, this file is too large'

        }
    });
    // $('.money').inputmask("numeric", {
    //     radixPoint: ".",
    //     groupSeparator: ",",
    //     digits: 2,
    //     autoGroup: true,
    //     allowMinus: false,
    //     autoUnmask: true,
    //     rightAlign: false,
    //     removeMaskOnSubmit: true,
    //     oncleared: function() { self.Value(''); }
    // });

    $('[data-toggle="tooltip"]').tooltip();
    // END READY

    $(function () {
        var getData = function (request, response) {
            $.ajax({
                url: "/companies/find_naics/",
                type: "POST",
                dataType: "json",
                data: { term: request.term},
                complete: function() {},
                success: function(data, textStatus, xhr){
                    if (data.status == 200) {
                        var result = [];
                        $.each(data.naics, function (i, a) {
                        a.label = a.title;
                        result.push(a);
                    });
                    response(result);
                    } else {
                        alert(data.message)
                    }
                },
                error: function() {
                    alert("There are some error, Please try again!")
                }
            });
        };

        var selectItem = function (event, ui) {
            $("#search_autocomplete").val(ui.item.value);
            $("#naic_id").val(ui.item.id)
            return false;
        }

        $("#search_autocomplete").autocomplete({
            source: getData,
            appendTo: '#books-search-results',
            select: selectItem,
            minLength: 2,
            change: function() {

            }
        });
    });

    $('.money').inputmask("numeric", {
        radixPoint: ".",
        groupSeparator: ",",
        digits: 2,
        autoGroup: true,
        allowMinus: false,
        autoUnmask: true,
        rightAlign: false,
        removeMaskOnSubmit: true,
        oncleared: function(){ 
            self.Value(''); 
        }
    });

    //  $('.money').click(function () {
    //     this.caretToStart();
    // });
 


});
