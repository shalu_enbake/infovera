$(function() {
	$('.datatable tbody').on('click', '.delete-row', function (e) {	
			e.preventDefault();
			$(window).keydown(function(event){
				if(event.keyCode == 13) {
					event.preventDefault();
				}
			});
			console.log('called');
			var deleteElement = $(this).closest("tr");
			var url = $(this).attr('href');
			var string = ''//'id='+ id ;
			var html = '<div class="modal fade" id="confirmContainer"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><a class="close" data-dismiss="modal">x</a>' +
            '<h3>Please Confirm!</h3></div><div class="modal-body">' +
            'Are you sure you want to delete this record?</div><div class="modal-footer">' +
            '<a href="#" class="btn btn-danger" id="confirmYesBtn">Confirm</a>' +
            '<a href="#" class="btn" data-dismiss="modal">Cancel</a></div></div></div></div>';
         	 console.log('set vars');
            $('#alert-area').html(html);
            $('#confirmContainer').modal('show');
			console.log('called modal');
            var context = $(this);
           	 	$('#confirmYesBtn').on("click",function(){
               //call success
			   console.log('success');
					$.ajax({
						type: "GET",
						url: url,
						data: string,
						cache: false,
						success: function(){
          					$('#confirmContainer').modal('hide');
							deleteElement.fadeOut(function(){$(this).delay(1000).remove();});
							console.log('Hide');
						}
					 });		   
            	});
			});
	/// Call URL

	$('.datatable tbody').on('click', '.delete-refresh', function (e) {	
			e.preventDefault();
			$(window).keydown(function(event){
				if(event.keyCode == 13) {
					event.preventDefault();
				}
			});
			console.log('called');
			var deleteElement = $(this).closest("tr");
			var url = $(this).attr('href');
			var string = ''//'id='+ id ;
			var html = '<div class="modal fade" id="confirmContainer"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><a class="close" data-dismiss="modal">x</a>' +
            '<h3>Please Confirm!</h3></div><div class="modal-body">' +
            'Are you sure you want to delete this record?</div><div class="modal-footer">' +
            '<a href="#" class="btn btn-danger" id="confirmYesBtn">Confirm</a>' +
            '<a href="#" class="btn" data-dismiss="modal">Cancel</a></div></div></div></div>';
         	 console.log('set vars');
            $('#alert-area').html(html);
            $('#confirmContainer').modal('show');
			console.log('called modal');
            var context = $(this);
           	 	$('#confirmYesBtn').on("click",function(){
               //call success
			   console.log('success');
					location.replace(url)		   
            	});
			});
	/// Call URL

	$('.ajaxCallRow2').on("click",function(e){
			e.preventDefault();
			console.log('called');
			var deleteElement = $(this).closest("tr");
			var url = $(this).attr('href');
			var string = ''//'id='+ id ;
		 	 console.log('set vars');
               //call success
			   console.log('success');
					$.ajax({
						type: "GET",
						url: url,
						data: string,
						cache: false,
						success: function(){
        					deleteElement.fadeOut(function(){$(this).delay(1000).remove();});
							console.log('Hide');
						}
					 });		   
        	});
	/// Call URL	
$('.ajaxCallRow').on("click",function(e){
			e.preventDefault();
			console.log('called');
			var deleteElement = $(this).closest("tr");
			var url = $(this).attr('href');
			
			($(this).attr('data-message')) ? vmessage = $(this).attr('data-message') : vmessage = "Task has been added to your springboard";
			
			var string = ''//'id='+ id ;
			console.log('set vars');
               //call success
			   console.log('success');
					$.ajax({
						type: "GET",
						url: url,
						data: string,
						cache: false,
						success: function(){
          					deleteElement.fadeOut(function(){$(this).delay(1000).remove();});
							$("#alert-area").append($("<div class='alert alert-success fade in' data-alert><a class='close' data-dismiss='alert'>x</a> " + vmessage + "</div>"));
   							$(".alert").delay(2000).fadeOut("slow", function () { $(this).remove(); });
							console.log('Hide');
						}
					 });		   
            });

$('.ajax-dropdown').on("click",function(e){
			e.preventDefault();
			console.log('called');
			
			var url = $(this).attr('href');
			var label = $(this).text();
			($(this).attr('data-message')) ? vmessage = $(this).attr('data-message') : vmessage = "Task has been added to your springboard";
			
			var string = ''//'id='+ id ;
			console.log('set vars');
               //call success
			   console.log('success');
					$.ajax({
						type: "GET",
						url: url,
						data: string,
						cache: false,
						success: function(){
							$(this).closest(".data-toggle").html(label + '<span class="caret"></span>')
						}, 
						error: function(){
							$(this).closet(".data-toggle").html('ERROR!!!')
						}
					 });		   
            });

$('.ajaxCallRowSupport').on("click",function(e){
			e.preventDefault();
			console.log('called');
			var deleteElement = $(this).closest("tr");
			var url = $(this).attr('href');
			
			($(this).attr('data-message')) ? vmessage = $(this).attr('data-message') : vmessage = "Support ticket has been closed. You can view closed tickets by clicking the tab - Closed Tickets.";
			
			var string = ''//'id='+ id ;
			console.log('set vars');
               //call success
			   console.log('success');
					$.ajax({
						type: "GET",
						url: url,
						data: string,
						cache: false,
						success: function(){
          					deleteElement.fadeOut(function(){$(this).delay(1000).remove();});
							$("#alert-area").append($("<div class='alert alert-success fade in' data-alert><a class='close' data-dismiss='alert'>x</a> " + vmessage + "</div>"));
   							$(".alert").delay(30000).fadeOut("slow", function () { $(this).remove(); });
							console.log('Hide');
						}
					 });		   
            });

$('.ajaxCancelRow').on("click",function(e){
			e.preventDefault();
			$(window).keydown(function(event){
				if(event.keyCode == 13) {
					event.preventDefault();
				}
			});
			console.log('called');
			var deleteElement = $(this).closest("tr");
			var url = $(this).attr('href');
			var string = ''//'id='+ id ;
			var html = '<div class="modal" id="confirmContainer"><div class="modal-header"><a class="close" data-dismiss="modal">x</a>' +
            '<h3>Please Confirm!</h3></div><div class="modal-body">' +
            'Are you sure you want to submit Cancellation Request to CTM office?</div><div class="modal-footer">' +
            '<a href="#" class="btn btn-danger" id="confirmYesBtn">Confirm</a>' +
            '<a href="#" class="btn" data-dismiss="modal">Cancel</a></div></div>';
         	 console.log('set vars');
            $('#alert-area').html(html);
            $('#confirmContainer').modal('show');
			console.log('called modal');
            var context = $(this);
           	 	$('#confirmYesBtn').on("click",function(){
               //call success
			   console.log('success');
					$.ajax({
						type: "GET",
						url: url,
						data: string,
						cache: false,
						success: function(){
          					$('#confirmContainer').modal('hide');
							// deleteElement.fadeOut(function(){$(this).delay(1000).remove();});
							console.log('Hide');
							location.reload();
						}
					 });		   
            	});
			});
  
  $('.show-row').on("click",function(e){
			e.preventDefault();
			$(window).keydown(function(event){
				if(event.keyCode == 13) {
					event.preventDefault();
				}
			});
			console.log('called');
			var url = $(this).attr('href');
			var string = ''//'id='+ id ;
			var html = '<div class="modal fade" id="confirmContainer"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><a class="close" data-dismiss="modal">x</a>' +
            '<h3>You are out of credits!</h3></div><div class="modal-body">' +
            'If you would like to continue, your account will be charged $129.</div><div class="modal-footer">' +
            '<a href="#" class="btn" data-dismiss="modal">Cancel</a> <a href="' + url + '" class="btn btn-success btn-fill">Continue</a></div></div></div></div>';
         	 console.log('set vars');
            $('#alert-area').html(html);
            $('#confirmContainer').modal('show');
            var context = $(this);
			});

/// End	
});

