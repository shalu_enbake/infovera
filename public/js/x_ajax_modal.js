$(document).ready(function () {

    // Support for AJAX loaded modal window.
    // Focuses on first input textbox after it loads the window.

    // This will highjack anything with data-toggle="modal" and remotely load the href into a modal
    $('body').on('click', '[data-toggle="modal"]', function (e) {

        // Prevent the link from going anywhere
        e.preventDefault();

        // set a variable with the href of the element
        var href = $(this).attr('href');

        // If there's no link then don't do anything
        if (href.indexOf('#') == 0) {
        } else {

            // Get the data and put it into a modal
            $.get(href, function (data) {
                $('<div class="modal fade">' + data + '</div>').modal({backdrop: 'static', keyboard: false});

            // if the get request is successful do some stuff to the form elements
        }).success(function () {

                // focus the cursor on the first input box
                $('input:text:visible:first').focus();

                // add a class to the text inputs
                $('input:text').addClass('col-md-4');

                // make text areas a little bigger
                $('textarea').addClass('col-md-5 textareaHeight');

                // add a width to select drop-downs
                $('select').addClass('col-md-4');

                // validate the form using jQuery Validation Plugin 1.9.0
                //$('.modal form').validate();

                // validate using parsley
                //$('.modal-body > form').parsley('validate');

                // start date picker so it's available in the modal
               // Datepicker




           });
        }
        return false;
    });

// Remove the modal
$('body').on('hidden', '.modal', function () {
    $(this).remove();
});


// Ajax Modal Form Submit
// prevent normal form submit
$('body').on('submit', '#disableForm', function (e) {
    e.preventDefault();
    $('.modal form').trigger('submit');
});

// Submit the form in the modal. Form gets submitted to the form action in the modal
$('body').on('click', '#modalSubmitBtn', function (e) {
    var $button = $(this);

    // Find the form within the modal
    var $form = $(this).parents('.modal').find('form');

    // CSRF
    token = $('meta[name=csrf-token]').attr('content');

    $form.prepend($('<input>', {
      name: '_csrf',
      type: 'hidden',
      value: token
     }));


    // END CSRF

    // submit the form
    $form.trigger('submit');


});

// submit the form
$('body').on('submit', '.modal-body > form', function (e) {
    e.preventDefault();

    $('.modal-body > form').ajaxSubmit({
        beforeSubmit: function () {
            $('#modalSubmitBtn').attr('disabled', 'disabled');
                // Show loading gif
                // $('.modal-footer').prepend('<img src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/0.16.1/images/loader-small.gif">');
            },

            success: function (data) {

                location.reload();

                // Reload page once information has been submitted
                $('.modal').modal('hide');
            }
        });
});

// Ajax Form Submit
$('.aSubmit').on('submit', function (e) {
    e.preventDefault();


     $('#modalSubmitBtn').addClass('disabled'); // Disables visually
     $('#modalSubmitBtn').text('<i class="fa fa-spinner fa-pulse"></i> Please Wait');

     $('.aSubmit').ajaxSubmit({
        success: function () {
               //newAlert('alert-success', 'Data Saved');
               toastr.success('Success','Information has been saved!');
           }
       });

 });

// Destroy on hide
$('.modal').on('hidden',function(e){
    $(this).remove();
});


/*
/* Ajax Submit Alert
*/
function newAlert(status, message) {
    var type = (status == 'ok') ? 'success' : 'danger';
    $("#alert-area").append($("<div class='alert alert-" + type + "fade in' data-alert><a class='close' data-dismiss='alert'>x</a> " + message + "</div>"));
    $(".alert").delay(2000).fadeOut("slow", function () { $(this).remove(); });

    //$('.alert').delay(3000).fadeOut('slow', function () {
        //    $(this).remove();
        //});
}

});