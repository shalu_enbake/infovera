class Company
include DataMapper::Resource

storage_names[:default] = 'company'

########################################################
#
# Properties
#
########################################################
property :id, Serial
property :x_id, String, required: false, unique: true, default: proc { UUIDTools::UUID.random_create }
property :name, String
property :sales_force_id, String
property :address_1, String
property :address_2, String
property :address_3, String
property :city, String
property :state, String
property :zip, String
property :county, String
property :country, String
property :phone, String
property :toll_free, String
property :website, String
property :fax, String
property :description, Text
property :structure, String
property :timezone, String
property :contact_first_name, String
property :contact_last_name, String
property :contact_title, String
property :contact_phone, String
property :contact_mobile, String
property :contact_email, String
property :ein, String, unique: true
property :ubi, String, unique: true
property :sic, String
property :duns, String
property :naics, String
property :naics_sector_code, String
property :naics_2, String
property :user_type, String
property :active, Boolean
property :revenue, Decimal, precision: 12, scale: 2
property :ebitda, Decimal, precision: 12, scale: 2
property :employees, Integer



########################################################
#
# Timestamps
#
########################################################
property :created_at, DateTime
property :updated_at, DateTime


########################################################
#
# Soft Deletes
#
########################################################
property :deleted, Boolean, default: false
property :deleted_at, DateTime


########################################################
#
#  Validations
#
########################################################
without_auto_validations do 
  property :naic_id, Integer
end
validates_within :naic_id, :set => 0..999999

########################################################
#
# Relationships
#
########################################################
has n, :users
has n, :scores
has n, :reports
has n, :affiliate_views 
has n, :affiliate_relationships
has n, :affiliate_group_companies 
has n, :company_datas
has n, :alerts
has n, :company_users
has n, :company_impact_zones
has n, :alerts
belongs_to :naic, required: false
belongs_to :naics_sector, required: false
belongs_to :naics_sub_sector, required: false
belongs_to :naics_industry_group, required: false

########################################################
#
# Callbacks for Algolia Index
#
########################################################
after :create, :save_algolia_index
after :create, :send_email_on_company_create
after :create, :save_alerts
after :update, :update_algolia_index
after :destroy, :destroy_algolia_index

########################################################
#
# Callbacks definition
#
########################################################

def send_email_on_company_create
  # Insert Mailjet
  variable = Mailjet::Send.create(messages: [{
    'From'=> { 
      'Email'=> "tim.mushen@infovera.com", 'Name'=> "InfoVera" 
    },
    'To'=> [
      { 'Email'=> "karla@infovera.com" }
    ],
    'TemplateID'=> 256962,
    'TemplateLanguage'=> true,
    'Subject'=> "New Company created.",
    'Variables'=> { 
      "companyname" => self.name,
      "naic" => self.naic.title,
      "naics_sector" => self.naics_sector.name,
      "address" => self.address_1 + "," + self.address_2 + "," + self.city + "," + self.state,
      "phone" => self.phone,
      "website" => self.website,
      "contact_phone" => self.contact_phone,
      "contact_email" => self.contact_email,
    }
  }])
  # End Mailjet  
end
def save_algolia_index
  # my_index = Algolia::Index.new(Sinatra::Application.settings.company_index)
  my_index = Algolia::Index.new(Sinatra::Application.settings.company_index)
  score = self.scores.last
  naics_name = self.naics_sector.name rescue nil
  naics_name ||= "Unknown"
  if score.nil?
   score = self.scores.new
  end


	my_index.add_object({
      :objectID => self.id,
      :name => self.name,
      :id => self.id,
      :x_id => self.x_id,
      :ubi => self.ubi,
      :ein => self.ein,
      :address => self.address_1,
      :address_2 => self.address_2,
      :city => self.city,
      :state => self.state,
      :zip => self.zip,
      :phone => self.phone,
      :website => self.website,
      :naics => self.naics,
      # :naics_name => ,
      :tevo =>score.tevo.to_i,
      :revenue => score.revenue.to_i,
      :total_asset => score.assets.to_i,
      :naics_name => naics_name,
      # :cash => score.cash,
      # :ebitda => score.ebitda,
      # :ar => score.ar,
      # :inventory => score.inventory,
      # :total_liabilities => score.total_liabilities,
      :number_of_employee => score.employees,
      # :ebitda_percent_rank => score.ebitda_percent_rank,
      # :cash_percent_rank => score.cash_percent_rank,
      # :ar_percent_rank => score.ar_percent_rank,
      # :inventory_percent_rank => score.inventory_percent_rank,
      # :rev_emp_percent_rank => score.rev_emp_percent_rank,
      # :total_liabilitie_percent_rank => score.total_liabilitie_percent_rank,
      # :revenue_percent_rank => score.revenue_percent_rank,
      # :total_assets_percent_rank => score.total_assets_percent_rank
     })
end

def update_algolia_index
  my_index = Algolia::Index.new(Sinatra::Application.settings.company_index)
  score = self.scores.last
  naics_name = self.naics_sector.name rescue nil
  naics_name ||= "Unknown"
  if score.nil?
   score = self.scores.new
  end
	my_index.save_object({
      :objectID => self.id,
      :name => self.name,
      :id => self.id,
      :x_id => self.x_id,
      :ubi => self.ubi,
      :ein => self.ein,
      :address => self.address_1,
      :address_2 => self.address_2,
      :city => self.city,
      :state => self.state,
      :zip => self.zip,
      :phone => self.phone,
      :website => self.website,
      :naics => self.naics,
      # :naics_name => ,
      :tevo =>score.tevo.to_i,
      :revenue => score.revenue.to_i,
      :total_asset => score.assets,
      :naics_name => naics_name,
      # :cash => score.cash,
      # :ebitda => score.ebitda,
      # :ar => score.ar,
      # :inventory => score.inventory,
      # :total_liabilities => score.total_liabilities,
      :number_of_employee => score.employees,
      # :ebitda_percent_rank => score.ebitda_percent_rank,
      # :cash_percent_rank => score.cash_percent_rank,
      # :ar_percent_rank => score.ar_percent_rank,
      # :inventory_percent_rank => score.inventory_percent_rank,
      # :rev_emp_percent_rank => score.rev_emp_percent_rank,
      # :total_liabilitie_percent_rank => score.total_liabilitie_percent_rank,
      # :revenue_percent_rank => score.revenue_percent_rank,
      # :total_assets_percent_rank => score.total_assets_percent_rank
    })
end

def destroy_algolia_index
	my_index = Algolia::Index.new(Sinatra::Application.settings.company_index)
	my_index.delete_object(self.id)
end

def save_alerts
  Alert.create(name: self.name, message: "Company Created.", alert_date: DateTime.now, points: 5, company_id: self.id)
end



########################################################
#
# Additional Methods
# Use SecureRandom.hex(64)
#
#
########################################################

########################################################
#
# end class
#
########################################################

end 
		