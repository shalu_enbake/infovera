class PendingCompany
include DataMapper::Resource

storage_names[:default] = 'pending_companies'

########################################################
#
# Properties
#
########################################################
property :id, Serial
property :x_id, String, required: false, unique: true, default: proc { UUIDTools::UUID.random_create }
property :name, String
property :address_1, String
property :address_2, String
property :city, String
property :state, String
property :zip, String
property :phone, String
property :website, String
property :contact_first_name, String
property :contact_last_name, String
property :contact_phone, String
property :contact_email, String
property :naics, String
property :active, Boolean, default: true
property :revenue, Decimal, precision: 12, scale: 2
property :ebitda, Decimal, precision: 12, scale: 2
property :employees, Integer
property :affiliate_id, Integer



########################################################
#
# Timestamps
#
########################################################
property :created_at, DateTime
property :updated_at, DateTime


########################################################
#
# Soft Deletes
#
########################################################
property :deleted, Boolean, default: false
property :deleted_at, DateTime


########################################################
#
#  Validations
#
########################################################

########################################################
#
# Relationships
#
########################################################

########################################################
#
# Additional Methods
# Use SecureRandom.hex(64)
#
#
########################################################

########################################################
#
# end class
#
########################################################

end 
		