class CompanyUser
include DataMapper::Resource

storage_names[:default] = 'company_users'

########################################################
#
# Properties
#
########################################################
property :id, Serial
# property :x_id, String, required: false, unique: true, default: proc { UUIDTools::UUID.random_create }
property :first_name, String
property :last_name, String
property :email, String
property :password, BCryptHash, :required => true
property :phone, String
property :title, String
property :active, Boolean, default: true



########################################################
#
# Timestamps
#
########################################################
# property :created_at, DateTime
# property :updated_at, DateTime


########################################################
#
# Soft Deletes
#
########################################################
property :deleted, Boolean, default: false
property :deleted_at, DateTime


########################################################
#
# Relationships
#
########################################################
belongs_to :company
has n, :achievements

########################################################
#
# Callbacks for Algolia Index
#
########################################################

########################################################
#
# Hooks
#
########################################################

########################################################
#
# Callbacks definition
#
########################################################

########################################################
#
# Additional Methods
# Use SecureRandom.hex(64)
#
#
########################################################

########################################################
#
# end class
#
########################################################

end 
		