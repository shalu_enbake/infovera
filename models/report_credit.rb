class ReportCredit
include DataMapper::Resource

storage_names[:default] = 'report_credit'

########################################################
#
# Properties
#
########################################################
property :id, Serial, field: 'Id'
property :x_id, String, required: false, unique: true, default: proc { UUIDTools::UUID.random_create }
property :report_level, String, field: 'Report Level'
property :comments, String, field: 'Comments'
property :credit_date, DateTime
property :used, Boolean, default: false 
property :score_id, Integer, default: 0
property :date_used, DateTime


########################################################
#
# Timestamps
#
########################################################
property :created_at, DateTime



########################################################
#
# Soft Deletes
#
########################################################
property :deleted, Boolean, default: false
property :deleted_at, DateTime

########################################################
#
# Relationships
#
########################################################
belongs_to :affiliate
belongs_to :score, required: false

########################################################
#
# Hooks
#
########################################################


########################################################
#
# Additional Methods
# Use SecureRandom.hex(64)
#
#
########################################################


########################################################
#
# end class
#
########################################################

end 