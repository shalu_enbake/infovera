class Academy
include DataMapper::Resource

storage_names[:default] = 'academy'

########################################################
#
# Properties
#
########################################################
property :id, Serial
property :x_id, UUID, required: true, unique: true, default: proc { UUIDTools::UUID.random_create }
property :name, String
property :desc, String
property :url, Text
property :video_image, String
property :video_image_secure_url, String
property :is_overview, Boolean
property :is_how_to, Boolean
property :is_event_webinar, Boolean
property :is_training_webinar, Boolean
property :priority, Integer
property :only_pro_affiliate, Boolean, default: false
########################################################
#
# Timestamps
#
########################################################
property :created_at, DateTime
property :updated_at, DateTime


########################################################
#
# Soft Deletes
#
#######################################################
property :deleted, Boolean, default: false
property :deleted_at, DateTime

########################################################
#
# Relationships
#
########################################################
has n, :academy_affiliateusers

########################################################
#
# Hooks
#
########################################################


########################################################
#
# Additional Methods
# Use SecureRandom.hex(64)
#
#
########################################################


########################################################
#
# end class
#
########################################################

end 