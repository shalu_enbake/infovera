class Report
include DataMapper::Resource

storage_names[:default] = 'reports'

########################################################
#
# Properties
#
########################################################
property :id, Serial
property :x_id, UUID, required: true, unique: true, default: proc { UUIDTools::UUID.random_create }
property :active, Boolean, default: true
property :date_created, DateTime
property :tevo, Integer
property :level, Integer
property :s3_path, String
property :affiliate_user_id, Integer, default: 0
property :manually_created, Boolean, default: false
########################################################
#
# Timestamps
#
########################################################
property :created_at, DateTime
property :updated_at, DateTime


########################################################
#
# Soft Deletes
#
########################################################
property :deleted, Boolean, default: false
property :deleted_at, DateTime


########################################################
#
# Relationships
#
########################################################
belongs_to :company , required: false
belongs_to :score , required: false
belongs_to :affiliate , required: false
# belongs_to :affiliate_user, required: false

########################################################
#
# Callbacks
#
########################################################
after :create, :save_alerts
# after :destroy, :save_alerts_on_destroy

########################################################
#
# Callbacks definition
#
########################################################

def save_alerts
  Alert.create(name: self.affiliate.name, message: "#{self.affiliate.name} created a report for #{self.company.name}", alert_date: DateTime.now, points: 10, company_id: self.company.id, affiliate_id: self.affiliate.id)
end

########################################################
#
# Additional Methods
# Use SecureRandom.hex(64)
#
#
########################################################


########################################################
#
# end class
#
########################################################

end 
		