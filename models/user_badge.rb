class UserBadge
include DataMapper::Resource

storage_names[:default] = 'user_badges'

########################################################
#
# Properties
#
########################################################
property :id, Serial
property :x_id, UUID, required: true, unique: true, default: proc { UUIDTools::UUID.random_create }
property :active, Boolean, default: true
property :sign_in_count, Integer,  default: 0
property :first_company_searched, Boolean, default: false
property :first_group_created, Boolean, default: false
property :first_score_created, Boolean, default: false
property :first_company_created, Boolean, default: false
property :first_network_request, Boolean, default: false
property :first_company_added_to_group, Boolean, default: false
property :first_network_connection_accept, Boolean, default: false
property :first_company_user_created, Boolean, default: false
property :first_video_completed, Boolean, default: false
property :first_company_user_logged_in, Boolean, default: false
property :first_logo_upload, Boolean, default: false

########################################################
#
# Timestamps
#
########################################################
property :created_at, DateTime
property :updated_at, DateTime


########################################################
#
# Soft Deletes
#
########################################################
property :deleted, Boolean, default: false
property :deleted_at, DateTime


########################################################
#
# Relationships
#
########################################################
belongs_to :company_user, required: false
belongs_to :affiliate_user, required: false

########################################################
#
# Hooks
#
########################################################

########################################################
#
# Additional Methods
# Use SecureRandom.hex(64)
#
#
########################################################


########################################################
#
# end class
#
########################################################

end 