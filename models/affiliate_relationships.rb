class AffiliateRelationship
include DataMapper::Resource

storage_names[:default] = 'affiliate_relationships'

########################################################
#
# Properties
#
########################################################
property :id, Serial
property :x_id, UUID, required: true, unique: true, default: proc { UUIDTools::UUID.random_create }
property :active, Boolean, default: true
property :allow_report_view, Boolean, default: true 
property :company_approved, Boolean, default: false 
property :company_approved_date, DateTime
property :company_rejected, Boolean, default: false 
property :company_rejected_date, DateTime
property :company_requested, Boolean, default: false 
property :company_requested_date, DateTime
property :affiliate_approved, Boolean, default: false 
property :affiliate_approved_date, DateTime
property :affiliate_rejected, Boolean, default: false 
property :affiliate_rejected_date, DateTime
property :affiliate_requested, Boolean, default: false 
property :affiliate_requested_date, DateTime
property :is_form_company, Boolean, default: false
property :is_form_affiliate, Boolean, default: false

########################################################
#
# Timestamps
#
########################################################
property :created_at, DateTime
property :updated_at, DateTime


########################################################
#
# Soft Deletes
#
########################################################
property :deleted, Boolean, default: false
property :deleted_at, DateTime


########################################################
#
# Relationships
#
########################################################
belongs_to :company 
belongs_to :affiliate 

########################################################
#
# Hooks
#
########################################################

########################################################
#
# Callbacks
#
########################################################
after :create, :save_alerts
after :create, :send_mail_to_affiliate
after :destroy, :save_alerts_on_destroy

########################################################
#
# Callbacks definition
#
########################################################

def save_alerts
  Alert.create(name: self.affiliate.name, message: "#{self.affiliate.name} affiliate connected to #{self.company.name} company.", alert_date: DateTime.now, points: 10, company_id: self.affiliate.id, affiliate_id: self.company.id)
end

def save_alerts_on_destroy
  Alert.create(name: self.affiliate.name, message: "#{self.affiliate.name} affiliate dis-connected to #{self.company.name} company.", alert_date: DateTime.now, points: 10, company_id: self.affiliate.id, affiliate_id: self.company.id)
end

def send_mail_to_affiliate
	if self.affiliate.email.present?
		# Insert Mailjet
		variable = Mailjet::Send.create(messages: [{
		'From'=> { 
			'Email'=> "tim.mushen@infovera.com", 'Name'=> "InfoVera" 
		},
		'To'=> [
			{ 'Email'=> self.affiliate.email }
		],
		'TemplateID'=> 266360,
		'TemplateLanguage'=> true,
		'Subject'=> "Request a relationship by an Affiliate: #{self.affiliate.name} to Company: #{self.company.name}",
		'Variables'=> { 
			"affiliate" => self.affiliate.name,
			"company" => self.company.name,
			"ubi" => (self.company.ubi rescue nil)
		}
		}])
		# End Mailjet
	end
end


########################################################
#
# Additional Methods
# Use SecureRandom.hex(64)
#
#
########################################################


########################################################
#
# end class
#
########################################################

end 