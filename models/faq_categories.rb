class FaqCategory
include DataMapper::Resource

storage_names[:default] = 'faq_categories'

########################################################
#
# Properties
#
########################################################
property :id, Serial
property :x_id, UUID, required: true, unique: true, default: proc { UUIDTools::UUID.random_create }
property :active, Boolean, default: true
property :name, String
property :cat_order, Integer, default: 500

########################################################
#
# Timestamps
#
########################################################
property :created_at, DateTime
property :updated_at, DateTime


########################################################
#
# Soft Deletes
#
########################################################
property :deleted, Boolean, default: false
property :deleted_at, DateTime


########################################################
#
# Relationships
#
########################################################
has n, :faqs

########################################################
#
# Hooks
#
########################################################


########################################################
#
# Additional Methods
# Use SecureRandom.hex(64)
#
#
########################################################


########################################################
#
# end class
#
########################################################

end 
		