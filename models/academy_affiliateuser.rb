class AcademyAffiliateuser
include DataMapper::Resource

storage_names[:default] = 'academy_affiliateusers'

########################################################
#
# Properties
#
########################################################
property :id, Serial
property :x_id, UUID, required: true, unique: true, default: proc { UUIDTools::UUID.random_create }
property :completed, Boolean ,default: false
########################################################
#
# Timestamps
#
########################################################
property :created_at, DateTime
property :updated_at, DateTime


########################################################
#
# Soft Deletes
#
#######################################################
property :deleted, Boolean, default: false
property :deleted_at, DateTime

########################################################
#
# Relationships
#
########################################################


########################################################
#
# Hooks
#
########################################################
belongs_to :affiliate_user , required: false
belongs_to :academy, required: false

########################################################
#
# Additional Methods
# Use SecureRandom.hex(64)
#
#
########################################################


########################################################
#
# end class
#
########################################################

end 