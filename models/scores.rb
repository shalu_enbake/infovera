class Score
include DataMapper::Resource

storage_names[:default] = 'scores'

########################################################
#
# Properties
#
########################################################
property :id, Serial
property :x_id, UUID, required: true, unique: true, default: proc { UUIDTools::UUID.random_create }
property :active, Boolean, default: true
property :level, String
property :score_date, DateTime
property :revenue, Decimal, precision: 12, scale: 2
property :assets, Decimal, precision: 12, scale: 2
property :employees, Integer
property :approved, Boolean, default: false

# Level 1
property :asset_class, String
property :sector_asset_class_combo, String
property :naics, String
property :naics_sector, String
property :sector_asset_class_key, String
property :sac_intercept_estimate, String
property :sac_intercept, String 
property :log_of_revenue, String
property :asset_lookup_value, String
property :naics_name, String
property :ebitda, String
property :net_income, String

# Level 2
property :ar, String
property :cash, String
property :tevo, String
property :tevo_upper, String
property :tevo_lower, String
property :tevo_median, String
property :tevo_min, String
property :tevo_max, String
property :inventory, String
property :total_liabilities, Decimal, precision: 12, scale: 2
property :number_of_employee, String
property :asset_class, String
property :upper_estimate, String
property :lower_estimate, String
property :base_tevo, String

# Multiples
property :multiple_revenue_lower, String
property :multiple_revenue_estimate, String
property :multiple_revenue_upper, String

property :multiple_ebitda_lower, String
property :multiple_ebitda_estimate, String
property :multiple_ebitda_upper, String

property :multiple_net_income_lower, String
property :multiple_net_income_estimate, String
property :multiple_net_income_upper, String

property :multiple_assets_lower, String
property :multiple_assets_estimate, String
property :multiple_assets_upper, String

# Percentiles
property :percentile_revenue, String
property :percentile_ebitda, String
property :percentile_cash, String
property :percentile_ar, String
property :percentile_inventory, String
property :percentile_assets, String
property :percentile_total_liabilities, String
property :percentile_rev_emp, String

# Waterfall
property :waterfall_asset_revenue, String
property :waterfall_ebitda, String
property :waterfall_cash, String
property :waterfall_ar, String
property :waterfall_inventory, String
property :waterfall_total_liabilities, String
property :waterfall_rev_emp, String
property :waterfall_assets, String
property :waterfall_revenue, String



########################################################
#
# Timestamps
#
########################################################
property :created_at, DateTime
property :updated_at, DateTime


########################################################
#
# Soft Deletes
#
########################################################
property :deleted, Boolean, default: false
property :deleted_at, DateTime


########################################################
#
# Relationships
#
########################################################
belongs_to :company
has n, :reports
belongs_to :affiliate_user, required: false
########################################################
#
# Hooks
#
########################################################

########################################################
#
# Callbacks
#
########################################################
after :create, :save_score_to_algolia
after :create, :save_alerts
after :create, :send_mail_to_company_users

########################################################
#
# Callbacks definition
#
########################################################
def save_score_to_algolia
  my_index = Algolia::Index.new(Sinatra::Application.settings.affiliate_index)
  my_index.add_object({
    :objectID => self.id,
    :level => self.level,
    :id => self.id,
    :x_id => self.x_id.to_s,
    :tevo => self.tevo,
    :revenue => self.revenue,
    :total_asset => self.assets,
    :number_of_employee => self.employees,
    :company_id => self.company_id,
  })
  company = self.company
  company_index = Algolia::Index.new(Sinatra::Application.settings.company_index)
  naics_name = company.naics_sector.name rescue nil
  naics_name ||= "Unknown"
  company_index.partial_update_object({ "tevo": self.tevo.to_i, "total_asset": self.assets, "number_of_employee": self.employees, "objectID": company.id.to_s})
end

def save_alerts
  Alert.create(name: self.company.name, message: "#{self.company.name} created a score.", alert_date: DateTime.now, points: 10, company_id: self.company.id, score_id: self.id)
end

def send_mail_to_company_users
  if self.company.company_users.present?
    recipients = []
    self.company.company_users.each do |user|
      recipients <<  {'Email'=> user.email, 'Name'=> "#{user.first_name rescue nil}" + " " + "#{user.last_name rescue nil}"}
    end
    variable = Mailjet::Send.create(messages: [{
      'From'=> {
          'Email'=> "tim.mushen@infovera.com", 'Name'=> "InfoVera" 
      },
      'To'=> recipients,
      'TemplateID'=> 289039,
      'TemplateLanguage'=> true,
      'Subject'=> 'New Score is Created.',
      "Variables": {
        "company": self.company.name,
        "affiliate_user": "#{self.affiliate_user.first_name rescue nil}" + " " + "#{self.affiliate_user.last_name rescue nil}",
        "url": "#{Sinatra::Application.settings.app_url}/account/scores/"
      }
    }])
  end
end


########################################################
#
# Additional Methods
# Use SecureRandom.hex(64)
#
#
########################################################


########################################################
#
# end class
#
########################################################

end 
    