#recurring_services Class
class AffiliateCategory
include DataMapper::Resource
storage_names[:default] = 'affiliate_categories'

########################################################
#
# Properties
#
########################################################
property :id, Serial
# property :sales_force_id, String, field: 'SalesForceId'
property :x_id, UUID, required: true, unique: true, default: proc { UUIDTools::UUID.random_create }
property :name, String

########################################################
#
# Timestamps
#
########################################################
property :created_at, DateTime, field: 'CreatedOn'
property :updated_at, DateTime, field: 'ModifiedOn'


########################################################
#
# Relationships
#
########################################################
has n, :affiliate_subcategories
has n, :affiliate_specialties
########################################################
#
# Validations
#
########################################################

validates_uniqueness_of :name

########################################################
#
# Hooks
#
########################################################


########################################################
#
# Additional Methods
#
########################################################


########################################################
#
# end class
#
########################################################

end
