#recurring_services Class
class AffiliateSubcategory
include DataMapper::Resource
storage_names[:default] = 'affiliate_subcategories'

########################################################
#
# Properties
#
########################################################
property :id, Serial
# property :sales_force_id, String, field: 'SalesForceId'
property :x_id, UUID, required: true, unique: true, default: proc { UUIDTools::UUID.random_create }
property :name, String

########################################################
#
# Timestamps
#
########################################################
property :created_at, DateTime, field: 'CreatedOn'
property :updated_at, DateTime, field: 'ModifiedOn'


########################################################
#
# Relationships
#
########################################################
belongs_to :affiliate_category
has n, :affiliate_specialties

########################################################
#
# Validations
#
########################################################

validates_uniqueness_of :name, :scope => :affiliate_category_id,
    :message => "There's already a category of that title in this section"

########################################################
#
# Hooks
#
########################################################


########################################################
#
# Additional Methods
#
########################################################


########################################################
#
# end class
#
########################################################

end
