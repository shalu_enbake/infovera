class CompanyData
include DataMapper::Resource

storage_names[:default] = 'company_data'

########################################################
#
# Properties
#
########################################################
property :id, Serial
property :x_id, String, required: false, unique: true, default: proc { UUIDTools::UUID.random_create }
property :company_name, String
property :end_user_contact_name, String
property :end_user_phone_number, String
property :end_user_email, String
property :authorization, Boolean
property :percentage_of_annual_revenue, String
property :margin_percentag, String
property :sell_primarily_to_business_or_customer, String
property :management_team_length, Integer
property :naic_title, String
property :number_of_ftes, Integer
property :number_of_managers, Integer
property :number_of_partners, Integer
property :intended_use_for_assessment, Boolean
property :succession, String
property :sale, String
property :acquistion, String
property :value_build, String
property :compensation_planning, String
property :type_of_organization, String
property :discribe_industry, String
property :total_revenue, Decimal, precision: 12, scale: 2
property :ebitda, Decimal, precision: 12, scale: 2
property :valuation_of_assets, Decimal, precision: 12, scale: 2
property :total_liabilities, Decimal, precision: 12, scale: 2
property :business_profilability_compared, String
property :dollar_value_normalization_adjustment, String
property :describe_one_time_and_recurring_sales, String
property :top_ten_customer_revenue_percentage, String
property :agreements_with_customers, String
property :business_system_and_process, String
property :quality_of_information_system, String
property :strength_of_management_team, String
property :talent_capability_culture, String
property :reliance_on_owner, String
property :success_plan, String
property :revenue_growth, String
property :margin_improvement, String
property :other_opportunities, String
property :scalability, String
property :product_extension, String
property :organization, String
property :level, String
property :cash, Decimal, precision: 12, scale: 2
property :ar, Decimal, precision: 12, scale: 2
property :inventory, Decimal, precision: 12, scale: 2
property :revenue_per_employee, Decimal, precision: 12, scale: 2
property :employee, String


########################################################
#
# Timestamps
#
########################################################
property :created_at, DateTime
property :updated_at, DateTime


########################################################
#
# Soft Deletes
#
########################################################
property :deleted, Boolean, default: false
property :deleted_at, DateTime


########################################################
#
#  Validations
#
########################################################


########################################################
#
# Relationships
#
########################################################
belongs_to :affiliate, required: false
belongs_to :naic, required: false
belongs_to :user, required: false
belongs_to :company, required: false

########################################################
#
# Callbacks definition
#
########################################################


########################################################
#
# Additional Methods
# Use SecureRandom.hex(64)
#
#
########################################################

########################################################
#
# end class
#
########################################################

end 
		