class AffiliateAchievement
include DataMapper::Resource

storage_names[:default] = 'affiliate_achievements'

########################################################
#
# Properties
#
########################################################
property :id, Serial
property :x_id, UUID, required: true, unique: true, default: proc { UUIDTools::UUID.random_create }

########################################################
#
# Timestamps
#
########################################################
property :created_at, DateTime
property :updated_at, DateTime


########################################################
#
# Soft Deletes
#
########################################################
property :deleted, Boolean, default: false
property :deleted_at, DateTime


########################################################
#
# Relationships
#
########################################################
belongs_to :affiliate, required: false


########################################################
#
# Hooks
#
########################################################


########################################################
#
# Additional Methods
# Use SecureRandom.hex(64)
#
#
########################################################


########################################################
#
# end class
#
########################################################

end 