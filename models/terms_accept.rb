class TermsAccepted
include DataMapper::Resource

storage_names[:default] = 'terms_accepted'

########################################################
#
# Properties
#
########################################################
property :id, Serial
property :x_id, UUID, required: true, unique: true, default: proc { UUIDTools::UUID.random_create }
property :active, Boolean, default: true
property :accepted_on, DateTime 
property :affiliate_id, Integer, default: 0
property :affiliate_user_id, Integer, default: 0
property :user_id, Integer, default: 0
property :company_id, Integer, default: 0

########################################################
#
# Timestamps
#
########################################################
property :created_at, DateTime
property :updated_at, DateTime


########################################################
#
# Soft Deletes
#
########################################################
property :deleted, Boolean, default: false
property :deleted_at, DateTime


########################################################
#
# Relationships
#
########################################################
belongs_to :term
belongs_to :affiliate, required: false
belongs_to :affiliate_user, required: false 
belongs_to :user, required: false 
belongs_to :company, required: false

########################################################
#
# Hooks
#
########################################################


########################################################
#
# Additional Methods
# Use SecureRandom.hex(64)
#
#
########################################################


########################################################
#
# end class
#
########################################################

end 
		