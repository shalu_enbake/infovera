class Affiliate
include DataMapper::Resource

storage_names[:default] = 'affiliates'

########################################################
#
# Properties
#
########################################################
property :id, Serial
property :x_id, UUID, required: true, unique: true, default: proc { UUIDTools::UUID.random_create }
property :active, Boolean, default: true
property :name, String
property :address, String
property :address_2, String 
property :city, String 
property :state, String
property :zip, String 
property :country, String
property :phone, String 
property :email, String 
property :web, String 
property :affiliate_type, String, default: "Affiliate"
property :contact_first_name, String
property :contact_last_name, String
property :contact_title, String
property :contact_email, String
property :contact_phone, String
property :description, Text 
property :summary, Text
property :logo_image_name, String 
property :logo_secure_url, String 
property :logo_bytes, String 
property :logo_format, String
property :logo_width, String 
property :logo_height, String 
property :latitude, String
property :longitude, String
property :facebook, String
property :linkedin, String
property :blog, String
property :proaffiliate_id, Integer,required: false

########################################################
#
# Timestamps
#
########################################################
property :created_at, DateTime
property :updated_at, DateTime


########################################################
#
# Soft Deletes
#
########################################################
property :deleted, Boolean, default: false
property :deleted_at, DateTime


########################################################
#
# Relationships
#
########################################################
has n, :affiliate_relationships
has n, :affiliate_users 
has n, :affiliate_groups
has n, :affiliate_views 
has n, :affiliate_group_companies
has n, :terms_accepted
has n, :affiliate_achievements
has n, :reports
has n, :report_credits
has n, :affiliate_report_logs 
has n, :affiliate_specialties
has n, :alerts
has n, :affiliate_proaffiliate_relationships
has n, :forecasts
belongs_to :affiliate_level_plan, required: false

########################################################
#
# Hooks
#
########################################################

########################################################
#
# Callbacks for Algolia Index
#
########################################################
after :create, :save_algolia_index
after :create, :save_alerts
after :update, :update_algolia_index
after :destroy, :destroy_algolia_index

########################################################
#
# Callbacks definition
#
########################################################
def save_algolia_index
	my_index = Algolia::Index.new(Sinatra::Application.settings.affiliate_index)
	my_index.add_object({
      :objectID => self.id,
      :id => self.id,
      :x_id => self.x_id,
      :name => self.name,
      :phone => self.phone, 
      :email => self.email,
      :web => self.web,
      :secure_logo_url => self.logo_secure_url,
      :affiliate_type => self.affiliate_type,
      :state => self.state
     })
end

def update_algolia_index
	my_index = Algolia::Index.new(Sinatra::Application.settings.affiliate_index)
	my_index.save_object({
      :objectID => self.id,
      :id => self.id,
      :x_id => self.x_id,
      :name => self.name,
      :phone => self.phone, 
      :email => self.email,
      :web => self.web,
      :secure_logo_url => self.logo_secure_url,
      :affiliate_type => self.affiliate_type,
      :state => self.state
    })
end

def destroy_algolia_index
	my_index = Algolia::Index.new(Sinatra::Application.settings.affiliate_index)
	my_index.delete_object(self.id)
end

def save_alerts
      Alert.create(name: self.name, message: "Affiliate Created.", alert_date: DateTime.now, points: 5, affiliate_id: self.id)
end

########################################################
#
# Additional Methods
# Use SecureRandom.hex(64)
#
#
########################################################


########################################################
#
# end class
#
########################################################

end 