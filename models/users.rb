#recurring_services Class
class User
include DataMapper::Resource
storage_names[:default] = 'users'

########################################################
#
# Properties
#
########################################################
property :id, Serial
# property :sales_force_id, String, field: 'SalesForceId'
property :user_name, String
property :password, BCryptHash, :required => true
property :first_name, String
property :last_name, String
property :email, String
property :work_phone, String
property :mobile, String
property :role, String
property :user_type, String
property :active, Boolean, default: true
property :company_id, Integer
# property :password_reset, Boolean, default: false

########################################################
#
# Timestamps
#
########################################################
property :created_at, DateTime, field: 'CreatedOn'
property :updated_at, DateTime, field: 'ModifiedOn'


########################################################
#
# Relationships
#
########################################################
belongs_to :company
has n, :affiliate_views 

########################################################
#
# Hooks
#
########################################################


########################################################
#
# Additional Methods
#
########################################################


########################################################
#
# end class
#
########################################################

end 