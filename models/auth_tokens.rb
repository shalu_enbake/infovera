class AuthTokens
require 'securerandom.rb'
include DataMapper::Resource

storage_names[:default] = 'auth_tokens'

########################################################
#
# Properties
#
########################################################
property :id, Serial
property :x_id, UUID, required: true, unique: true, default: proc { UUIDTools::UUID.random_create }
property :token, String

########################################################
#
# Timestamps
#
########################################################
property :created_at, DateTime
property :updated_at, DateTime


########################################################
#
# Soft Deletes
#
#######################################################
property :deleted, Boolean, default: false
property :deleted_at, DateTime

########################################################
#
# Relationships
#
########################################################


########################################################
#
# Hooks
#
########################################################
before :create, :create_token


########################################################
#
# Additional Methods
# Use SecureRandom.hex(64)
#
#
########################################################
def create_token
	self.token = SecureRandom.urlsafe_base64
end

########################################################
#
# end class
#
########################################################

end 