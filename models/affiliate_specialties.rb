#recurring_services Class
class AffiliateSpecialty
include DataMapper::Resource
storage_names[:default] = 'affiliate_specialties'

########################################################
#
# Properties
#
########################################################
property :id, Serial
# property :sales_force_id, String, field: 'SalesForceId'
property :x_id, UUID, required: true, unique: true, default: proc { UUIDTools::UUID.random_create }


########################################################
#
# Timestamps
#
########################################################
property :created_at, DateTime, field: 'CreatedOn'
property :updated_at, DateTime, field: 'ModifiedOn'


########################################################
#
# Relationships
#
########################################################

belongs_to	:affiliate
belongs_to	:affiliate_category
belongs_to	:affiliate_subcategory

########################################################
#
# Validations
#
########################################################



########################################################
#
# Hooks
#
########################################################


########################################################
#
# Additional Methods
#
########################################################


########################################################
#
# end class
#
########################################################

end
