class CompanyBaseScore
include DataMapper::Resource

storage_names[:default] = 'company_bases'

########################################################
#
# Properties
#
########################################################
property :id, Serial
property :x_id, UUID, required: true, unique: true, default: proc { UUIDTools::UUID.random_create }
property :performance_analysis, String
property :importance, String
property :comments, String
property :possible, Integer
property :assessed, String
property :company_score, Integer
property :impact_zone, Integer

########################################################
#
# Timestamps
#
########################################################
property :created_at, DateTime
property :updated_at, DateTime


########################################################
#
# Soft Deletes
#
########################################################
property :deleted, Boolean, default: false
property :deleted_at, DateTime


########################################################
#
# Relationships
#
########################################################
belongs_to :score

########################################################
#
# Hooks
#
########################################################

########################################################
#
# Callbacks for Algolia Index
#
########################################################

########################################################
#
# Callbacks definition
#
########################################################

########################################################
#
# Additional Methods
# Use SecureRandom.hex(64)
#
#
########################################################


########################################################
#
# end class
#
########################################################

end 