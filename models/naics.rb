class Naic
include DataMapper::Resource

storage_names[:default] = 'naics'

########################################################
#
# Properties
#
########################################################
property :id, Serial, field: 'Id'
property :code, String, field: 'Code'
property :title, String, field: 'Title'
property :level, String, field: 'Level'

########################################################
#
# Timestamps
#
########################################################
# property :created_at, DateTime
# property :updated_at, DateTime


########################################################
#
# Soft Deletes
#
########################################################
# property :deleted, Boolean, default: false
# property :deleted_at, DateTime

########################################################
#
# Relationships
#
########################################################
has n, :companies


########################################################
#
# Hooks
#
########################################################


########################################################
#
# Additional Methods
# Use SecureRandom.hex(64)
#
#
########################################################


########################################################
#
# end class
#
########################################################

end 