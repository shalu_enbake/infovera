class AffiliateProaffiliateRelationship
include DataMapper::Resource

storage_names[:default] = 'affiliate_proaffiliate_relationships'

########################################################
#
# Properties
#
########################################################
property :id, Serial
property :x_id, UUID, required: true, unique: true, default: proc { UUIDTools::UUID.random_create }
property :active, Boolean, default: true
property :affiliate_id, Integer
property :proaffiliate_id, Integer 
property :start_date, DateTime, default: proc { Time.now }

########################################################
#
# Timestamps
#
########################################################
property :created_at, DateTime
property :updated_at, DateTime


########################################################
#
# Soft Deletes
#
########################################################
property :deleted, Boolean, default: false
property :deleted_at, DateTime


########################################################
#
# Relationships
#
########################################################
belongs_to :affiliate
belongs_to :proaffiliate, 'Affiliate', child_key: ['proaffiliate_id'], parent_key: ['affiliate_id']

########################################################
#
# Hooks
#
########################################################


########################################################
#
# Additional Methods
# Use SecureRandom.hex(64)
#
#
########################################################


########################################################
#
# end class
#
########################################################

end 
		