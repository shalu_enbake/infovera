class Alert
include DataMapper::Resource

storage_names[:default] = 'alerts'

########################################################
#
# Properties
#
########################################################
property :id, Serial
property :x_id, UUID, required: true, unique: true, default: proc { UUIDTools::UUID.random_create }
property :name, String
property :message, String
property :alert_date, DateTime
property :points, Integer

########################################################
#
# Timestamps
#
########################################################
property :created_at, DateTime
property :updated_at, DateTime


########################################################
#
# Soft Deletes
#
########################################################
property :deleted, Boolean, default: false
property :deleted_at, DateTime

########################################################
#
# Relationships
#
########################################################
belongs_to :company, required: false
belongs_to :affiliate, required: false
belongs_to :score, required: false


########################################################
#
# Hooks
#
########################################################


########################################################
#
# Additional Methods
# Use SecureRandom.hex(64)
#
#
########################################################


########################################################
#
# end class
#
########################################################

end 