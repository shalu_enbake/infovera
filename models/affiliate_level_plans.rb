class AffiliateLevelPlan
include DataMapper::Resource

storage_names[:default] = 'affiliate_level_plans'

########################################################
#
# Properties
#
########################################################
property :id, Serial
property :x_id, UUID, required: true, unique: true, default: proc { UUIDTools::UUID.random_create }
property :title, String
property :affiliate_type, String
property :report_credits, Integer, default: 0
property :amount, Integer

########################################################
#
# Timestamps
#
########################################################
# property :created_at, DateTime
# property :updated_at, DateTime


########################################################
#
# Soft Deletes
#
########################################################
# property :deleted, Boolean, default: false
# property :deleted_at, DateTime

########################################################
#
# Relationships
#
########################################################
has n, :affiliates


########################################################
#
# Hooks
#
########################################################


########################################################
#
# Additional Methods
# Use SecureRandom.hex(64)
#
#
########################################################


########################################################
#
# end class
#
########################################################

end 