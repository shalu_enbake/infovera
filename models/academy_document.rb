class AcademyDocument
include DataMapper::Resource

storage_names[:default] = 'academy_document'

########################################################
#
# Properties
#
########################################################
property :id, Serial
property :x_id, UUID, required: true, unique: true, default: proc { UUIDTools::UUID.random_create }
property :name, String
property :upload_name, String
property :desc, String
property :s3_path, Text
property :priority, Integer
property :only_pro_affiliate, Boolean, default: false

########################################################
#
# Timestamps
#
########################################################
property :created_at, DateTime
property :updated_at, DateTime


########################################################
#
# Soft Deletes
#
#######################################################
property :deleted, Boolean, default: false
property :deleted_at, DateTime

########################################################
#
# Relationships
#
########################################################
belongs_to :academy, required: false

########################################################
#
# Hooks
#
########################################################


########################################################
#
# Additional Methods
# Use SecureRandom.hex(64)
#
#
########################################################


########################################################
#
# end class
#
########################################################

end 