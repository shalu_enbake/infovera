class Forecast
include DataMapper::Resource

storage_names[:default] = 'forecasts'

########################################################
#
# Properties
#
########################################################
property :id, Serial
property :x_id, UUID, required: true, unique: true, default: proc { UUIDTools::UUID.random_create }
property :end_date, DateTime
property :company_id , Integer
property :level , String
########################################################
#
# Timestamps
#
########################################################
property :created_at, DateTime
property :updated_at, DateTime


########################################################
#
# Soft Deletes
#
########################################################
property :deleted, Boolean, default: false
property :deleted_at, DateTime


########################################################
#
# Relationships
#
########################################################
belongs_to :affiliate
belongs_to :company, required: false
########################################################
#
# Hooks
#
########################################################

########################################################
#
# Callbacks 
#
########################################################
after :create, :send_notification

########################################################
#
# Callbacks definition
#
########################################################

def send_notification
	# Insert Mailjet
	if Sinatra::Application.environment == :staging
		variable = Mailjet::Send.create(messages: [{
			'From'=> { 
				'Email'=> "tim.mushen@infovera.com", 'Name'=> "InfoVera" 
			},
			'To'=> [
				{ 'Email'=> "alex.entrekin@gmail.com", 'Name' => "Alex" },
				{ 'Email'=> "tim.mushen@infovera.com", 'Name' => "Tim Mushen" }
			],
			'TemplateID'=> 286468,
			'TemplateLanguage'=> true,
			'Subject'=> "New Forecast is created."
		}])
	else
		variable = Mailjet::Send.create(messages: [{
			'From'=> { 
				'Email'=> "tim.mushen@infovera.com", 'Name'=> "InfoVera" 
			},
			'To'=> [
				{ 'Email'=> Sinatra::Application.settings.forecast_user_email
				}
			],
			'TemplateID'=> 286468,
			'TemplateLanguage'=> true,
			'Subject'=> "New Forecast is created."
		}])
	end
	# End Mailjet
end
########################################################
#
# Additional Methods
# Use SecureRandom.hex(64)
#
#
########################################################


########################################################
#
# end class
#
########################################################

end 