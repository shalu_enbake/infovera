class TevoReport
include DataMapper::Resource

storage_names[:default] = 'reports'

########################################################
#
# Properties
#
########################################################
property :id, Serial
# property :user_name, String
# property :transaction_id, String
# property :company_id, Integer
property :level, String
# property :generated_by_user_id, String
# property :generated_by_user_type, String
# property :is_visible, String
# property :industry_code, String
# property :company_name, String
# property :company_description, String
# property :hierarchy_level, String
# property :revenue, String
# property :total_asset, String
# property :cash, String
# property :ebitda, String
# property :ar, String
# property :inventory, String
# property :total_liabilities, String
# property :number_of_employee, String
# property :created_at, DateTime
# property :is_outdated, String
# property :asset_class, String
# property :sector_asset_class_combo, String
# property :sac_value, String
# property :look_up_key, String
# property :log_of_revenue, String
# property :lnrx_log_of_revenue, String
# property :cal_c, String
# property :rank_of_estimate, String
# property :upper_estimate, String
# property :lower_estimate, String
property :tevo, Integer
# property :upper_tevo, String
# property :lower_tevo, String
property :min_tevo, String
property :max_tevo, String
# property :mediean_tevo, String, field: "MedieanTevo"
# property :lower_adjustmentlimit, String, field: "LowerAdjustmentlimit"
# property :upper_adjustment_limit, String, field: "UpperAdjustmentLimit"

# property :ebitda_percent_rank, String, field: "EBITDAPercentRank"
# property :cash_percent_rank, String, field: "CashPercentRank"
# property :ar_percent_rank, String, field: "ARPercentRank"
# property :inventory_percent_rank, String, field: "InventoryPercentRank"
# property :rev_emp_percent_rank, String, field: "RevEmpPercentRank"
# property :total_liabilitie_percent_rank, String, field: "TotalLiabilitiePercentRank"
# property :revenue_percent_rank, String, field: "RevenuePercentRank"
# property :total_assets_percent_rank, String, field: "TotalAssetsPercentRank"
# property :rev_emp_tevo_adjustment, String, field: "RevEmpTevoAdjustment"
# property :ebitda_tevo_adjustment, String, field: "EBITDATevoAdjustment"
# property :cash_tevo_adjustment, String, field: "CashTevoAdjustment"
# property :ar_tevo_adjustment, String, field: "ARTevoAdjustment"
# property :inventory_tevo_adjustment, String, field: "InventoryTevoAdjustment"
# property :total_liabilitie_tevo_adjustment, String, field: "TotalLiabilitieTevoAdjustment"
# property :total_adjustment, String, field: "TotalAdjustment"
property :final_tevo_score, String
# property :ebitda_midpoints, String, field: "EBITDAMidpoints"
# property :cash_midpoints, String, field: "CashMidpoints"
# property :ar_midpoints, String, field: "ARMidpoints"
# property :inventory_midpoints, String, field: "InventoryMidpoints"
# property :total_liabilities_midpoints, String, field: "TotalLiabilitiesMidpoints"
# property :rev_emp_midpoints, String, field: "RevEmpMidpoints"
# property :revenue_midpoints, String, field: "RevenueMidpoints"
# property :assets_midpoints, String, field: "AssetsMidpoints"
# property :ebitda_tevo_adjustment_point, String, field: "EBITDATevoAdjustmentPoint"
# property :cash_tevo_adjustment_point, String, field: "CashTevoAdjustmentPoint"
# property :ar_tevo_adjustment_point, String, field: "ARTevoAdjustmentPoint"
# property :inventory_tevo_adjustment_point, String, field: "InventoryTevoAdjustmentPoint"
# property :total_loabilitie_tevo_adjustment_point, String, field: "TotalLoabilitieTevoAdjustmentPoint"
# property :revemp_tevo_adjustment_point, String, field: "RevempTevoAdjustmentPoint"


# property :multiple_calc_pr_lower, String, field: "MultipleCalcPRLower"
# property :multiple_calc_pr_median, String, field: "MultipleCalcPRMedian"
# property :multiple_calc_pr_upper, String, field: "MultipleCalcPRUpper"
# property :multiple_calc_pebitda_lower, String, field: "MultipleCalcPEBITDALower"
# property :multiple_calc_pebitda_median, String, field: "MultipleCalcPEBITDAMedian"
# property :multiple_calc_pebitda_upper, String, field: "MultipleCalcPEBITDAUpper"
# property :multiple_calc_p_asset_lower, String, field: "MultipleCalcPAssetLower"
# property :multiple_calc_p_asset_median, String, field: "MultipleCalcPAssetMedian"
# property :multiple_calc_p_asset_upper, String, field: "MultipleCalcPAssetUpper"
# property :multiple_calc_p_net_income_lower, String, field: "MultipleCalcPNetIncomeLower"
# property :multiple_calc_p_net_income_median, String, field: "MultipleCalcPNetIncomeMedian"
# property :multiple_calc_p_net_income_upper, String, field: "MultipleCalcPNetIncomeUpper"
# property :multiple_adjustments, String, field: "MultipleAdjustments"


property :pr_adjust_multiples_lower, String
property :pr_adjust_multiples_estimate, String
property :pr_adjust_multiples_upper, String
property :pebitda_adjust_multiples_lower, String
property :pebitda_adjust_multiples_estimate, String
property :pebitda_adjust_multiples_upper, String
property :p_asset_adjust_multiples_lower, String
property :p_asset_adjust_multiples_estimate, String
property :p_asset_adjust_multiples_upper, String
property :p_net_income_adjust_multiples_lower, String
property :p_net_income_adjust_multiples_estimate, String
property :p_net_income_adjust_multiples_upper, String



# property :company, String
property :generated_by_user, String

########################################################
#
# Timestamps
#
########################################################
property :created_at, DateTime
# property :updated_at, DateTime


########################################################
#
# Soft Deletes
#
########################################################
property :deleted, Boolean, default: false
property :deleted_at, DateTime


########################################################
#
# Relationships
#
########################################################
belongs_to :company
belongs_to :user, required: false
belongs_to :affiliate_user, required: false
belongs_to :affiliate, required: false
########################################################
#
# Hooks
#
########################################################


########################################################
#
# Additional Methods
# Use SecureRandom.hex(64)
#
#
########################################################


########################################################
#
# end class
#
########################################################

end 
		