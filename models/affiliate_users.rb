#recurring_services Class
class AffiliateUser
include DataMapper::Resource
storage_names[:default] = 'affiliate_users'

########################################################
#
# Properties
#
########################################################
property :id, Serial
# property :sales_force_id, String, field: 'SalesForceId'
property :x_id, UUID, required: true, unique: true, default: proc { UUIDTools::UUID.random_create }
property :user_name, String
property :first_name, String
property :last_name, String
property :email, String
property :work_phone, String
property :mobile, String
property :role, String
property :user_type, String
property :active, Boolean, default: true
property :password, BCryptHash, :required => true
property :password_reset, Boolean, default: false
property :stripe_customer_id, String
property :subscribed_plan_id, String
property :subscribed_plan, String

########################################################
#
# Timestamps
#
########################################################
property :created_at, DateTime, field: 'CreatedOn'
property :updated_at, DateTime, field: 'ModifiedOn'
property :deleted, Boolean, default: false
property :deleted_at, DateTime, required: false

########################################################
#
# Relationships
#
########################################################
belongs_to :affiliate, required: false
has n, :academy_affiliateusers
has n, :achievements
has 1, :user_badge

########################################################
#
# Validations
#
########################################################
validates_presence_of :email
validates_presence_of :first_name
validates_presence_of :last_name
validates_uniqueness_of :email

########################################################
#
# Hooks
#
########################################################


########################################################
#
# Additional Methods
#
########################################################


########################################################
#
# end class
#
########################################################

end 