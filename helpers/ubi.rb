def generate_ubi(starting_number)
	# starting_number = "ANB-100-001"
	
	# Format is ANB-100-001, where 100-001 is a sequential number and ANB is incremented to ANC when numeric value is greater than 999-999

	# Get number, strip out alpha
	alpha = starting_number.split('-',2).first
	numeric = starting_number.split('-',2).last.tr("-","").to_i

	# see if number is larger than 999999, if so increment alpha
	if numeric >= 999999
		# Reset to 1
		numeric = 1

		# Right pad to get 6 places
		numeric = numeric.to_s.rjust(6,"0")

		# Check if the last alpha is not Z
		if alpha[2] != "Z"
			# Create a new array
			alpha_sequence = Array.new
			# Put Alpha in Numbers
		  alpha.split("").each do |letter|
		   # Split the Alpha to an array	
		 	 alpha_sequence << get_number_from_letter(letter).to_i
		  end

		  # increment the last Letter/Number (eg A to B)
		  alpha_sequence[2] = alpha_sequence[2] + 1 
		
		else
			# Letter is Z so we'll shift the Letters eg ANZ -> AOA
			# Create a new Array
			alpha_sequence = Array.new

			# Put Alpha in Numbers
		  alpha.split("").each do |letter|
		   # Find the alpha in a number
		 	 alpha_sequence << get_number_from_letter(letter).to_i
		  end 
		  
		  # If the last letter is 26 or greater (shouldn't be greater but let's fix it if it is)
		  if alpha_sequence[2] >= 26
		  	# reset the 3rd letter to A/1
		  	alpha_sequence[2] = 1

		  	# Now check to see if the 2nd Letter needs to be changed
		  	if alpha_sequence[1] != 26
		  		# Increment the 2nd letter
		  		alpha_sequence[1] =	alpha_sequence[1] + 1
		  	else
		  		# The 2nd Letter was Z so let's set it to A
		  		alpha_sequence[1] = 1
		  		# We'll also roll the 1st letter up one
		  		alpha_sequence[0] = alpha_sequence[0].to_i + 1
		  	end # End  alpha_sequence[1] != 26

		  end # End alpha_sequence[2] >= 26
		 
		end # alpha[2] != "Z"

		# Init the final letter variable
		alpha_sequence_letters = ""

	  # Put Numbers in Alpha and roll it into the string
	  alpha_sequence.each do |n|
	  	alpha_sequence_letters << get_letter_from_number(n.to_i)
	  end
	else # The Letters are good just roll up the numbers
		# increment number
		numeric = numeric + 1
		# Right pad the numbers
		numeric = numeric.to_s.rjust(6,"0")
		# Keep the alpha the same
		alpha_sequence_letters = alpha
	end

	# prepent alpha
	ubi = "#{alpha_sequence_letters}-#{numeric.to_s[0,3]}-#{numeric.to_s[3,3]}"
	# return number
	return ubi
end

def get_letter_from_number(number)
	number_hash = Hash[(1..26).to_a.zip(("A".."Z").to_a)]
  return number_hash[number]
end

def get_number_from_letter(letter)
	alphabet_hash = Hash[('A'..'AA').to_a.zip(("1".."26").to_a)]
	return alphabet_hash[letter]
end	

def get_new_ubi
	last_ubi_number = Company.last(order: :id, :ubi.not => nil)
	puts "#{last_ubi_number.id}"
	return generate_ubi(last_ubi_number.ubi)
end