########################################################
#
#  Affiliate Views
#
########################################################
def affiliate_views(affiliate_id: 0, user_id: 0, company_id: 0)
	
	@affiliate_view = AffiliateView.create(
			affiliate_id: affiliate_id,
			user_id: user_id,
			company_id: company_id
		)

end

def affiliate_user_type(id)
  affiliate = Affiliate.first(id: id)
  return affiliate.affiliate_type
end 