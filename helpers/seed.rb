########################################################
#
#  Set
#
########################################################
def demo_seed_affiliate_views()

	300.times do |i|
		view = AffiliateView.create(
			affiliate_id: 1,
			company_id: 1,
			user_id: 1, 
			created_at: Time.now - rand(0..8).days 
		)
	end
end

########################################################
#
#  Seed Credits
#
########################################################
def demo_seed_credits()

	@affiliate = Affiliate.get(1)

	credits = @affiliate.report_credits.all(used: false).count rescue nil
	credits ||= 0
	number_to_add = 10 - credits

	if number_to_add > 0
		number_to_add.times do 
			credit = ReportCredit.create(
				report_level: 2,
				affiliate_id: 1,
				credit_date: Time.now
				# sleep(3)
			)
		end
	end

end

########################################################
#
#  Seed Affiliate Relationships
#
########################################################
def demo_seed_affiliate_relationships()
	@companies = Company.all 

	@companies.all(limit: 10).each do |i|
		relationship = AffiliateRelationship.create(
				company_approved: true, 
				company_approved_date: Time.now,
				company_id: i.id,
				affiliate_id: 1
			)
	end
end

########################################################
#
#  Seed L4's & L5's
#
########################################################
def demo_seed_l4_l5()

		@affiliate = Affiliate.get(1)
		
		@companies = @affiliate.affiliate_relationships.companies
 
		@company_count = @companies.count
		@company_count ||=0

		@l4_reports = Report.all(level: "L4", affiliate_id: @affiliate.id, created_at: (Date.today.beginning_of_month..Date.today.end_of_month)).count
		@l4_reports ||= 0
		
		@l5_reports = Report.all(level: "L5", affiliate_id: @affiliate.id, created_at: (Date.today.beginning_of_month..Date.today.end_of_month)).count
		@l5_reports ||= 0 

		number_of_l4s_to_add = 20 - @l4_reports
		number_of_l4s_to_add ||= 0

		number_of_l5s_to_add = 20 - @l5_reports
		number_of_l5s_to_add ||= 0

		if number_of_l4s_to_add > 0
		number_of_l4s_to_add.times do 
			report = Report.create(
					level: 4,
					affiliate_id: 1,
					created_at: Time.now - rand(0..30).days, 
					date_created: Time.now - rand(0..30).days, 
					score_id: 1,
					company_id: @companies.first(:offset => rand(@company_count)).id,
					tevo: rand(500..800)
				)
			end
		end

	if number_of_l5s_to_add > 0
		number_of_l5s_to_add.times do 
			report = Report.create(
					level: 5,
					affiliate_id: 1,
					created_at: Time.now - rand(0..30).days, 
					date_created: Time.now - rand(0..30).days, 
					score_id: 1,
					company_id: @companies.first(:offset => rand(@companies_count)).id,
					tevo: rand(500..800)
				)
		end
	end

end

########################################################
#
#  Seed Companies
#
########################################################
def demo_seed_companies()


	@companies = Company.all 
	

1000.times do |i| 
	naics = Naic.first(:offset => rand(Naic.all.count))
	naics.id ||= ""
	naics_sector = NaicsSector.first(code: naics.code.to_s[0..1])
	naics_sector.id ||= ""

	geo = Geokit::Geocoders::GoogleGeocoder.geocode("#{Faker::Address.zip_code}, US")


	company = Company.create(
		name: Faker::Company.name,
		address_1: Faker::Address.street_address,
		address_2: "",
		city: geo.city,
		state: geo.state_code,
		zip: geo.zip,
		phone: Faker::PhoneNumber.phone_number,
		ubi: get_new_ubi,
		naics: naics.code,
		naics_sector_code: naics.code.to_s[0..1],
		naic_id: naics.id,
		naics_sector_id: naics_sector.id,

	)

end


end		

########################################################
#
#  Seed Scores
#
########################################################
def demo_seed_scores()
	@companies = Company.all 



	# start
	@companies.each do |c|
	@revenue = rand(100000..20000000)
	@assets = rand(10000..40000000)

	url = "#{settings.api_url}/v1/scores/tevo/"
	@result = HTTParty.post(url, 
    :body => { 
    	:naics => c.naics,
    	:revenue => @revenue,
    	:assets => @assets
    })

	if @result['status'] == "error"
		halt "#{@result['message']}"
	else
	# Insert into Score
	@score = Score.create(
		company_id: c.id,
		level: 1,
		revenue: @revenue,
		assets: @assets,
		naics: c.naics, 
		score_date: Time.now, 
		naics_sector: @result["score"]["naics_sector"],
		sector_asset_class_key: @result["score"]["sector_asset_class_key"],
		sac_intercept_estimate: @result["score"]["sac_intercept_estimate"],
		sac_intercept: @result["score"]["sac_intercept"],
		log_of_revenue: @result["score"]["log_of_revenue"],
		asset_lookup_value: @result["score"]["asset_lookup_value"],
		asset_class: @result["score"]["asset_class"],
		sector_asset_class_combo: @result["score"]["sector_asset_combo"],
		tevo: @result["score"]["tevo"]
	)
	end

	end
end

########################################################
#
#  Seed L2 Scores
#
########################################################
def demo_seed_l2_scores()
	@company = Company.all
	@affiliate = Affiliate.get(session[:affiliate_id])



	@company.each do |company|
	@revenue = rand(1000000..2999999999)
	@assets = rand(100000..99999999)
	@employees = rand(5..250)
	@ebitda = rand(10000..20000000)
	@cash = rand(100000..20000000)
	@ar = rand(30000..3000000)
	@inventory = rand(300000.9999999)
	@total_liabilities = rand(300000.99999999)
	@rev_emp = (@revenue/@employees).round(2) rescue nil 
	@rev_emp ||= 0

	# begin
	url = "#{settings.api_url}/v1/scores/extended/"
	@result = HTTParty.post(url, 
    :body => { 
        :naics => company.naics,
        :revenue => @revenue,
        :assets => @assets,
        :ebitda => @ebitda,
        :cash => @cash,
        :ar => @ar,
        :inventory => @inventory,
        :total_liabilities => @total_liabilities ,
        :employees => @employees,
        :rev_emp => @rev_emp,
    })

	if @result['status'] == "error"
		halt  "#{@result['message']}"
		# redirect "/companies/#{params[:x_id]}/score/l2"
	else
	# Insert into Score
	@score = Score.create(
		company_id: company.id,
		level: 2,
		naics: company.naics,
		tevo: @result["score"]["extended"]["extended_tevo_score"],
		tevo_upper:  @result["score"]["multiples"]["industry_metrics"]["tevo_stats"]["max"].to_i,
		tevo_lower: @result["score"]["multiples"]["industry_metrics"]["tevo_stats"]["min"].to_i,
		tevo_min: @result["score"]["extended"]["industry_metrics"]["industry_tevo_max"].to_i,
		tevo_median: @result["score"]["extended"]["industry_metrics"]["industry_tevo_median"].to_i,
		tevo_max: @result["score"]["extended"]["industry_metrics"]["industry_tevo_min"],
		score_date: Time.now, 
		naics_sector: @result["score"]["tevos"]["naics_sector"],
		sector_asset_class_key: @result["score"]["tevos"]["sector_asset_class_key"],
		sac_intercept_estimate: @result["score"]["tevos"]["sac_intercept_estimate"],
		sac_intercept: @result["score"]["tevos"]["sac_intercept"],
		log_of_revenue: @result["score"]["tevos"]["log_of_revenue"],
		asset_lookup_value: @result["score"]["tevos"]["asset_lookup_value"],
		asset_class: @result["score"]["tevos"]["asset_class"],
		sector_asset_class_combo: @result["score"]["tevos"]["sector_asset_combo"],
		revenue: @revenue,
		assets: @assets,
		ar: @ar,
		inventory: @inventory,
		total_liabilities: @total_liabilities,
		number_of_employee: @employees,
		
		# upper_estimate: ,
		# lower_estimate: ,

		multiple_revenue_lower: @result["score"]["multiples"]["multiple_revenue_lower"],
		multiple_revenue_estimate: @result["score"]["multiples"]["multiple_revenue_median"],
		multiple_revenue_upper: @result["score"]["multiples"]["multiple_revenue_upper"],

		multiple_ebitda_lower: @result["score"]["multiples"]["multiple_ebitda_lower"],
		multiple_ebitda_estimate: @result["score"]["multiples"]["multiple_ebitda_median"],
		multiple_ebitda_upper: @result["score"]["multiples"]["multiple_ebitda_upper"],

		multiple_net_income_lower: @result["score"]["multiples"]["multiple_net_income_lower"],
		multiple_net_income_estimate: @result["score"]["multiples"]["multiple_net_income_median"],
		multiple_net_income_upper: @result["score"]["multiples"]["multiple_net_income_upper"],

		multiple_assets_lower: @result["score"]["multiples"]["multiple_assets_lower"],
		multiple_assets_estimate: @result["score"]["multiples"]["multiple_assets_median"],
		multiple_assets_upper: @result["score"]["multiples"]["multiple_assets_upper"],

		percentile_revenue: @result["score"]["extended"]["percent_ranks"]["revenue"],
		percentile_ebitda: @result["score"]["extended"]["percent_ranks"]["ebitda"],
		percentile_cash: @result["score"]["extended"]["percent_ranks"]["cash"],
		percentile_ar: @result["score"]["extended"]["percent_ranks"]["ar"],
		percentile_inventory: @result["score"]["extended"]["percent_ranks"]["inventory"],
		percentile_assets: @result["score"]["extended"]["percent_ranks"]["assets"] ,
		percentile_total_liabilities: @result["score"]["extended"]["percent_ranks"]["total_liabilities"],
		percentile_rev_emp: @result["score"]["extended"]["percent_ranks"]["rev_emp"],

		waterfall_revenue: @result["score"]["extended"]["waterfall"]["revenue"],
		waterfall_ebitda: @result["score"]["extended"]["waterfall"]["ebitda"],
		waterfall_cash: @result["score"]["extended"]["waterfall"]["cash"],
		waterfall_ar: @result["score"]["extended"]["waterfall"]["ar"],
		waterfall_inventory: @result["score"]["extended"]["waterfall"]["inventory"],
		waterfall_total_liabilities: @result["score"]["extended"]["waterfall"]["total_liabilities"],
		waterfall_rev_emp: @result["score"]["extended"]["waterfall"]["rev_emp"],
		waterfall_assets: @result["score"]["extended"]["waterfall"]["assets"]
	)
	end

end
end


########################################################
#
#  Randomize Dates
#
########################################################
def demo_seed_randomize_dates 
	@alerts = Alert.all 

	@alerts.each do |alert|
		alert.update(created_at: Time.now - rand(1..30).days)
	end

	@scores = Score.all 

	@scores.each do |score| 
		score.update(
			created_at: Time.now - rand(1..30).days,
			score_date: Time.now - rand(1..30).days
			)
	end

	@reports = Report.all 

	@reports.each do |report| 
		report.update(
			created_at: Time.now - rand(1..30).days,
			date_created: Time.now - rand(1..30).days
			)
	end

end

########################################################
#
#  Seed Network Requets
#
########################################################
def demo_seed_network_requests
	# @all_network_requests = AffiliateRelationships.all(company_approved: false)

	@companies = Company.all 

	@companies.all(limit: 4).each do |i|
		relationship = AffiliateRelationship.create(
				company_approved: false, 
				company_id: i.id,
				affiliate_id: 1
			)
	end

end


########################################################
#
#  Seed Revenue Number
#
########################################################
def demo_seed_algolia_revenue

my_index = Algolia::Index.new(Sinatra::Application.settings.company_index)

@companies = Company.all 

@companies.each do |company|

	my_index.save_object({
      :objectID => company.id,
      :name => company.name,
      :id => company.id,
      :x_id => company.x_id,
      :ubi => company.ubi,
      :ein => company.ein,
      :address => company.address_1,
      :address_2 => company.address_2,
      :city => company.city,
      :state => company.state,
      :zip => company.zip,
      :phone => company.phone,
      :website => company.website,
      :naics => company.naics,
      :tevo => rand(400..800),
      :revenue => rand(400000..22000000),
      :total_asset =>  rand(400000..22000000),
      # :naics_name => naics_name,
      :number_of_employee => rand(3..200)
    })

end

end