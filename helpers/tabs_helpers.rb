########################################################
#
#  Groups Dashboard Tabs
#
########################################################
def group_dashboard_tabs(active)
 x_tabs(active: active, tabs: [
    {url:"/groups/dashboard/#{@group.x_id}", name:"Dashboard", id:"groupDashboard", icon:"fa-tachometer"},
    {url:"/groups/finance/#{@group.x_id}", name:"Financial", id:"groupFinancial", icon:"fa-bank"},
    {url:"/groups/base/#{@group.x_id}", name:"Base Business", id:"groupBaseBusiness", icon:"fa-anchor"},
    {url:"/groups/systems/#{@group.x_id}", name:"Systems", id:"groupSystems", icon:"fa-sitemap"},
    {url:"/groups/management/#{@group.x_id}", name:"Management", id:"groupManagement", icon:"fa-users"},
    {url:"/groups/growth/#{@group.x_id}", name:"Growth", id:"groupGrowth", icon:"fa-line-chart"},
  ]) 
end

########################################################
#
#  Company Detail Tabs
#
########################################################
def company_detail_tabs(active)
 x_tabs(active: active, tabs: [
    {url:"/companies/#{@company.x_id}", name:"Dashboard", id:"companyDashboard", icon:"fa-tachometer"},
    {url:"/companies/#{@company.x_id}/profile/", name:"Profile", id:"companyProfile", icon:"fa-vcard"},
    {url:"/companies/#{@company.x_id}/score/all", name:"Score", id:"companyScore", icon:"fa-tachometer"},
    {url:"/companies/#{@company.x_id}/contacts/", name:"Contacts", id:"companyContacts", icon:"fa-users"},
    # {url:"/companies/#{@company.x_id}/reports/", name:"Reports", id:"companyReports", icon:"fa-file-o"},
    {url:"/companies/#{@company.x_id}/affiliates/", name:"Network", id:"companyNetwork", icon:"fa-sitemap"},
    # {url:"/companies/#{@company.x_id}/score/", name:"Intent", id:"companyIntent", icon:"fa-file-pdf-o"},
    {url:"/companies/#{@company.x_id}/alerts/", name:"Alerts", id:"companyAlerts", icon:"fa-bell"},
    # {url:"/companies/#{@company.x_id}/company_data/", name:"Company Data", id:"companyData", icon:"fa-bell"},
  ]) 
end

########################################################
#
#  Company Score Tabs
#
########################################################
def company_score_tabs(active)
 x_tabs(active: active, tabs: [
    {url:"/companies/#{@company.x_id}/score/all/", name:"Scores", id:"companyScores", icon:"fa-tachometer"},

  ]) 
end

########################################################
#
# Company Tabs 
#
########################################################
def company_tabs(active)
  x_tabs(active: active, tabs: [
    {url:"/companies/", name:"My Companies", id:"myCompanies", icon:"fa-tachometer"},
    {url:"/companies/all/", name:"All Companies", id:"allCompanies", icon:"fa-tachometer"},
    {url:"/companies/add/", name:"Add Company", id:"addCompany", icon:"fa-plus"},
  ]) 
end


########################################################
#
# Network Tabs 
#
########################################################
def network_tabs(active)
  x_tabs(active: active, tabs: [
    {url:"/network/", name:"My Network", id:"myNetwork", icon:"fa-sitemap"},
    {url:"/network/affiliates/", name:"Network", id:"affiliateNetwork", icon:"fa-sitemap"},
    {url:"/network/requests/", name:"Network Requests", id:"networkRequests", icon:"fa-bell"},
    # {url:"/network/pro-affiliates/", name:"Pro Affiliates", icon:"fa-plus"},
    # {url:"/network/affiliates/", name:"Affiliates", icon:"fa-plus"},
  ]) 
end


########################################################
#
# Profile Tabs 
#
########################################################
def profile_tabs(active)
  x_tabs(active: active, tabs: [
    {url:"/profile/", name:"Profile", id:"profile", icon:"fa-building"},
    {url:"/profile/logo/", name:"Logo", id:"profileLogo", icon:"fa-image"},
    # {url:"/profile/report-customization/", name:"Report Customization", id:"reportCustomization", icon:"fa-list"},
    {url:"/profile/categories/", name:"Categories", id:"profileCategories", icon:"fa-list"},
    {url:"/profile/description/", name:"Description", id:"profileDescription", icon:"fa-pencil-square"},
    {url:"/profile/users/", name:"Users", id:"affiliateUsers", icon:"fa-users"},
    # {url:"/profile/history/", name:"History", id:"profileHistory", icon:"fa-history"},
    {url:"/profile/plan/", name:"Plan", id:"subscriptionPlan", icon:"fa-sliders"},
    {url:"/profile/reset-password/", name:"Reset Password", id:"resetPassword", icon:"fa-key"},
    {url:"/profile/billing/", name:"Billing", id:"profileBilling", icon:"fa-table"},
    # {url:"/profile/forecast/", name:"Forecast", id:"profileForecast", icon:"fa-table"},
  ]) 
end

########################################################
#
# Academy Tabs 
#
########################################################
def acadmey_tabs(active)
 x_tabs(active: active, tabs: [
    {url:"/academy/", name:"Academy", id:"mainAcademy", icon:"fa-play"},
    {url:"/academy/webinars/", name:"Webinars", id:"academyWebinars", icon:"fa-play"},
    {url:"/academy/resources/", name:"Resources", id:"academyResources", icon:"fa-signal"},
])
    
end