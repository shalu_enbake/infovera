########################################################
#
#  Affiliate
#
########################################################
def number_of_reports
	@num_report = @affiliate.reports.all.count
end

def number_of_companies
	@num_companies = @affiliate.affiliate_relationships.companies.count
end

def number_of_groups
	@num_companies = @affiliate.affiliate_groups.count
end

def affiliate_base_strength
	@strength = number_of_reports + number_of_companies + number_of_groups
end

def affiliate_base_color
	case affiliate_base_strength
	when 0..18
	  @color = "#ed6b3b"
	when 19..36
	  @color = "#f3c548"
	when 36..54
	  @color = "#7aac51"
	when 54..80
	  @color = "#449ce1"
	else
	  @color = "#367eb5"
	end
end

def affiliate_base_level
	case affiliate_base_strength
	when 0..18
	  @level = "Base"
	when 19..36
	  @level = "Intermediate"
	when 36..54
	  @level = "Advance"
	when 54..80
	  @level = "Expert"
	else
	  @level = "All Star"
	end
end

def affiliate_dail_value
	case affiliate_base_strength
	when 0..18
	  @dail_value = 20
	when 19..36
	  @dail_value = 40
	when 36..54
	  @dail_value = 60
	when 54..80
	  @dail_value = 80
	else
	  @dail_value = 100
	end
end