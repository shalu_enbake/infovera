########################################################
#
#  Get Score Count
#
########################################################
def get_score_count(company_id: 0)

	score_count = Score.all(company_id: company_id).count
	score_count ||= 0
	return score_count
end


########################################################
#
#  Get Score Count
#
########################################################
def get_score_count_badge(company_id: 0)
 score_count = get_score_count(company_id: company_id)
 return "&nbsp; <span class='badge'>#{score_count} #{"Score".pluralize(score_count)}</span>"
end