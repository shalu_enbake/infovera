########################################################
#
#  Has acccepted terms
#
########################################################
def has_accepted_terms?
	@affiliate = Affiliate.get(session[:affiliate_id])
	@terms = Term.last(active: true)
	# halt "#{@terms.inspect} <hr> #{@affiliate.terms_accepted.inspect}"
	if @affiliate.terms_accepted.all.count == 0
		session[:authorized] = false
		redirect "/login/terms/"
	else
		if @affiliate.terms_accepted.last.term_id != @terms.id 
			session[:authorized] = false
			redirect "/login/terms/"
		end
	end
end