########################################################
#
#  Get Report Levels
#
########################################################
def get_report_levels
	levels = [	
		{name: "L2", id: "L2"},
		{name: "L3", id: "L3"},
		{name: "L4", id: "L4"},
		{name: "L5", id: "L5"},
	]
	return levels
end

def get_user_roles
	levels = [	
		{name: "Read-Only", id: "Read-Only"},
		{name: "Contributor", id: "Contributor"},
		{name: "Admin", id: "Admin"},
	]
	return levels
end


def get_selected_plan_amount(plan_id)
	amount = Stripe::Plan.retrieve(plan_id).amount
	return amount
end


