########################################################
#
# Download PDF
#
########################################################
# def download_pdf(loan_number: 0, save_name: "")
#   File.open(save_name, 'wb') do |file|
#     reap = s3.get_object({ bucket:'dev-vystem', key:"loans/#{loan_number}" }, target: )
#   end
# end
def s3_init()
  connection = Fog::Storage.new({
      :provider                 => 'AWS',
      :region                   => settings.aws_region,
      :aws_access_key_id        => settings.aws_access_key_id,
      :aws_secret_access_key    => settings.aws_secret_access_key
  })
  return connection
end


def get_loan_pdf_link_https(bucket: Sinatra::Application.settings.aws_bucket, s3_path: "")
  s3 = x_s3_init
  expiry = Time.now.to_i + 604800
  link = s3.get_object_https_url(Sinatra::Application.settings.aws_bucket, "#{s3_path}", expiry)
  link ||= "#"
  return link
end

def random_color(message)
  if  message.include?("connected")
    return "bg_custom-connected"

  elsif message.include?("dis-connected")
    return "bg_custom-danger1"

  elsif message.include?("destroy a report")
    return "bg_custom-danger2"      
  
  elsif message.include?("tevo is updated")
    return "bg_custom-sucess1"

  elsif message.include?("Company Created")
    return "bg_custom-updated"
  elsif message.include?("created a score")
    return "bg_custom-sucess2"
  elsif message.include?("created a report")
    return "bg_custom-sucess3" 
    
  else
    return "bg_custom-default"
  end
end


