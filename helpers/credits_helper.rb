########################################################
#
#  Get Level 2 Credits
#
########################################################
def get_level2_credits

	affiliate = Affiliate.get(session[:affiliate_id])
	credits = affiliate.report_credits.all(deleted: false, used: false, report_level: "L2").count rescue nil 
	credits ||= 0
	return credits
end


########################################################
#
#  Get Credits Badge
#
########################################################
def get_level2_credits_badge

 return "&nbsp; <span class='badge'>#{get_level2_credits} Credits</span>"
end

def check_specilitiy(affiliate_subcategory,affiliate_category_id,affiliate_id)
  affiliate_specialties = AffiliateSpecialty.first(affiliate_subcategory_id: affiliate_subcategory, affiliate_category_id:affiliate_category_id,affiliate_id: affiliate_id).present?
  return affiliate_specialties
end